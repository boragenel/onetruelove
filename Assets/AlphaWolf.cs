﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OneTrueLove;

public class AlphaWolf : MonoBehaviour {

    Vector3 basePos;

	// Use this for initialization
	void Start () {
        basePos = transform.position;
        Dispatcher.GameEvents.OnDayStart += updatePos;
	}
	

    public void updatePos()
    {
        transform.position = basePos;
    }

    private void OnDestroy()
    {
        Dispatcher.GameEvents.OnDayStart -= updatePos;
    }

}
