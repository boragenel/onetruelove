﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if( Input.GetMouseButtonUp(0) )
        {
            GameController.Instance.setState(GameState.DURING);
            Time.timeScale = 1;
            gameObject.SetActive(false);
        }
	}
}
