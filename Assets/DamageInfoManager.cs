﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageInfoManager : MonoBehaviour {

    public List<DamageInfo> dmgInfoList;

	// Use this for initialization
	void Start () {
		
	}

    public void Show(Vector3 pos, float amount)
    {
        DamageInfo tempInfo = null;

        foreach(DamageInfo dInfo in dmgInfoList)
        {

            if( !dInfo.isPlaying )
            {
                tempInfo = dInfo;
                break;
            }

        }
        if( tempInfo != null)
        tempInfo.Show(pos, amount);


    }

    // Update is called once per frame
    void Update () {
		
	}
}
