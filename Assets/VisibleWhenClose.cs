﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleWhenClose : MonoBehaviour {

    Transform pTransform;
    float radius = 20;
    SpriteRenderer sRenderer;

    void Start()
    {
        pTransform = GameController.Instance.player.transform;
        sRenderer = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
		if( Vector3.Distance(transform.position, pTransform.position) < radius)
        {
            sRenderer.enabled = true;
        } else
        {
            sRenderer.enabled = false;
        }
	}
}
