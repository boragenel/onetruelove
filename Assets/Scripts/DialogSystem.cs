﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System;

public class DialogSystem : MonoBehaviour {

    public Text text;
    public GameObject answers;
    public Tweener tweener;

    //responses for yes / no 
    public Action yesAction;
    public Action noAction;

    IEnumerator leaver;


	void Start () {

        //Debug.Log(GetComponent<RectTransform>().anchoredPosition+"*****");        
        noAction = closePanel;
	}
	

    public void show(string _text,int _lines=1,bool _question=false)
    {
        //leaver is a function that controls the closing of a dialog message? DPJ
        if( leaver!= null)
        StopCoroutine(leaver);
        tweener.Kill();

        //activate the dialog panel
        gameObject.SetActive(true);
        //instantiate the rectangle with 0 height at top of screen, so it can scroll down
        GetComponent<RectTransform>().sizeDelta = new Vector2(600, 0);
        //set the text component to include the message we want to display
        text.text = _text;
        //tween (move between current state and the given state) to a height of 50 + 45 for every line, of 1 second).
        tweener = GetComponent<RectTransform>().DOSizeDelta(new Vector2(600, 50+(45*_lines)),1);

        //if it isn't a question, we are closing the dialog based on time.
        if( !_question)
        { 
            //leaver is a pointer to the leave coroutine that will be called on next line. Can call stop on it, and it'll stop the coroutine. 
            leaver = leave(0.8f+ (_lines*2.2f));
            StartCoroutine(leaver);
            answers.SetActive(false);
        }
        else
        {
            answers.SetActive(true);
        }
    }

    //wait X amount of time before retracting the dialog box back up (tweening for the slide effect)
    IEnumerator leave(float _secs)
    {
        yield return new WaitForSeconds(_secs);
        tweener = GetComponent<RectTransform>().DOSizeDelta(new Vector2(600, 0), 1);
    }

    public void invokeYesAction()
    {
        yesAction.Invoke();
    }


    public void invokeNoAction()
    {
        noAction.Invoke();
    }

    public void closePanel()
    {
        gameObject.SetActive(false);
    }




}
