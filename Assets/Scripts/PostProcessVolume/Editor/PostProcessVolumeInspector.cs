﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PostProcessVolume))]
public class PostProcessVolumeInspector: Editor
{
    PostProcessVolume pp;
    int selected = -1;

    private void OnEnable()
    {
        if (pp == null)
            pp = (PostProcessVolume)target;
    }

    private void OnSceneGUI()
    {
        Vector3[] points = new Vector3[4];
        points[0] = new Vector3(pp.Area.min.x, pp.Area.min.y);
        points[1] = new Vector3(pp.Area.min.x, pp.Area.max.y);
        points[2] = new Vector3(pp.Area.max.x, pp.Area.min.y);
        points[3] = new Vector3(pp.Area.max.x, pp.Area.max.y);

        Handles.color = new Color(1f, 1f, 0, 0.33f);
        Rect transformedArea = pp.Area;
        transformedArea.position = pp.Area.position + new Vector2(pp.transform.position.x, pp.transform.position.y);
        Handles.DrawSolidRectangleWithOutline(transformedArea, Handles.color, Color.yellow);
        Handles.color = Color.yellow;
        var left = points[0] + (points[1] - points[0]) * 0.5f;
        var right = points[2] + (points[3] - points[2]) * 0.5f;
        left += pp.transform.position;
        Handles.DrawWireDisc(left, Vector3.forward, 10f);
    }

    void drawHandle(int index)
    {

    }
}
