﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

//this class is set on an object that surrounds both village and forest, so we can apply post processing effects to the visuals in those areas.
public class PostProcessVolume : MonoBehaviour {
    public Rect Area = new Rect(Vector3.zero, Vector2.one);
    public PostProcessingProfile PostProcess;
    
    
    private void Update()
    {
        var nRect = Area;
        //not sure how this bit works, as the transform shouldnt be moving during the game? DPJ
        nRect.position += new Vector2(transform.position.x, transform.position.y);
        //if the camera is within the post processing rectangle
        if (nRect.Contains(Camera.main.transform.position))
        {
            //apply the post process
            var pp = Camera.main.GetComponent<PostProcessingBehaviour>();
            if(pp != null)
            {
                pp.profile = PostProcess;
                
            }
        }
    }

    //display the area on the scene view
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position + (Vector3.right * Area.min.x) + (Vector3.up * Area.min.y), transform.position + (Vector3.right * Area.max.x) + (Vector3.up * Area.min.y));
        Gizmos.DrawLine(transform.position + (Vector3.right * Area.min.x) + (Vector3.up * Area.min.y), transform.position + (Vector3.right * Area.min.x) + (Vector3.up * Area.max.y));
        Gizmos.DrawLine(transform.position + (Vector3.right * Area.min.x) + (Vector3.up * Area.max.y), transform.position + (Vector3.right * Area.max.x) + (Vector3.up * Area.max.y));
        Gizmos.DrawLine(transform.position + (Vector3.right * Area.max.x) + (Vector3.up * Area.min.y), transform.position + (Vector3.right * Area.max.x) + (Vector3.up * Area.max.y));
    }
}
