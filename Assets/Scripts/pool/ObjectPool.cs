﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
*
* 30/01/2016
* Bora Genel
* © Brainoid Games 2016
*
* Object Pool
* 
* The pool here is not a complex one.
* It is simply built around useObject(path) function 
* where path is used to analyze the type of the object since the objects are Prefabs in Resources folder.
* Pool used Resources.Load() to load the Prefabs, if it has an unused instance , returns it.
* If there are no instances, it creates the instance with Instantiate method which is pretty slow.
* It is advised to create several objects when loading the game, then use them when needed.
* If you try to create objects when loading the level, you will have a noticable lag, at least once.
*
**/
public sealed class ObjectPool :MonoBehaviour {

	// Dictionary helps us manage the pool
	private Dictionary<string,List<GameObject> > pool = new Dictionary<string, List<GameObject>>();

	// Singleton instance
	private static ObjectPool instance=null;

	// Singleton padlock
	private static readonly object padlock = new object();

    public GameObject forestTreePrefab;
    public GameObject boulderPrefab;
    public GameObject smallStonePrefab;
    public GameObject cabbagePrefab;
    public GameObject blobPrefab;
    public GameObject pickupPrefab;
    public GameObject werewolfPrefab;
    public GameObject frogPrefab;
    public GameObject ladyLiamPrefab;
    public GameObject rosesPrefab;

    static public ObjectPool Instance
    {
        get
        {
            if (instance == null)
            {
                instance = UnityEngine.Object.FindObjectOfType(typeof(ObjectPool)) as ObjectPool;

                if (instance == null)
                {
                    GameObject go = new GameObject("ObjectPool");
                    
                    DontDestroyOnLoad(go);
                    instance = go.AddComponent<ObjectPool>();
                }
            }
            return instance;
        }
    }

     void Start()
    {
        init();
    }

    public void init()
    {
        populatePoolWithPrefab(forestTreePrefab, 100);
        populatePoolWithPrefab(boulderPrefab, 40);
        populatePoolWithPrefab(smallStonePrefab, 50);
        populatePoolWithPrefab(cabbagePrefab, 30);
        populatePoolWithPrefab(pickupPrefab, 30);
        populatePoolWithPrefab(werewolfPrefab, 5);
        populatePoolWithPrefab(frogPrefab, 10);
        populatePoolWithPrefab(rosesPrefab, 10);
        populatePoolWithPrefab(ladyLiamPrefab, 10);
    }


    /*
	*
	* @path: string path pointing somewhere in Resources folder.
	*
	* useObject checks the dictionary if it has any objects of @path type.
	* If it has an available instance of that type, it returns it.
	* If not, then it calls the createObject(path) to instantiate the object, then returns it.
	*
	*/
    public GameObject useObject(string path)
	{


		List<GameObject> list = null;
		if( pool.ContainsKey(path) )
		{
			list = pool[path] as List<GameObject>;
		}

		if( list != null)
		{
			
			for(int i=0; i< list.Count; i++)
			{

				if( !list[i].activeSelf )
				{
					
					list[i].SetActive(true);
					return list[i];
				}

			}

		} else {
			List<GameObject> newList = new List<GameObject>();
			pool.Add(path,newList);
		}

		// IF NOT


		GameObject obj = createObject(path);
		return obj;



	}

    public GameObject useObject(GameObject _go)
    {



        List<GameObject> list = null;
        if (pool.ContainsKey(_go.name))
        {
            list = pool[_go.name] as List<GameObject>;
        }

        if (list != null)
        {

            for (int i = 0; i < list.Count; i++)
            {

                if (!list[i].activeSelf)
                {

                    list[i].SetActive(true);
                    return list[i];
                }

            }

        }
        else
        {
            List<GameObject> newList = new List<GameObject>();
            pool.Add(_go.name, newList);
        }

        // IF NOT


        GameObject obj = createObject(_go);
        return obj;



    }


    public void populatePoolWithPrefab(GameObject _prefab,int _noOfInstances)
    {
        GameObject go;
        for(int i = 0; i< _noOfInstances; i++)
        {
            go = createObject(_prefab, false);
            go.transform.parent = transform;
        }

    }


	/*
	*
	* @path: string path pointing somewhere in Resources folder.
	* @activeStatus: This helps when creating a chunk of objects but not activating them(set to false to do that). Default is true.
	*
	* createObject simply instantiates the object of given @path type and adds it to the pool.
	* It names it with auto_increment id's so we can distinguish gameObjects from each other in Editor.
	* Then of course, it returns the intance created.
	*
	*/
	public GameObject createObject(string path,bool activeStatus=true)
	{

		GameObject go = GameObject.Instantiate( Resources.Load(path) ) as GameObject;
		

		
		if( !pool.ContainsKey(path) ){
			

			pool.Add(path,new List<GameObject>());


		} 

		pool[path].Add(go);
		go.name+= pool[path].Count.ToString();
		go.SetActive(activeStatus);
		
		
		
		return go;

	}

    public GameObject createObject(GameObject _go, bool activeStatus = true)
    {

        GameObject go = GameObject.Instantiate(_go) as GameObject;


        

        if (!pool.ContainsKey(_go.name))
        {


            pool.Add(_go.name, new List<GameObject>());


        }

        pool[_go.name].Add(go);
        go.name += pool[_go.name].Count.ToString();
        go.SetActive(activeStatus);



        return go;

    }


    /*
	*
	* @obj: A gameObject possibly present in our pool
	*
	* When you kill an object in pool, you set them inactive instead of destroying them,
	* So you can use them again. Pool then checks if the object is active of not by looking at gameObject.activeSelf bool.
	*
	*/
    public void killObject(GameObject obj)
	{

		obj.SetActive(false);
        obj.transform.parent = transform;

    }


	/*
	*
	* @obj: A gameObject possibly present in our pool
	*
	* This is the easy way to kill all the children of a gameObject.
	*
	*/
	public void killChildrenOfObject(GameObject obj)
	{

		for(int i = 0; i < obj.transform.GetChildCount(); i++)
		{

			
			killObject(obj.transform.GetChild(i).gameObject);

		}

		obj.SetActive(false);

	}

}
