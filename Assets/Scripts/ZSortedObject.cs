﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

//script to set an objects z index based on it's y position.
[ExecuteInEditMode]
public class ZSortedObject : MonoBehaviour {

    SpriteRenderer sRenderer;
    //the max amount to sort by
    private int sortMargin = 1000;
    public bool liveUpdate = false;
    float prevPosY = 0;

    private void Awake()
    {
        sRenderer = GetComponent<SpriteRenderer>();
    }

    void Start () {      
        //updateZSortingAccordingtoYPos(null);
        MessageDispatcher.AddListener("UpdateZSorting", updateZSortingAccordingtoYPos,true);

    }

    //sets the sorting order based on the y position on the screen
    public void updateZSortingAccordingtoYPos(IMessage msg)
    {     
        sRenderer.sortingOrder = sortMargin - Mathf.RoundToInt(transform.position.y);       
    }

	// Update is called once per frame
	void Update () {

        //if game isnt playing, don't try update? Was this due to issues trying to update when it shouldn't be? DPJ
        if (!Application.isPlaying)
        {
            liveUpdate = true;
        }

        if (liveUpdate)
            updateZSortingAccordingtoYPos(null);
    }

}
