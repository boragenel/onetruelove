﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//used to display the alchemy crafting panel upon trigger enter and hide it when you exit
public class TriggerAlchemy : MonoBehaviour {
    public GameObject craftingPanel;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            //enables the panel
            craftingPanel.gameObject.SetActive(true);
            //calls reset, to reset any selected materials etc
            craftingPanel.transform.GetChild(0).GetComponent<CraftMenuManager>().ResetMenu();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            craftingPanel.gameObject.SetActive(false);         
        }
    }
}
