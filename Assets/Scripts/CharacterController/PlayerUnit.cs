﻿using OneTrueLove;
using UnityEngine;
using DG.Tweening;

using System.Collections;

[RequireComponent(typeof(CharacterController2D))]
public class PlayerUnit: GenericUnit
{
    DialogSystem ds;

    public AudioClip playerDeathSFX;
    

    public bool isDeadKnown = false;
    //reference to the different sprites to show for different states
    public GameObject frogSelf;
    public GameObject werewolfSelf;
    public GameObject rosesSelf;

    //reference to recipeBook so that we can update it if any potions used on player is the first time they've been used
    public RecipeBookManager _recipeBookManager;

    public bool inLove = false;

    protected override void Awake()
    {
        base.Awake();
        InitDefaults();

        ds = GameController.Instance.dialogSystem;

        //Subscriptions
        Dispatcher.GameEvents.OnDayStart += DayStart;
        OnDamageTaken += DamageTaken;
        OnDeath += HandleDeath;
        OnPotionEffect += PotionEffect;
    }

    private void OnDestroy()
    {
        //UNSubscriptions
        Dispatcher.GameEvents.OnDayStart -= DayStart;
        OnDamageTaken -= DamageTaken;
        OnDeath -= HandleDeath;
        OnPotionEffect -= PotionEffect;
        
    }


    //handling the different click actions on the player
    void PotionEffect(PotionType type)
    {
        //DPJ is this now handled elsewhere?
        //playCastAnim();
        //SoundsEffectsManager.Instance.Play(VialHitSFX);
        GameController.Instance.cursorPotion.SetActive(false);

        switch (type)
        {
            case PotionType.DMG_FIREBALL:
                //get hold of the fireball particles

                GameObject fball = GameController.Instance.Fireball;
                fball.SetActive(true);
                fball.transform.position = transform.position;
                //hard code duration as distance tends to be roughly consistent
                float fireDuration = 0.3f;
                //tween the fireball to your current position
                fball.transform.DOMove(transform.position + (Vector3.up * 2), fireDuration).SetEase(Ease.Linear).OnComplete(() => fireballComplete(fball));
                ds.show("GREAT BALLS OF FIRE, YOU'VE SCORCHED YOURSELF!", 2);
                break;
            case PotionType.DMG_CHILL:
                //gets particle effect
                GameObject chill = GameController.Instance.Chill;
                chill.SetActive(true);
                chill.transform.position = transform.position;
                //slows speed for some time and does some damage
                GameController.Instance.StartCoroutine(ChillEffect(chill));
                ds.show("SO... COLD... BRRRRRR. SO HARD TO MOVE...", 2);
                break;
            case PotionType.DMG_EARTH:
                //shakes camera and does delayed damage;
                CameraShake.Shake(7, 2f);
                GameController.Instance.StartCoroutine(DelayedDamage(10, 1f));
                Dispatcher.GameEvents.OnEarthquake.Invoke();
                // TODO: damage all the enemies in a radius
                ds.show("THE EARTH BENEATH YOU TREMORS FEROCIOUSLY...", 2);
                break;
            case PotionType.DMG_LOVE:
                //start the ending "liam love" after 6 seconds.
                ds.show("SUDDENLY, YOU FEEL AN OVERWHELMING SENSE OF SELF-LOVE. YOU'RE A STRONG INDEPENDANT MAN, WHO DON'T NEED NO WOMAN TO MAKE YOU HAPPY.", 4);
                GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetGameIn(6, PotionType.LIAM_LOVE)
            );
                break;
            case PotionType.TRANS_FROG:
                //switch sprites
                frogSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
                GameController.Instance.StartCoroutine(FrogEffect());
                ds.show("BUD..... WISE.....", 1);

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                break;
            case PotionType.TRANS_WEREWOLF:
                GetComponent<GenericUnit>().SetSpeed(45);
                werewolfSelf.SetActive(true);
                SoundsEffectsManager.Instance.Play(HowlFX);
                transform.GetChild(0).gameObject.SetActive(false);
                ds.show("*HOOOOOOWL* \n WEREWOLVES CARE NOT FOR ALCHEMY, JUST EATING BLOBS... A WASTED DAY. ", 3);

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                GameController.Instance.StartCoroutine(WerewolfEffect());
                break;
            case PotionType.TRANS_YOU:
                //set confused variable, which in the controller script inverts the controls
                GetComponent<GenericUnit>().IsConfused = true;
                ds.show("YOU'RE MIND BECOMES CLOUDED... AS YOU PONDER HOW TO TRANSFORM YOURSELF INTO WORTH HUSBAND MATERIAL", 3);

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                break;
            case PotionType.TRANS_FLOWER:
                //waits, displays message spending day as flower and restarts
                GameController.Instance.StartCoroutine(FlowerEffect());
                rosesSelf.SetActive(true);
                //switch sprites
                transform.GetChild(0).gameObject.SetActive(false);
                ds.show("FINALLY YOU FEEL ROOTED IN LIFE... BUT SUDDENLY YOU REALISE WHY. YOU'VE BEEN TRANSFORMED INTO BEAUTIFUL ROSES", 3);

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                break;
            case PotionType.HEAL_INC_MAX_HP:
                //inc max hp and heal that amount - DPJ we got new functions for these?
                Health.Max += 20;
                TakeDamage(-20);
                ds.show("YOU FEEL MORE RESILIENT. BRING THE PAIN!", 2);
                // TODO: heal anim, dialog
                break;
            case PotionType.HEAL_TO_MAX_HP:
                //heal the difference between max hp and current hp
                ds.show("YOU FEEL REVITALISED. FIGHTING FIT AND READY TO RUMBLE.", 2);
                TakeDamage(-(Health.Max - Health.Current));
                break;
            case PotionType.HEAL_INSTA_KILL:
                //insta kill - well blackout
                Health.Current = 0;
                isDeadKnown = false;
                //this handles the message display of "passed out", sorts out animations and dropping materials
                HandleDeath(null);
                IsAlive = false;
                break;
            case PotionType.HEAL_POISON:
                //damages 10 damage over 2.5 sec
                StartCoroutine(poisonTick());
                // TODO: add poison effect
                ds.show("YOU FEEL VERY UNWELL... AS THOUGH YOUR LIFE IS BEING SLOWLY DRAINED AWAY BY THE MINUTE...", 3);
                break;
            case PotionType.TIME_INC_MOVESPEED:
                //set the speed of the component that handles the movement
                GetComponent<GenericUnit>().SetSpeed(GetComponent<GenericUnit>().Speed * 1.5f);

                if (GameController.Instance.potionUsages.ContainsKey("SPEED_SELF"))
                    GameController.Instance.potionUsages["SPEED_SELF"]++;
                else GameController.Instance.potionUsages.Add("SPEED_SELF", 1);

                if (GameController.Instance.potionUsages["SPEED_SELF"] > 2)
                {
                    GameController.Instance.StartCoroutine(
                    GameController.Instance.dayController.resetGameIn(6, "DINOLAND"));
                    GameController.Instance.setState(GameState.END);
                    ds.show("YOU START TO RUN SO FAST THAT YOU GO BACK IN TIME... :(", 2);
                    return;
                }

                ds.show("YOUR LEGS FEEL CHARGED WITH TREMENDOUS ENERGY... ", 2);
                break;
            case PotionType.TIME_PAUSE:
                //simply sets the pause state
                GameController.Instance.setState(GameState.PAUSED);
                //ds.show("SUDDENLY, TIME STOOD STILL... SECRET PAUSE MENU Y'ALL", 2);
                break;
            case PotionType.TIME_TELEPORT:
                //move 10 units in the direction you are facing!

                if (GameController.Instance.potionUsages.ContainsKey("TELEPORT"))
                    GameController.Instance.potionUsages["TELEPORT"]++;
                else GameController.Instance.potionUsages.Add("TELEPORT", 1);

                if(GameController.Instance.potionUsages["TELEPORT"] > 2)
                {
                    GameController.Instance.StartCoroutine(
                    GameController.Instance.dayController.resetGameIn(6, "DINOLAND"));
                    GameController.Instance.setState(GameState.END);
                    ds.show("TOO MUCH TELEPORTATION GOES WRONG AND YOU GO BACK IN TIME...", 2);
                    return;
                }

                transform.position += -transform.GetChild(0).up * 10;
                ds.show("WOW, TELEPORTATION. LIKE IN THE MOVIES", 2);
                break;
            case PotionType.TIME_REWIND:
                // TODO: Rewind effect on clock

                if (GameController.Instance.potionUsages.ContainsKey("REWIND"))
                    GameController.Instance.potionUsages["REWIND"]++;
                else GameController.Instance.potionUsages.Add("REWIND", 1);

                if (GameController.Instance.potionUsages["REWIND"] > 2)
                {
                    GameController.Instance.StartCoroutine(
                    GameController.Instance.dayController.resetGameIn(5, "DINOLAND"));
                    GameController.Instance.setState(GameState.END);
                    ds.show("TOO MUCH REWINDING GOES WRONG AND YOU REWIND MILLIONS OF YEARS...", 2);
                    return;
                }


                ds.show("THE HANDS ON THE CLOCK START TURNING BACKWARDS... WHAT THE...", 2);
                //adds one to days and restarts day
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.rewindDayIn(6)
           );
                break;
        }

        //update array of discovered potions
        if (type != PotionType.NONE && type != PotionType.TIME_PAUSE && GameController.Instance.discoveredPotions[type] == false)
        {
            Debug.Log("First time discovering potion, adding to recipe book!");
            GameController.Instance.discoveredPotions[type] = true;
            //call the recipe manager to handle new update
            _recipeBookManager.AddRecipeToBook(type);
        }
        //unequips any potion
        GameController.Instance.curDraggedPotion.type = PotionType.NONE;

    }

    //set up defaults for start of game and restart day, animations and states
    void InitDefaults()
    {
        gameObject.SetActive(true);
        if (Animator != null)
            Animator.Play("Idle");
        Controller.enabled = true;
        Controller.Speed = Speed;
        IsAlive = true;
        if( !GameController.Instance.isHardMode )
            Health.Current = Health.Max;

        rosesSelf.SetActive(false);
        werewolfSelf.SetActive(false);
        frogSelf.SetActive(false);

        //reset back to start position? DPJ
        transform.GetChild(0).localScale = new Vector3(20, 20, 20);

        transform.GetChild(0).gameObject.SetActive(true);
        //GetComponent<TopdownCharacterController>().Speed = TopdownCharacterController.INIT_MOVE_SPEED;
        GameController.Instance.changeScene(PortalSceneType.LIAM_HOUSE);
        isDeadKnown = false;


    }

    void DayStart()
    {
        InitDefaults();
        //unsure why we do the change scne here, it is also done in InitDefaults DPJ
        GameController.Instance.changeScene(PortalSceneType.LIAM_HOUSE);
    }

    void DamageTaken(float amount, object source)
    {

    }

    //called upon blacking out - sets animation, displays message, resets the day, drops materials
    void HandleDeath(object instigator)
    {
        if (Animator != null)
            Animator.SetTrigger("Faint");
        Controller.enabled = false;

        //gameObject.SetActive(false);
        if (!isDeadKnown)
        {

            if( GameController.Instance.isHardMode)
            {

                GameController.Instance.dialogSystem.show("YOU BLACK OUT.THE PARK RANGERS ARE OFF DUTY, YOU ARE ALL ALONE NOW.", 2);
                GameController.Instance.StartCoroutine(GameController.Instance.dayController.resetGameIn(3.5f, "HARD_ENDING"));
                

            } else
            {

            
                if( !GameController.Instance.dayController.nightTimeShown)
                { 
                    GameController.Instance.dialogSystem.show("YOU PASSED YOUR PAIN TRESHOLD. \n ENJOY THE BLACKOUT.", 2);
                    GameController.Instance.StartCoroutine(GameController.Instance.dayController.resetDayIn(3.5f, true));
                    
                }


            }

            SoundsEffectsManager.Instance.Play(playerDeathSFX);
            dropRegents();
            isDeadKnown = true;
        }

        //Dispatcher.PlayerEvents.OnDeath.Invoke(instigator);
    }

    //how to drop the materials we have on the ground around us when we die
    public void dropRegents()
    {
        //get a reference to the material manager that stores info about all the materials we are holding
        MaterialManager matManager = GameController.Instance.materialManger;
        MaterialPickup p;
        GameObject tempMat;
        int i = 0;
        //how far away from our corpse we can drop the materials
        float dropDistanceX = 15;
        
        
        //go through each cinnamon item we have
        for (i = 0; i < matManager.qCinnamon; i++)
        {
            //generate a new pickup, set as cinnamon
            tempMat =  ObjectPool.Instance.useObject(ObjectPool.Instance.pickupPrefab);
            tempMat.GetComponent<MaterialPickup>().SetAsCinnamon();
            //sets position randomly within a circle around the player, with the radius of drop distance
            tempMat.transform.position = (Vector2)transform.position + (Random.insideUnitCircle.normalized * dropDistanceX);
        }
        for (i = 0; i < matManager.qLavender; i++)
        {
            tempMat = ObjectPool.Instance.useObject(ObjectPool.Instance.pickupPrefab);
            tempMat.GetComponent<MaterialPickup>().SetAsLavender();
            tempMat.transform.position = (Vector2)transform.position + (Random.insideUnitCircle.normalized * dropDistanceX);
        }
        for (i = 0; i < matManager.qShrooms; i++)
        {
            tempMat = ObjectPool.Instance.useObject(ObjectPool.Instance.pickupPrefab);
            tempMat.GetComponent<MaterialPickup>().SetAsShrooms();
            tempMat.transform.position = (Vector2)transform.position + (Random.insideUnitCircle.normalized * dropDistanceX);
        }
        for (i = 0; i < matManager.qOranges; i++)
        {
            tempMat = ObjectPool.Instance.useObject(ObjectPool.Instance.pickupPrefab);
            tempMat.GetComponent<MaterialPickup>().SetAsOranges();
            tempMat.transform.position = (Vector2)transform.position + (Random.insideUnitCircle.normalized * dropDistanceX);
        }
        //reset quantities to 0. 
        matManager.qCinnamon = 0;
        matManager.qLavender = 0;
        matManager.qOranges = 0;
        matManager.qShrooms = 0;
        //re-display the material quantities
        matManager.UpdateMaterialDisplays();

    }


    /////////////////////////////////////////////////////////////


    //do five ticks of 2 damage over 2.5 seconds
    public IEnumerator poisonTick()
    {

        if (IsPoisoned)
            yield break;

        IsPoisoned = true;

        yield return new WaitForSeconds(2f);
        TakeDamage(2);
        yield return new WaitForSeconds(2f);
        TakeDamage(2);
        yield return new WaitForSeconds(2f);
        TakeDamage(2);
        yield return new WaitForSeconds(2f);
        TakeDamage(2);
        yield return new WaitForSeconds(2f);
        TakeDamage(2);

        IsPoisoned = false;

    }

    public void playCastAnim()
    {
        Animator.SetTrigger("Cast");
    }

    //apply damage after a defined delay
    public IEnumerator DelayedDamage(float _amount, float _time)
    {
        yield return new WaitForSeconds(_time);
        TakeDamage(_amount);
    }

    public void Kill()
    {
        HandleDeath(null);
    }

    //simply waits for some time then resets day, doesn't handle changing into wolf
    public IEnumerator WerewolfEffect()
    {

        yield return new WaitForSeconds(5f);

        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3, true)
            );

    }

    //simply waits for some time then resets day, displays message, doesn't handle changing into frog
    public IEnumerator FrogEffect()
    {
        //reduce movement speed
        GetComponent<CharacterController2D>().Speed *= 0.1f;

        yield return new WaitForSeconds(5f);

        ds.show("YOU SPENT THE DAY AS A FROG. SHAME :(", 2);

        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3, true)
            );

    }

    //simply waits for some time then resets day, displays message, doesn't handle changing into wolf
    public IEnumerator FlowerEffect()
    {
        GetComponent<CharacterController2D>().Speed = 0;
        //ds.show("YOU ARE A FLOWER NOW :/", 1);
        // TODO: TURN YOURSELF INTO A FROG
        yield return new WaitForSeconds(5f);
        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3, true)
            );

    }

    //slow down, apply damage, wait, resume speed, turn off ice particle effect
    public IEnumerator ChillEffect(GameObject _chill)
    {
        GetComponent<CharacterController2D>().Speed *= 0.7f;
        TakeDamage(15);
        // TODO: add permanent chill effect on player
        yield return new WaitForSeconds(3f);

        _chill.SetActive(false);

    }

    //called when fireball hits target. Adds damage and deactivates the fireball effect.
    public void fireballComplete(GameObject fball)
    {
        TakeDamage(30);
        fball.SetActive(false);

    }






}