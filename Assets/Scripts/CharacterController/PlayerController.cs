﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OneTrueLove
{
    //legacy controller?
    [RequireComponent(typeof(GenericUnit))]
    public class PlayerController: MonoBehaviour
    {
        private GenericUnit unit;

        private void Awake()
        {
            unit = GetComponent<GenericUnit>();
        }

        private void Update()
        {
            //if you're dead, stop walking.
            if (!unit.IsAlive)
            {
                GetComponentInChildren<Animator>().SetBool("IsWalking", false);
                return;
            }
                
            //if we are paused etc, don't do the regular control function calls
            if (GameController.Instance.currentState != GameState.DURING)
                return;

            //get a direction based on the keyboard input
            Vector3 direction = new Vector3();
            direction.x = Input.GetAxis("Horizontal");
            direction.y = Input.GetAxis("Vertical");
            //call the move method in GenericUnit in order to move
            unit.Move(direction);
        }
    }
}
