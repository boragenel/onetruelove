﻿using OneTrueLove;
using UnityEngine;
using DG.Tweening;
using System.Collections;
using com.ootii.Messages;

[RequireComponent(typeof(CharacterController2D))]
public class EnemyUnit: GenericUnit
{
    public AudioClip werewolfDieSFX;
    

    DialogSystem ds;
    
    Transform player;
    Animator animator;
    public bool dontResetAtTheEndOfTheDay = false;
    //reference to recipeBook so that we can update it if any potions used on player is the first time they've been used
    public RecipeBookManager _recipeBookManager;

    [Header("Enemy Type")]
    public bool isWerewolf = false;
    public bool isFrog = false;

    //reference needed so that we can rewind days if we use rewind on an enemy
    private DayController dayController;

    public float regentDropChance = 1f; // between 0 and 1 (design choice - set to 100% for now)



    protected override void Awake()
    {
        base.Awake();
        //sets intitial values, and destroys and non perm werewolves and sets all to blobs, switches on colliders and animations
        InitDefaults();
        gameObject.layer = LayerMask.NameToLayer("Enemy");
        //reference to the dialog system as we need display messages when different potions are applies to the different enemies
        ds = GameController.Instance.dialogSystem;
        dayController = GameController.Instance.dayController;
        player = GameController.Instance.player.transform;
        _recipeBookManager = FindObjectOfType<RecipeBookManager>();
        //Subscriptions
        Dispatcher.GameEvents.OnDayStart += DayStart;
        OnDamageTaken += DamageTaken;
        OnDeath += HandleDeath;
        Dispatcher.GameEvents.OnEarthquake += getDamageFromEarthquake;

        OnPotionEffect += PotionEffect;
    }

    private void OnEnable()
    {
        Controller.Speed = Speed;

        Damage = INIT_DAMAGE;
        Health.Max = INIT_HEALTH;
        Health.Current = Health.Max;
    }

    private void OnDestroy()
    {
        //UNSUB
        Dispatcher.GameEvents.OnDayStart -= DayStart;
        OnDamageTaken -= DamageTaken;
        OnDeath -= HandleDeath;
        OnPotionEffect -= PotionEffect;
    }

    //this is the old handle click method
    void PotionEffect(PotionType type)
    {

        player.GetComponent<PlayerUnit>().playCastAnim();
        GameController.Instance.cursorPotion.SetActive(false);

        //work out difference in distance between player and this enemy, turn it degrees and minus 90 off of it for some reason. Get the angle from player to enemy essesntially.
        float angleBetween = (Mathf.Atan2(player.position.y - transform.position.y, player.position.x - transform.position.x) * Mathf.Rad2Deg) - 90;

        //dont play a sound if there is no potion current equipped
        if (type != PotionType.NONE)
        {
            SoundsEffectsManager.Instance.Play(VialHitSFX);
        }
        //the things to do based on the different potions effect
        switch (type)
        {
            case PotionType.DMG_FIREBALL:
                //get hold of the fireball particles
                GameObject fball = GameController.Instance.Fireball;
                fball.SetActive(true);
                //originate the fireball from the player
                fball.transform.position = player.position;

                //set the angle of fireball based on angle between player and enemy
                fball.transform.eulerAngles = new Vector3(0, 0, angleBetween);
                //work out how long it'll take to get to the enemy in seconds based on the distance away
                float fireDuration = Mathf.Abs((player.position - transform.position).magnitude / 60f);
                //tween (animate) the fireball between the two positions, and when finished run the fireballComplete function.
                fball.transform.DOMove(transform.position, fireDuration).SetEase(Ease.Linear).OnComplete(() => fireballComplete(fball));
                //is this to stop the enemy moving while fire ball approaches, based on if frog or normal enemy
                if (GetComponent<DummyEnemyAI>() != null)
                GetComponent<DummyEnemyAI>().conciousTimer = fireDuration;
                else if (GetComponent<DummyFrogAI>() != null)
                    GetComponent<DummyFrogAI>().conciousTimer = fireDuration;
                break;
            case PotionType.DMG_CHILL:
                //gets particle effect
                GameObject chill = GameController.Instance.Chill;
                chill.SetActive(true);
                chill.transform.position = transform.position;
                //applies damage and slow down immediately. After 3 seconds, resumes to normal speed.
                GameController.Instance.StartCoroutine(ChillEffect(chill));
                break;
            case PotionType.DMG_EARTH:
                //stops movement for 2 seconds on the enemy
                CameraShake.Shake(7, 2f);
                if (GetComponent<DummyEnemyAI>() != null)
                    GetComponent<DummyEnemyAI>().conciousTimer = int.MaxValue;
                else if (GetComponent<DummyFrogAI>() != null)
                {
                    GetComponent<DummyFrogAI>().conciousTimer = int.MaxValue;
                }
                Dispatcher.GameEvents.OnEarthquake.Invoke();
                //apply damage to the enemy after 1 seconds

                GameController.Instance.StartCoroutine(DelayedDamage(15, 1f));
                break;
            case PotionType.DMG_LOVE:
                //deactivates the enemy, cannot return to consciousness
                if(GetComponent<DummyEnemyAI>() != null)
                    GetComponent<DummyEnemyAI>().conciousTimer = int.MaxValue;
                else if (GetComponent<DummyFrogAI>() != null)
                    GetComponent<DummyFrogAI>().conciousTimer = int.MaxValue;

                //set the love particles to start and move them to the position of the enemy
                GameController.Instance.Love.SetActive(true);
                GameController.Instance.Love.transform.position = transform.position;
                GetComponentInChildren<Collider2D>().enabled = false;


                break;
            case PotionType.TRANS_FROG:
                ds.show("LOOKS LIKE YOU TURNED IT INTO A FROG... ARE YOU GONNA KISS IT?!", 2);
                //deactivate the old enemy, and instantiate a new object at the position. 
                ObjectPool.Instance.killObject(gameObject);
                GameObject frogGo = ObjectPool.Instance.useObject(ObjectPool.Instance.frogPrefab);
                frogGo.transform.position = transform.position;

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                break;
            case PotionType.TRANS_WEREWOLF:
                //deactate the object, then create a new werewolf. DPJ Not sure why we are adjusting speed and damage if the werewolf prefab is instantiated. 
                Controller.Speed = 40;
                ObjectPool.Instance.killObject(gameObject);
                Damage = 40;
                GameObject gow = ObjectPool.Instance.useObject(ObjectPool.Instance.werewolfPrefab);
                SoundsEffectsManager.Instance.Play(HowlFX);
                gow.transform.position = transform.position;

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                break;
            case PotionType.TRANS_YOU:
                Controller.Speed = 0;
                checkDropItem();
                //removing object and add new instance of the pink liam prefab
                ObjectPool.Instance.killObject(gameObject);
                GameObject gou = ObjectPool.Instance.useObject(ObjectPool.Instance.ladyLiamPrefab);
                gou.transform.position = transform.position;
                //make the enemy not respond for a very long time
                if (GetComponent<DummyEnemyAI>() != null)
                    GetComponent<DummyEnemyAI>().conciousTimer = int.MaxValue;
                else if (GetComponent<DummyFrogAI>() != null)
                    GetComponent<DummyFrogAI>().conciousTimer = int.MaxValue;


                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                break;
            case PotionType.TRANS_FLOWER:
                ds.show("WOW. WHAT A WONDERFUL FLOWER! WOULD LOOK LOVELY IN A VASE AT HOME...", 2);
                checkDropItem();
                //remove enemy and insantiate a flower prefab
                ObjectPool.Instance.killObject(gameObject);
                GameObject gor = ObjectPool.Instance.useObject(ObjectPool.Instance.rosesPrefab);
                gor.transform.position = transform.position;
                //make the enemy not respond for a very long time...
                if (GetComponent<DummyEnemyAI>() != null)
                GetComponent<DummyEnemyAI>().conciousTimer = int.MaxValue;
                else if (GetComponent<DummyFrogAI>() != null)
                { 
                    GetComponent<DummyFrogAI>().conciousTimer = int.MaxValue;
                }

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                break;
            case PotionType.HEAL_INC_MAX_HP:
                ds.show("WHAT A WISE MOVE... IT SEEMS YOU'VE MADE BLOBBA-FET MORE RESISTANT", 2);
                // TODO: heal anim
                //increase max HP of this enemy, and heal itself so that it had those extra hitpoints, not just an increase max hitpoints.
                Health.Max += 20;
                TakeDamage(-20);
                break;
            case PotionType.HEAL_TO_MAX_HP:
                ds.show("WHAT COMPASSION YOU'VE SHOWN! YOU'VE RESTORED IT'S HEALTH!", 2);
                //heal however many hp are missing from the enemies health pool
                TakeDamage(-(Health.Max - Health.Current));
                break;
            case PotionType.HEAL_INSTA_KILL:
                ds.show("THAT MADE QUICK WORK OF THAT BLOB BETTER NOT SPILL THIS POTION...", 2);
                Health.Current = 0;
                HandleDeath(null);
                IsAlive = false;
                break;
            case PotionType.HEAL_POISON:
                //poison tick just applies 2 damage every X amount of time
                StartCoroutine(poisonTick());
                ds.show("YOU'VE POISONED IT! A SLOW DEATH AWAITS...", 2);
                // TODO: add poison effect
                break;
            case PotionType.TIME_INC_MOVESPEED:
                Controller.Speed *= 1.5f;
                ds.show("OH NO, IT'S MOVING FASTER NOW!", 2);
                break;
            case PotionType.TIME_PAUSE:
                GameController.Instance.setState(GameState.PAUSED);
                //ds.show("SUDDENLY, TIME STOOD STILL... SECRET PAUSE MENU Y'ALL", 2);
                break;
            case PotionType.TIME_TELEPORT:
                //switch position of player and enemy - so does getting player transform at start mean we can ammend it and it changes the players transform too? Not just a copy? DPJ

                if (GameController.Instance.potionUsages.ContainsKey("TELEPORT"))
                    GameController.Instance.potionUsages["TELEPORT"]++;
                else GameController.Instance.potionUsages.Add("TELEPORT", 1);

                if (GameController.Instance.potionUsages["TELEPORT"] > 2)
                {
                    GameController.Instance.StartCoroutine(
                    GameController.Instance.dayController.resetGameIn(6, "DINOLAND"));
                    GameController.Instance.setState(GameState.END);
                    ds.show("TOO MUCH TELEPORTATION GOES WRONG AND YOU GO BACK IN TIME...", 2);
                    return;
                }

                Vector3 _pos = transform.position;
                transform.position = player.transform.position;
                player.transform.position = _pos;
                break;
            case PotionType.TIME_REWIND:
                

                if (GameController.Instance.potionUsages.ContainsKey("REWIND"))
                    GameController.Instance.potionUsages["REWIND"]++;
                else GameController.Instance.potionUsages.Add("REWIND", 1);

                if (GameController.Instance.potionUsages["REWIND"] > 2)
                {
                    GameController.Instance.StartCoroutine(
                    GameController.Instance.dayController.resetGameIn(5, "DINOLAND"));
                    GameController.Instance.setState(GameState.END);
                    ds.show("TOO MUCH REWINDING GOES WRONG AND YOU REWIND MILLIONS OF YEARS...", 2);
                    return;
                }

                ds.show("THE HANDS ON THE CLOCK START TURNING BACKWARDS... WHAT THE...", 2);
                //set the game to reset in 3 seconds, whilst adding 1 extra to the day once the reinitialiation has done.
                GameController.Instance.StartCoroutine(
          GameController.Instance.dayController.rewindDayIn(3)
          );
                break;
        }

        //update array of discovered potions
        if (type != PotionType.NONE && type != PotionType.TIME_PAUSE && GameController.Instance.discoveredPotions[type] == false)
        {
            Debug.Log("First time discovering potion, adding to recipe book!");
            GameController.Instance.discoveredPotions[type] = true;
            //call the recipe manager to handle new update
            _recipeBookManager.AddRecipeToBook(type);
        }

        //face the player towards the place that was clicked.
        player.transform.eulerAngles = new Vector3(0, 0, angleBetween);

        //reset the potion equipped to be none
        GameController.Instance.curDraggedPotion.type = PotionType.NONE;

    }

    private void Update()
    {
        float dist = Vector3.Distance(player.transform.position, transform.position);
        if ( GameController.Instance.currentState != GameState.DURING)
        {
            if(WalkingAudio != null && WalkingAudio.isPlaying)
                WalkingAudio.Stop();
            return;
        } else
        {
            if(WalkingAudio != null)
            {
                if(!WalkingAudio.isPlaying)
                    WalkingAudio.Play();

                WalkingAudio.volume = ((40f - dist) / 40f) * 1f;
                
            }
        }

        //so if we are past 9 o clock, not a werewolf and 1 in 2000 chance (slowly start turning all enemies into wolves -)
        if( dayController.hours > 9 && !isWerewolf && Random.Range(1,2000) < 2 )
        {
            //CREATE A WEREWOLF, HOOOOOWL! TODO:  PLAY A HOWL NOISE DURING THIS
            ObjectPool.Instance.killObject(gameObject);
            GameObject gow = ObjectPool.Instance.useObject(ObjectPool.Instance.werewolfPrefab);
            gow.transform.position = transform.position;
            
            if( dist < 200) {
                float vol = (200 - dist) / 200f;
                    if (vol < 0.1f)
                    vol = 0.1f;
                SoundsEffectsManager.Instance.Play(HowlFX, vol, Random.Range(1f,1.6f));
            }

        }
    }

    //set up defaults for char, called from start method and start of day
    void InitDefaults()
    {
        

        gameObject.SetActive(true);
        if (Animator != null)
            Animator.Play("Idle");
        Controller.enabled = true;
        Controller.Speed = Speed;

        Damage = INIT_DAMAGE;
        Health.Max = INIT_HEALTH;
        Health.Current = Health.Max;
        IsAlive = true;
        
        //get rid of any wolves and froggers
        if ((isWerewolf && !dontResetAtTheEndOfTheDay) || isFrog)
        {
            
            ObjectPool.Instance.killObject(gameObject);
        }
        else
            GetComponentInChildren<SpriteRenderer>().sprite = GameController.Instance.blob;
        //reenable colliders 
        GetComponentInChildren<Collider2D>().enabled = true;

    }

    //method to run when day start event is broadcast
    void DayStart()
    {
        InitDefaults();
        //why does this get done here? is it being done multiple times? DPJ
        GameController.Instance.changeScene(PortalSceneType.LIAM_HOUSE);
    }

    void DamageTaken(float amount, object source)
    {

    }

    private void getDamageFromEarthquake()
    {
        
        Vector3 earthquakeOrigin = GameController.Instance.mainCam.ScreenToWorldPoint(Input.mousePosition);
        if( Vector3.Distance(earthquakeOrigin,transform.position) < 80 )
        {
            TakeDamage(10);
        }
    }

    //DPJ - where is loot dropping now handled? only on flower or self potion?
    void HandleDeath(object instigator)
    {
        /*
        if (Animator != null)
            Animator.SetTrigger("Faint");
        Controller.enabled = false;
        */
        checkDropItem();
        ObjectPool.Instance.killObject(gameObject);
        
        GameController.Instance.deathParticle.transform.parent.position = transform.position;
        GameController.Instance.deathParticle.Emit(30);

        if (isWerewolf)
            SoundsEffectsManager.Instance.Play(werewolfDieSFX,0.6f);
        else if (deathSFX != null)
            SoundsEffectsManager.Instance.Play(deathSFX, 0.6f);

    }



    /////////////////////////////////////////////////////////////


    //do five ticks of 2 damage over 2.5 seconds
    public IEnumerator poisonTick()
    {
        yield return new WaitForSeconds(0.5f);
        TakeDamage(2);
        yield return new WaitForSeconds(0.5f);
        TakeDamage(2);
        yield return new WaitForSeconds(0.5f);
        TakeDamage(2);
        yield return new WaitForSeconds(0.5f);
        TakeDamage(2);
        yield return new WaitForSeconds(0.5f);
        TakeDamage(2);
    }

    //apply damage after a defined delay
    public IEnumerator DelayedDamage(float _amount, float _time)
    {

        yield return new WaitForSeconds(_time);
        TakeDamage(_amount);

    }

    //slow down, apply damage, wait, resume speed, turn off ice particle effect
    public IEnumerator ChillEffect(GameObject _chill)
    {
        Controller.Speed *= 0.5f;
        TakeDamage(15);
        yield return new WaitForSeconds(3f);
        Controller.Speed *= 2;
        _chill.SetActive(false);

    }

    //called when fireball hits target. Adds damage and deactivates the fireball effect.
    public void fireballComplete(GameObject fball)
    {
        TakeDamage(30);
        fball.SetActive(false);

    }

    //spawns a random material based on the regant drop chance
    public void checkDropItem()
    {
        if (Random.Range(0f, 1f) < regentDropChance)
        {
            GameObject go = ObjectPool.Instance.useObject(ObjectPool.Instance.pickupPrefab);
            go.transform.position = transform.position;

        }
    }


}