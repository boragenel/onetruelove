﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using OneTrueLove;

//base class for oTL, enemy and player units
[RequireComponent(typeof(CharacterController2D))]
public class GenericUnit : MonoBehaviour {

    public AudioClip VialHitSFX;
    public AudioClip deathSFX;
    public AudioClip HowlFX;

    [Header("Character Attributes")]
    public Health Health = new Health(100);
    public float Speed = 10f;
    public float Damage = 5f;
    public float INIT_DAMAGE;
    public float INIT_HEALTH;

      
    public bool IsAlive = true;
    public bool IsConfused = false;
    public bool IsPoisoned = false;

    [Header("Audio")]
    public AudioMixerGroup EffectsMixer;
    public AudioClip DamageTakenSFX;
    public AudioClip WalkingSFX;

    [Header("Components")]
    public Animator Animator;

    //Events - DPJ what does the delegate thing do?
    public event Action<object> OnDeath = delegate { };
    public event Action<float, object> OnDamageTaken = delegate { };
    public event Action<GenericUnit> OnAttack = delegate { };
    public event Action OnSpawn = delegate { };
    public event Action<PotionType> OnPotionEffect = delegate { };

    //Cached Components
    protected AudioSource DamageTakenAudio;
    protected AudioSource WalkingAudio;
    protected CharacterController2D Controller;

    protected virtual void Awake()
    {
        gameObject.layer = LayerMask.NameToLayer("Character");
        //set speed based on whats put in inspector against the character controller2d
        Controller = GetComponent<CharacterController2D>();
        Controller.Speed = Speed;

        if (Animator != null)
            Animator.Play("Idle");

        //add the sound for taking damage to the audio controller that's in the scene called EffectsMixer.
        if (DamageTakenSFX != null)
        {
            DamageTakenAudio = gameObject.AddComponent<AudioSource>();
            DamageTakenAudio.outputAudioMixerGroup = EffectsMixer;
        }
        //add and setup the sound for walking to the audio controller that's in the scene called EffectsMixer.
        if (WalkingSFX != null)
        {
            WalkingAudio = gameObject.AddComponent<AudioSource>();
            WalkingAudio.outputAudioMixerGroup = EffectsMixer;
            WalkingAudio.loop = true;
            WalkingAudio.playOnAwake = true;
            WalkingAudio.clip = WalkingSFX;
            WalkingAudio.mute = true;
            WalkingAudio.Play();
        }
        //subscribe
        Dispatcher.GameEvents.OnPause += DisableMovement;
        Dispatcher.GameEvents.OnResume += EnableMovement;
    }

    private void OnDestroy()
    {
        //UNSUB
        Dispatcher.GameEvents.OnPause -= DisableMovement;
        Dispatcher.GameEvents.OnResume -= EnableMovement;
    }

    private void Start()
    {
        //what subs to this? DPJ
        OnSpawn.Invoke();
    }

    public void SetSpeed(float speed)
    {
        Controller.Speed = speed;
    }

    public float GetSpeed()
    {
        return Controller.Speed;
    }

    //function that will invoke the "ondeath" after setting is alive to false and health to 0
    public void Kill(object instigator)
    {
        if (!IsAlive)
            return;
        IsAlive = false;
        Health.Current = 0;
        OnDeath.Invoke(instigator);
    }

    //this will call the subclasses "handle click" function (now know as potionEffect).
    public void ApplyPotion(PotionType type)
    {

       
        OnPotionEffect.Invoke(type);
    }

    //to handle taking daamge
    public void TakeDamage(float damage, object source = null)
    {
        //if already dead, don't bother!
        if (!IsAlive)
            return;
        if (DamageTakenAudio != null)
        {
            DamageTakenAudio.PlayOneShot(DamageTakenSFX);
            DamageTakenAudio.pitch = UnityEngine.Random.Range(0.9f, 1.1f);
            //if()

        }


        
        //reduce the units health
        Health.Current -= damage;
        //broadcast that it's been damaged and by who/what
        OnDamageTaken.Invoke(damage, source);



        //set scrolling text for the damage applies
        GameController.Instance.damageInfoManager.Show(transform.position, damage);

        // set to be dead if relevent 
        if (Health.Current <= 0)
        {
            IsAlive = false;
            Health.Current = 0;
            OnDeath.Invoke(source);
        }
    }

    //call the move method in the charcontroller2d, set walking animation, and invert direction if confused state is applies
    public void Move(Vector3 direction)
    {
        if (Controller.enabled)
        {
            if (IsConfused)
                direction *= -1;
            Controller.Move(direction.normalized);
            if (Animator != null)
                Animator.SetBool("IsWalking", Controller.IsWalking);
        }
        if (WalkingAudio != null)
            WalkingAudio.mute = !Controller.IsWalking;
    }

    //invoke attack action, and take damage.
    public virtual void Attack(GenericUnit target)
    {
        OnAttack.Invoke(target);
        target.TakeDamage(Damage, this);
    }

    //disable the controller component on the player when we do not want movement to occur (such as on scene transition)
    void DisableMovement()
    {
        Controller.enabled = false;
    }

    
    void EnableMovement()
    {
        Controller.enabled = true;
    }
}

//DPJ - is this just so we can access this by doing Unit.Health? Is that why we define this as a class, within a class?
[Serializable]
public class Health
{
    [SerializeField]
    public float Max = 100;
    [SerializeField]
    public float Current = 100;

    //for increasing max HP and healing to that max amount
    public Health(float max)
    {
        Max = max;
        Current = max;
    }

    //this is for when we increase max HP and want to heal to a custom amount (maybe not max)
    public Health(float max, float current)
    {
        Max = max;
        Current = current;
    }

    //setting hp to percentage, useful for different behaviours / displays based on percentage based info
    public float ToPercent()
    {
        if (Current == 0f)
            return Current;
        return Current / Max;
    }
}
