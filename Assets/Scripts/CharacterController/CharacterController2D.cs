﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class CharacterController2D: MonoBehaviour
{
    public float Speed = 20f;

    [Header("Collision Detection")]
    public float LinecastAngle = 45f;
    public int LinecastSteps = 10;

    [Header("Rotation")]
    public bool lookAtMovingDirection = true;
    public GameObject charObject;

    private const float CollisionThreshold = 0.5f;

    public bool IsWalking {
        get { return _isWalking; }
        protected set
        {
            _isWalking = value;
        }
    }

    private bool _isWalking = false;

    private CircleCollider2D _collider;

    struct LinecastCheckResult
    {
        public bool HasCollided;
        public float MaximumDistance;
    }

    private void Awake()
    {
        _collider = GetComponent<CircleCollider2D>();
    }

    private void OnDisable()
    {
        IsWalking = false;
    }

    //this is called from the generic unit controller
    public void Move(Vector3 direction)
    {
        direction.z = 0;

        // > than 0 but keeping in mind rounding errors
        if (Vector3.Distance(direction, Vector3.zero) > float.Epsilon)
        {
             
            float angle = 0f;
            //direction is the way you'r facing. if you're facing left...
            if (direction.x < 0)
                angle = 360 - (Mathf.Atan2(direction.x, -direction.y) * Mathf.Rad2Deg * -1);
            else
                //if you're facing right
                angle = Mathf.Atan2(direction.x, -direction.y) * Mathf.Rad2Deg;
            //set the player rotation based on where we are meant to be moving
            transform.rotation = Quaternion.Euler(0f, 0f, angle);
            
            //DPJ not to sure when this would ever be false?
            if( !lookAtMovingDirection && charObject != null)
                charObject.transform.localRotation = Quaternion.Euler(0f, 0f, -angle);
            //handles the actual movement now that direction is set
            MoveFullArc(direction);
            IsWalking = true;
        }
        else
            IsWalking = false;
    }

    private void MoveFullArc(Vector3 direction)
    {
        //make a list to store the results of all the line casts that will be done to detect if there is something infront of us and how far from us it is
        List<LinecastCheckResult> casts = new List<LinecastCheckResult>(LinecastSteps);
        
        //detect in 10 sets of 4.5 degrees, to check in total a 45 degree cone infront of the player
        //angle delta is how much to rotate between each line cast
        float angleDelta = LinecastAngle / LinecastSteps;
        //we start on 0.5 because we want to sweet from left to right infront of player, so 22.5 degrees either side
        for (float angle = -LinecastAngle * 0.5f; angle < LinecastAngle * 0.5f; angle += angleDelta)
        {
            //check if there is a collision from the current player along the cast line
            var res = CheckCollision(-DirFromAngle(angle, false));
            casts.Add(res);
        }
        if (!casts.Any(c => c.HasCollided))
        {
        
            transform.position += Speed * direction * Time.deltaTime;
        }
        else
        {
        
            var min = casts.Min(c => c.MaximumDistance);
            transform.position += min * direction * Time.deltaTime;
        }
    }

    private LinecastCheckResult CheckCollision(Vector3 direction)
    {
        //DPJ - I believe this is a way of setting how far we want to look along this angle, based on the colider radius of player
        var targetPos = (direction * Speed * Time.deltaTime) + (direction * _collider.radius);
        //draw red line in scene view
        Debug.DrawLine(transform.position, transform.position + targetPos, Color.red, Time.deltaTime);
        //check if there is a collision, based on the layer of the object and of course the collider radius and angle/endpoint of our line sensor thing. DPJ doesnt know what ~ operation is
        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, (Speed * Time.deltaTime) + _collider.radius, ~(1 << gameObject.layer));
        //if you did collide on that path
        if (hit.collider != null)
        {
            //work out how far away it was from our current position
            var dist = (new Vector3(hit.point.x, hit.point.y) - transform.position).magnitude;
            //check if that position is within our collider
            if (dist < _collider.radius)
                //return that we have collided and we are upon it
                return new LinecastCheckResult
                {
                    HasCollided = true,
                    MaximumDistance = 0
                };
            //otherwise return that we have hit but say how far away from it we are (whilst taking into account if things are close enough (collision threshold) to treat it the same as hitting it
            return new LinecastCheckResult
            {
                HasCollided = true,
                MaximumDistance = (dist - _collider.radius) > CollisionThreshold ? dist - _collider.radius : 0
            };
        }
        else
            //we havennt collided. Return speed as thats how far we should move
            return new LinecastCheckResult
            {
                HasCollided = false,
                MaximumDistance = Speed
            };
    }

    //DPJ, gives the angle in relation to players current angle (player at 60, checking -4.5 would mean 55.5 angle. 
    public Vector3 DirFromAngle(float angle, bool global = true)
    {
        if (!global)
            angle += transform.eulerAngles.z;

        return new Vector3(-Mathf.Sin(angle * Mathf.Deg2Rad), Mathf.Cos(angle * Mathf.Deg2Rad), 0);
    }
}