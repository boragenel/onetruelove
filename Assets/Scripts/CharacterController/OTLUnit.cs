﻿using OneTrueLove;
using UnityEngine;
using DG.Tweening;
using System;
using System.Collections;

[RequireComponent(typeof(CharacterController2D))]
public class OTLUnit: GenericUnit
{
    DialogSystem ds;

    

    public bool isDeadKnown = false;
    public GameObject frogSelf;
    public GameObject werewolfSelf;
    public GameObject rosesSelf;
    public GameObject liamSelf;

    public PlayerUnit player;

    //reference to reciepe book incase first use of spell is on OTL (so we add to recipe book) (DPJ - should bne done in base class)
    public RecipeBookManager _recipeBookManager;

    protected override void Awake()
    {
        base.Awake();
        //set defaults / animations / different visuals off
        InitDefaults();

        gameObject.layer = LayerMask.NameToLayer("Enemy");
        ds = GameController.Instance.dialogSystem;
        player = GameController.Instance.player.GetComponent<PlayerUnit>();

        //Subscriptions
        Dispatcher.GameEvents.OnDayStart += DayStart;
        OnDamageTaken += DamageTaken;
        OnDeath += HandleDeath;
        OnPotionEffect += PotionEffect;
    }

    private void OnDestroy()
    {
        //UNSubscriptions
        Dispatcher.GameEvents.OnDayStart -= DayStart;
        OnDamageTaken -= DamageTaken;
        OnDeath -= HandleDeath;
        OnPotionEffect -= PotionEffect;
    }

    //What to do when a potion is used upon the OTL
    void PotionEffect(PotionType type)
    {

        if (!IsAlive)
            return;

        //switch off the potion curser and play casting animation
        GameController.Instance.cursorPotion.SetActive(false);
        player.playCastAnim();

        switch (type)
        {

            case PotionType.DMG_FIREBALL:
                //get hold of the fireball particles
                GameObject fball = GameController.Instance.Fireball;
                fball.SetActive(true);
                SoundsEffectsManager.Instance.Play(deathSFX,0.5f);
                //originate the fireball from the player
                fball.transform.position = transform.position;
                //hard code duration as distance tends to be roughly consistent
                float fireDuration = 0.3f;
                fball.transform.DOMove(transform.position + (Vector3.up * 2), fireDuration).SetEase(Ease.Linear).OnComplete(() => fireballComplete(fball));
                ds.show("THE POTION BURSTS INTO FLAMES AT THE FEET OF YOUR ONE TRUE LOVE!", 2);
                //this will sort out changing music based on ending, then loading the ending canvas after a set duration
                GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetGameIn(4, type)
            //GameController.Instance.UnlockAchievement(type.ToString, 1);
            );

                break;
            case PotionType.DMG_CHILL:
                //gets particle effect
                GameObject chill = GameController.Instance.Chill;
                chill.SetActive(true);
                SoundsEffectsManager.Instance.Play(deathSFX, 0.5f);
                chill.transform.position = transform.position;
                GameController.Instance.StartCoroutine(ChillEffect(chill));
                ds.show("THE TEMPERATURE OF THE ROOM PLUMITS, AS YOU START TO QUESTION WHAT YOU'VE DONE...", 2);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(4, type)
           );
                break;
            case PotionType.DMG_EARTH:
                //shake camera and do delayed damage
                CameraShake.Shake(7, 2f);
                
                SoundsEffectsManager.Instance.Play(deathSFX, 0.5f);
                // TODO: damage all the enemies in a radius
                ds.show("NATALIE STARTS TO SHAKE... HER BODY IN A FIT... SOMETHINGS REALLY WRONG HERE...", 2);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(4, type)
           );
                break;
            case PotionType.DMG_LOVE:

                ds.show("NATALIE LOOKS AT YOU, WITH A LOOK IN HER EYE YOU'VE NEVER SEEN BEFORE", 2);
                // TODO: WIN!
                GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetGameIn(6, type)
            );
                break;
            case PotionType.TRANS_FROG:
                //turns on frog game object sprite within the OTL GO, and turn off the sprite
                frogSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
                ds.show("SHE INSTANTLY TRANSFORMS INTO A FROG... MAYBE YOU COULD GIVE HER A KISS?", 2);

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(6, type)
           );

                break;
            case PotionType.TRANS_WEREWOLF:
                //enable the werewolf child, disable the OTL sprite
                werewolfSelf.SetActive(true);
                SoundsEffectsManager.Instance.Play(HowlFX);
                transform.GetChild(0).gameObject.SetActive(false);
                //little red riding hood reference
                ds.show("BUT NATALIE, WHAT BIG EYES YOU HAVE! \n... AND WHAT BIG EARS YOU HAVE! \n ...AND WHAT BIG TEETH YOU HAVE!", 3);

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(5, type)
           );

                break;
            case PotionType.TRANS_YOU:
                //enable liam sprite, disable OTL sprite
                liamSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
                ds.show("HER BEAUTY MELTS AWAY, AS SHE IS TRANSFORMED INTO... YOU? YET YOU STILL FEEL DEEPLY IN LOVE WITH... YOURSELF.", 3);

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                //sets music and sets relevent ending in x amount of tim
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(6, type)
           );
                break;
            case PotionType.TRANS_FLOWER:
                //switch sprites
                rosesSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
                ds.show("From her feet lash out several roots, thrashing down through the floor boards and rooting theirselves in the ground... ".ToUpper(), 3);

                GameController.Instance.transformParticle.transform.parent.position = transform.position;
                GameController.Instance.transformParticle.Emit(30);

                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
          GameController.Instance.dayController.resetGameIn(6, type)
          );
                break;
            case PotionType.HEAL_INC_MAX_HP:
                //add to total health and then heal that same amount with inverted damage function
                Health.Max += 20;
                TakeDamage(-20);
                ds.show("SHE LOOKS BEMUSED, THOUGH YOU SENSE SHE IS MORE RESILIENT THAN EVER BEFORE", 3);
                IsPoisoned = false;
                // TODO: heal anim, dialog
                break;
            case PotionType.HEAL_TO_MAX_HP:
                ds.show("ANY SIGNS OF ILL HEALTH IN NATALIE HAVE NOW DISAPPEARED", 2);
                //restore the difference between max hp and current HP
                TakeDamage(-(Health.Max - Health.Current));
                IsPoisoned = false;
                break;
            case PotionType.HEAL_INSTA_KILL:
                Health.Current = 0;
                ds.show("HER BODY FALLS INSTANTLY TO THE GROUND. SHE'S STOPPED BREATHING. DAMN.", 2);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
         GameController.Instance.dayController.resetGameIn(6, type)
         );
                break;
            case PotionType.HEAL_POISON:
                IsPoisoned = true;
                StartCoroutine(poisonTick());
                // TODO: add poison effect
                ds.show("SUDDENLY NATALIE DOESN'T LOOK SO WELL... ", 2);
                //sets music and sets relevent ending in x amount of time
                break;
            case PotionType.TIME_INC_MOVESPEED:
                //potion of haste, reduce days remaining
                GameController.Instance.dayController.currentDay++;
                //if the days are now 0 or less, load the ending :(
                if (GameController.Instance.dayController.currentDay >= 8)
                {
                    GameController.Instance.StartCoroutine(LeavingTooSoon());

                }
                else
                {
                    ds.show("WITH HER NEWLY AQUIRED ENERGY, SHE CONSIDERS LEAVING TOWN A DAY EARLIER THAN SCHEDULED", 2);
                }
                //update the heart UI sprite
                GameController.Instance.dayController.daysLeftLabel.text = (8 - GameController.Instance.dayController.currentDay).ToString();
                break;
            case PotionType.TIME_PAUSE:
                //simply pauses game, nothing more.
                GameController.Instance.setState(GameState.PAUSED);
                break;
            case PotionType.TIME_TELEPORT:
                //teleports 10 units forward - DPJ could be improved by calculating distance between you and OTL 
                player.transform.position = transform.position;
                ds.show("YOU TELEPORT FORWARD, BUT FIND YOURSELF SUDDENLY INSIDE HER...", 2);
                SoundsEffectsManager.Instance.Play(deathSFX, 0.5f);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
                GameController.Instance.dayController.resetGameIn(6, type)
                );
                break;
            case PotionType.TIME_REWIND:

                if (GameController.Instance.potionUsages.ContainsKey("REWIND"))
                    GameController.Instance.potionUsages["REWIND"]++;
                else GameController.Instance.potionUsages.Add("REWIND", 1);

                if (GameController.Instance.potionUsages["REWIND"] > 2)
                {
                    GameController.Instance.StartCoroutine(
                    GameController.Instance.dayController.resetGameIn(5, "DINOLAND"));
                    GameController.Instance.setState(GameState.END);
                    ds.show("TOO MUCH REWINDING GOES WRONG AND YOU REWIND MILLIONS OF YEARS...", 2);
                    return;
                }

                ds.show("THE HANDS ON THE CLOCK START TURNING BACKWARDS... WHAT THE...", 2);
                //takes us back 1 day (day count) and restarts the day
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.rewindDayIn(3)
           );
                break;
        }

        //update array of discovered potions
        if (type != PotionType.NONE && type != PotionType.TIME_PAUSE && GameController.Instance.discoveredPotions[type] == false)
        {
            GameController.Instance.discoveredPotions[type] = true;
            //call the recipe manager to handle new update
            _recipeBookManager.AddRecipeToBook(type);
        }
        //reset potion type to avoid re-use
        GameController.Instance.curDraggedPotion.type = PotionType.NONE;

    }

    void InitDefaults()
    {
        gameObject.SetActive(true);
        if (Animator != null)
            Animator.Play("Idle");
        Controller.enabled = true;
        Controller.Speed = Speed;
        IsAlive = true;
        Health.Current = Health.Max;

        rosesSelf.SetActive(false);
        werewolfSelf.SetActive(false);
        frogSelf.SetActive(false);

        transform.GetChild(0).gameObject.SetActive(true);

        //GetComponent<TopdownCharacterController>().Speed = TopdownCharacterController.INIT_MOVE_SPEED;
        //DPJ unsure why we change the scene in here rather than within the game controller itself.
        GameController.Instance.changeScene(PortalSceneType.LIAM_HOUSE);
        isDeadKnown = false;
    }

    void DayStart()
    {
        InitDefaults();
        ////DPJ unsure why we change the scene in here again? It's done in Init Defaults, but really not sure why it's done in there either!
        GameController.Instance.changeScene(PortalSceneType.LIAM_HOUSE);
    }

    void DamageTaken(float amount, object source)
    {

    }

    //stop being able to move when die, set the faint animation, and send out broadcaster that we've died
    void HandleDeath(object instigator)
    {
        if (Animator != null)
            Animator.SetTrigger("Faint");
        Controller.enabled = false;
        Dispatcher.PlayerEvents.OnDeath.Invoke(instigator);

        GameController.Instance.dialogSystem.show("YOU SOMEHOW MANAGED TO POSION YOUR ONE TRUE LOVE SEVERAL TIMES!", 3);
        StartCoroutine(GameController.Instance.dayController.resetGameIn(4, "HEAL_POISON"));

    }

    /////////////////////////////////////////////////////////////

    //do five ticks of 2 damage over 2.5 seconds
    public IEnumerator poisonTick()
    {
        yield return new WaitForSeconds(2f);
        TakeDamage(2);
        yield return new WaitForSeconds(2f);
        TakeDamage(2);
        yield return new WaitForSeconds(2f);
        TakeDamage(2);
        yield return new WaitForSeconds(2f);
        TakeDamage(2);
        yield return new WaitForSeconds(0.5f);
        TakeDamage(2);
    }

    //handling the special case that we fast forward a day when already on 1 day or less
    public IEnumerator LeavingTooSoon()
    {
        ds.show("SHE MOVES SO FAST THAT SHE LEAVES THE TOWN IMMEDIATELY.", 2);
        yield return new WaitForSeconds(2f);
        GameController.Instance.endingManager.ShowEnding("OUT_OF_DAYS");

    }

    //apply damage after a defined delay

    public IEnumerator DelayedDamage(float _amount, float _time)
    {

        yield return new WaitForSeconds(_time);
        TakeDamage(_amount);

    }

    //simply waits for some time then resets day - (shouldn't. DPJ), doesn't handle changing into wolf
    public IEnumerator WerewolfEffect()
    {
        yield return new WaitForSeconds(5f);

        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3)
            );
    }

    //simply waits for some time then resets day, displays message, doesn't handle changing into frog
    public IEnumerator FrogEffect()
    {
        yield return new WaitForSeconds(5f);
        //best line ever - we don't need this though do we?
        ds.show("YOU SPENT THE DAY AS A FROG. SHAME :(", 2);
        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3)
            );
    }

    //slow down, apply damage, wait, resume speed, turn off ice particle effect
    public IEnumerator ChillEffect(GameObject _chill)
    {
        TakeDamage(15);
        // TODO: add permanent chill effect on player
        yield return new WaitForSeconds(3f);

        _chill.SetActive(false);
    }

    //called when fireball hits target. Adds damage and deactivates the fireball effect.
    public void fireballComplete(GameObject fball)
    {
        TakeDamage(30);
        fball.SetActive(false);
    }



}