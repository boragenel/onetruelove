﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CharacterController2D))]
public class CharacterController2DInspector: Editor {
    CharacterController2D controller;
    CircleCollider2D collider;

    private void OnEnable()
    {
        if (controller == null)
        {
            controller = (CharacterController2D)target;
            collider = controller.GetComponent<CircleCollider2D>();
        }
    }

    private void OnSceneGUI()
    {
        Handles.color = Color.red;
        Handles.DrawWireArc(controller.transform.position, Vector3.forward, Vector3.up, 360, collider.radius);
        Vector3 angleA = -controller.DirFromAngle(-controller.LinecastAngle * 0.5f, false);
        Vector3 angleB = -controller.DirFromAngle(controller.LinecastAngle * 0.5f, false);
        Handles.DrawAAPolyLine(controller.transform.position, controller.transform.position + angleA * collider.radius);
        Handles.DrawAAPolyLine(controller.transform.position, controller.transform.position + angleB * collider.radius);
    }


}
