﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this is linked to a button, allows you to select any potion in inspector then click button to equip the potion
public class TestPotion : MonoBehaviour {
    public PotionType typeToTest;

	// Don't have as potion vby default
	void Start () {
         typeToTest = PotionType.NONE;
    }

    //set the equipped potion to be whatever ptype that has been set in the inspector
    public void TestIt()
    {
        GameController.Instance.curDraggedPotion.type = typeToTest;
    }
}
