﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class RandomizableRectangleBorder : MonoBehaviour
{
    [Header("Generation Parameters")]
    public float rectWidth = 50f;
    public float rectHeight = 50f;
    public float borderThickness = 25f;
    public float ItemRadius = 2f;
    public int MaxTries = 20;

    public List<GameObject> listOfPrefabs;

    public List<GameObject> generatedObjects = new List<GameObject>();

    ObjectPool pool;


    void Start()
    {
        generate();
        //this will be called every new day to generated a randomized border to the level
        MessageDispatcher.AddListener("ResetDay", ResetGeneration);
    }


    public void ResetGeneration(IMessage msg)
    {
        //killAll();
        //generate();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(rectWidth, rectHeight, 1));
    }

    public void generate()
    {
        pool = ObjectPool.Instance;
        GameObject tempObject;

        ZSortedObject _zSortedObject;

        int noOfPrefabs = listOfPrefabs.Count;

        // top left -> top right
        float currentIterationX = -rectWidth / 2;
        
        //start top left, until we are at the end of the rectanlge (as 0 is central), place items and move along the item radius each iteration.
        while (currentIterationX < rectWidth / 2)
        {
            //choose a random "border" prefab (rock, tree, bush etc)
            tempObject = ObjectPool.Instance.useObject(listOfPrefabs[Random.Range(0, noOfPrefabs)]);
            tempObject.transform.parent = transform;
            //set the position to be the current position + random varience based on item radius, and on Y axis place it at the top row position, with some randomisation based on how "thick" we want the border to be
            tempObject.transform.localPosition = new Vector3(currentIterationX + Random.Range(0, ItemRadius), -(rectHeight / 2) - Random.Range(0, borderThickness));
            //we add because we want to move to the right acorss the page
            currentIterationX += ItemRadius;
            generatedObjects.Add(tempObject);

            //ensure that the ordering is based on which items are further south on the screen compared to those further north, to give the perception of depth. 
            _zSortedObject = tempObject.GetComponentInChildren<ZSortedObject>();
            if (_zSortedObject != null)
                _zSortedObject.updateZSortingAccordingtoYPos(null);

        }
        //now we are going to start doing the right side of the rectangle. 
        float currentIterationY = -rectHeight / 2;
        while (currentIterationY < rectHeight / 2)
        {
            tempObject = ObjectPool.Instance.useObject(listOfPrefabs[Random.Range(0, noOfPrefabs)]);
            tempObject.transform.parent = transform;
            //set it to be at the far right edge of rectangle plus ranndom amount based on the thickness of the forest edging. Y coordinate based on iteration + randomised value no more than item raidus
            tempObject.transform.localPosition = new Vector3((rectWidth / 2) + Random.Range(0, borderThickness), currentIterationY + Random.Range(0, ItemRadius));
            //we add as we want to move down the level
            currentIterationY += ItemRadius;
            generatedObjects.Add(tempObject);

            _zSortedObject = tempObject.GetComponentInChildren<ZSortedObject>();
            if (_zSortedObject != null)
                _zSortedObject.updateZSortingAccordingtoYPos(null);
        }

        //start at bottom right corner, come back towards the left bottom corner
        currentIterationX = rectWidth / 2;

         while (currentIterationX > -rectWidth / 2)
        {
            tempObject = ObjectPool.Instance.useObject(listOfPrefabs[Random.Range(0, noOfPrefabs)]);
            tempObject.transform.parent = transform;

            tempObject.transform.localPosition = new Vector3(currentIterationX + Random.Range(0, ItemRadius), (rectHeight / 2) + Random.Range(0, borderThickness));
            currentIterationX -= ItemRadius;
            generatedObjects.Add(tempObject);

            _zSortedObject = tempObject.GetComponentInChildren<ZSortedObject>();
            if (_zSortedObject != null)
                _zSortedObject.updateZSortingAccordingtoYPos(null);
        }

        //bottom left up to top left
        currentIterationY = rectHeight / 2;

        while (currentIterationY > -rectHeight / 2)
        {
            tempObject = ObjectPool.Instance.useObject(listOfPrefabs[Random.Range(0, noOfPrefabs)]);
            tempObject.transform.parent = transform;
            tempObject.transform.localPosition = new Vector3((-rectWidth / 2) - Random.Range(0, borderThickness), currentIterationY - Random.Range(0, ItemRadius));
            currentIterationY -= ItemRadius;
            generatedObjects.Add(tempObject);

            _zSortedObject = tempObject.GetComponentInChildren<ZSortedObject>();
            if (_zSortedObject != null)
                _zSortedObject.updateZSortingAccordingtoYPos(null);
        }

        // Updates all ZSortOders
        //MessageDispatcher.SendMessage("UpdateZSorting");

    }

    //checks an item isn't being place within radius of another object
    public bool PositionOverlaps(GameObject t)
    {
        var hit = Physics2D.OverlapCircle(t.transform.position, ItemRadius);
        if (hit != null)
        {
            return true;
        }
        return false;
    }

    //disables all generated objects
    public void killAll()
    {
        for (int i = 0; i < generatedObjects.Count; i++)
        {
            ObjectPool.Instance.killObject(generatedObjects[i]);
        }
    }
}
