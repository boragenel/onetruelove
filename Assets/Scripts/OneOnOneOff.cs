﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Function to reference from a buttons OnClick which takes enables one game object and disables the other.
/// </summary>
public class OneOnOneOff : MonoBehaviour {
    /// <summary>
    /// the game objects you wish to activate/deactivate
    /// </summary>
    public GameObject toClose;
    public GameObject toOpen;

    public AudioClip toggleSFX;

    public void SwitchActiveObjects()
    {
        toOpen.SetActive(true);
        toClose.SetActive(false);
        SoundsEffectsManager.Instance.Play(toggleSFX,0.3f);

    }
}
