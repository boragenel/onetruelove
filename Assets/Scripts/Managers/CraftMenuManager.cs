﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftMenuManager : MonoBehaviour {

    public AudioClip openCraftTableSFX;

    //bools that represent if the ingredient is current selected for mixing
    [SerializeField] private bool addShrooms;
    [SerializeField] private bool addLavender;
    [SerializeField] private bool addOranges;
    [SerializeField] private bool addCinnamon;
    //the green circle object behind the ingredients that represent visually if one has been selected
    public GameObject shroomSelect;
    public GameObject lavenderSelect;
    public GameObject orangesSelect;
    public GameObject cinnamonSelect;
    
    //reference to the games randomly initlaised recipe list
    public RecipeInit recipies;

    //access to the hot bar as we will need to add potions as we craft them to the hotbar
    public InventoryManager iManager;
    //access to the materials bar as we need to check we have enough to craft and update the amounts after crafting
    public MaterialManager mManager;

    //audio used in the brewing process
    public AudioClip BrewingSFX;
    public AudioClip MaterialAddedSFX;

	void Start () {
        //grey out any materials we don't have access to.
        UpdateCraftTableVisuals();
    }

    //the menu is displayed through enabling/disabling when we enter a trigger. So we want to update which ingredients we are able to add (based on material quantities)
    private void OnEnable()
    {
        //grey out any materials we don't have access to.
        UpdateCraftTableVisuals();
        SoundsEffectsManager.Instance.Play(openCraftTableSFX);
    }

    private void OnDisable()
    {
        SoundsEffectsManager.Instance.Play(openCraftTableSFX);
    }

    public void UpdateCraftTableVisuals()
    {
        if(mManager.qShrooms < 1)
        {
            //disbale the button from clicks
            transform.GetChild(0).GetChild(1).GetComponent<Button>().interactable = false;
            //turn off the selected background (this is useful if you've just crafted and now have none left, since green bg remains after craft otherwise)
            shroomSelect.SetActive(false);
            //set the variables that is used in the brew recipe to work out which potion is being crafted based on ingredients selected.
            addShrooms = false;
        }
        else
        {
            //ensure button is interactible, as last time menu was opened we may not have had any of that ingredient.
            transform.GetChild(0).GetChild(1).GetComponent<Button>().interactable = true;
        }

        if (mManager.qLavender < 1)
        {
            transform.GetChild(1).GetChild(1).GetComponent<Button>().interactable = false;
            lavenderSelect.SetActive(false);
            addLavender = false;
        }
        else
        {
            transform.GetChild(1).GetChild(1).GetComponent<Button>().interactable = true;
        }

        if (mManager.qOranges < 1)
        {
            transform.GetChild(2).GetChild(1).GetComponent<Button>().interactable = false;
            orangesSelect.SetActive(false);
            addOranges = false;
        }
        else
        {
            transform.GetChild(2).GetChild(1).GetComponent<Button>().interactable = true;
        }

        if (mManager.qCinnamon < 1)
        {
            transform.GetChild(3).GetChild(1).GetComponent<Button>().interactable = false;
            cinnamonSelect.SetActive(false);
            addCinnamon = false;
        }
        else
        {
            transform.GetChild(3).GetChild(1).GetComponent<Button>().interactable = true;
        }
       
    }

    //toggles the sound fx for selection, the green background, as well as the material selected state boolean 
    public void ClickShroom()
    {


        SoundsEffectsManager.Instance.Play(MaterialAddedSFX);
        //flip the active state
        shroomSelect.SetActive(!shroomSelect.activeSelf);
        addShrooms = !addShrooms;
    }

    public void ClickLavender()
    {
        SoundsEffectsManager.Instance.Play(MaterialAddedSFX);
        lavenderSelect.SetActive(!lavenderSelect.activeSelf);
        addLavender = !addLavender;
    }

    public void ClickOranges()
    {
        SoundsEffectsManager.Instance.Play(MaterialAddedSFX);
        orangesSelect.SetActive(!orangesSelect.activeSelf);
        addOranges = !addOranges;
    }

    public void ClickCinnamon()
    {
        SoundsEffectsManager.Instance.Play(MaterialAddedSFX);
        cinnamonSelect.SetActive(!cinnamonSelect.activeSelf);
        addCinnamon = !addCinnamon;
    }

    public void Brew()
    {
        SoundsEffectsManager.Instance.Play(BrewingSFX);

        //translate the four material state booleans into a string that represents 
        string recipeCode = "" + (addShrooms == true ? "1" : "0") + (addLavender == true ? "1" : "0") + (addOranges == true ? "1" : "0") + (addCinnamon == true ? "1" : "0");
        string thePotion = recipies.RecipeToPotion(recipeCode);

        //instantiate a new potion to pass to the addPotion method (which sets current active potion and adds potion to inventory
        Potion potion = new Potion();
        //set potion index to be the binary representation of the ingredients. This is stored as a string (not a byte). 
        potion.potionIndex = Convert.ToInt32(recipeCode, 2).ToString();
        //set the enum based on the thePotion, this is the effect / type of potion. Keep in mind type and recipe will not be consistent due to randomised spell book each game.
        potion.type = (PotionType)Enum.Parse(typeof(PotionType), thePotion);

        // add the potion, if there is no issues adding the potion, then update our material quantities etc. 
        if (iManager.AddPotion(potion))
        {
            //update our materials quantities
            mManager.PostBrewUpdate(recipeCode);
            UpdateCraftTableVisuals();
        }
        else
        {
            Debug.Log("Unable to create potion "+potion.potionIndex + ", for some reason");
        }           
        
    }

    /// <summary>
    /// Resets the state of all dynamic variables / objects in the craft menu. 
    /// </summary>
    public void ResetMenu()
    {
        shroomSelect.SetActive(false);
        addShrooms = false;
        lavenderSelect.SetActive(false);
        addLavender = false;
        orangesSelect.SetActive(false);
        addOranges = false;
        cinnamonSelect.SetActive(false);
        addCinnamon = false;
    }

}
