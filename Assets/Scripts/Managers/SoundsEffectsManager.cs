﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundsEffectsManager : MonoBehaviour {
    public int MaxPlayers = 30;
    public AudioMixerGroup AudioGroup;

    public AudioClip Test;

    private static SoundsEffectsManager _instance;

    //set up soft singleton
    public static SoundsEffectsManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (SoundsEffectsManager)FindObjectOfType(typeof(SoundsEffectsManager));
            }
            return _instance;
        }
    }

    private void Start()
    {
        gameObject.AddComponent<AudioSource>();
        //unsure what this is, maybe old code?
        Play(Test);
    }

    public void Play(AudioClip clip,float volume=1,float pitch=1)
    {
        var players = GetComponents<AudioSource>();
        //if there is no players playing and we aren't over the map Players amount (not sure how / why this is set)
        if(!players.Any(p => p.isPlaying) && players.Length < MaxPlayers)
        {
            
            AudioSource a = players.First();
            a.PlayOneShot(clip);
            a.volume = volume;
            a.pitch = pitch;
        }
        else if(players.Length < MaxPlayers)
        {
            var player = gameObject.AddComponent<AudioSource>();
            player.outputAudioMixerGroup = AudioGroup;
            player.PlayOneShot(clip);
            player.volume = volume;
            player.pitch = pitch;
            //if we don't have too many sounds playing, play the sounds clip. Each sound needs an audioplayer? DPJ

        }
        else
        {
            Debug.Log("Maximum audio players reached!");
        }
    }
}
