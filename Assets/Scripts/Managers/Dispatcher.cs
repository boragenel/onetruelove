﻿#pragma warning disable 0067

using System;
using System.Collections.Generic;
using UnityEngine;

namespace OneTrueLove
{
    public static class Dispatcher
    {
        public static class GameEvents
        {
            //delegate lets you assign functions on the fly, so doesn't need to be assigned to a specific function at compile time. Can assign more than one method this way
            public static Action OnPause = delegate { };
            public static Action OnResume = delegate { };
            public static Action OnDayEnd = delegate { };
            public static Action OnDayStart = delegate { };

            // Ingame
            public static Action OnEarthquake = delegate { };


            /// <summary>
            /// Fired when a potion is used, with the Potion instance and the Target as GenericUnit
            /// </summary>
            public static Action<Potion, GenericUnit> OnPotionUsed = delegate { };
        }

        public static class PlayerEvents
        {
            public static Action<MaterialPickup> OnPickup = delegate { };

            /// <summary>
            /// Fired when the player takes damage, with the amount of damage taken
            /// </summary>
            public static Action<float> OnDamageTaken = delegate { };

            /// <summary>
            /// Fired when a potion is created by the player, giving the Potion instance
            /// </summary>
            public static Action<Potion> OnPotionCreated = delegate { };
            public static Action<object> OnDeath = delegate { };
        }
    }
}