﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EndingManager : MonoBehaviour {
    //a list of different endings that need to be set up via the inspector 
    public List<EndingPanel> endPanels;
    //a reference to the default ending panel to show if there is no match on "ending type"
    public GameObject defaultEndingPanel;

    public GameObject pausePanel;

    //allow pausing (to turn down music) on the ending panel
    public void pause()
    {
        
        pausePanel.SetActive(!pausePanel.activeSelf);
        if (pausePanel.activeSelf)
        {
            Time.timeScale = 0;
        }
        else Time.timeScale = 1;
    }

    //the method called to load an ending
    public void ShowEnding(string endingString)
    {
        //the regular HUD is switched off for the display of the ending
        GameController.Instance.hudCanvas.SetActive(false);

        GameController.Instance.setState(GameState.END);

        bool hasFound = false;
        //go through each panel, if it's found a match to the ending based on the ending String, display it. 
        foreach (EndingPanel e in endPanels)
        {
            //Debug.Log(e.endingCause);
            if (e.endingCause.Contains(endingString))
            {
                e.gameObject.SetActive(true);
                hasFound = true;
                break;
            }
        }
        // if there was no match, show the default ending. (this shouldn't occur unless ending hasn't been written for the scenario)

        if (!hasFound)
        {
            defaultEndingPanel.SetActive(true);
        }
        
    }
}
