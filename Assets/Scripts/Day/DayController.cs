﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using com.ootii.Messages;
using DG.Tweening;
using UnityEngine.PostProcessing;
using OneTrueLove;

//class that controls the flow of the day, and the flow of the week
public class DayController : MonoBehaviour {


    public AudioClip morningBirds;

    public Text daysLeftLabel;
    public List<Day> days = new List<Day>();



    public AudioClip flashlightSound;

    //we count up days, despite the game showing days remaining
    public int currentDay = 1;
    public float hours = 0; // out of 24

    //this refers to 12 o clock, but tracked with a float that starts at 0 at midday
    public static float WAKE_UP_TIME = 0;
    public static float NIGHT_TIME = 9;

    public float gameTimeScale = 1;

    //THIS EXCITES ME
    public GameObject flashlight;
    //QOTSA REFERENCE!?
    public GameObject likeClockwork;
    public bool nightTimeShown = false;
    bool howlTimeShown = false;

    //array of messages that are shown randomly each morning
    public string[] morningGreetings = new string[5];

    PlayerUnit playerUnit;

    //the effect to apply to camera when night time is upon us
    public CameraFilterPack_Color_BrightContrastSaturation nightDarken;

    ColorGradingModel.Settings colorGradingSettings;

    public PostProcessingProfile profile;


    void Start () {
        MessageDispatcher.AddListener("ResetDay", ResetDay);
        //colorGradingSettings = profile.colorGrading.settings;
        playerUnit = GameController.Instance.player.GetComponent<PlayerUnit>();
    }
	
    public void ResetGame()
    {
        //to be called when game restarts from menu after a play through (or a future reset button).
        currentDay = 1;
        hours = 0;
        gameTimeScale = 10;
        nightTimeShown = false;
        howlTimeShown = false;
        days = new List<Day>();
        flashlight.SetActive(false);
    }

    //when we rewind, update the day totals accordingly then call a regular reset day
    public void RewindDay()
    {
        if (currentDay != 1)
            currentDay--;
        ResetDay(null);

    }

    //reset everything ready for a new day - back to 12, set the music, 

    public void ResetDay(IMessage msg)
    {
        flashlight.SetActive(false);
        hours = 0;
        gameTimeScale = 10;
        nightTimeShown = false;
        howlTimeShown = false;
        //love leads to different music
        GameController.Instance.Love.SetActive(false);
        //GameController.Instance.player.GetComponent<PlayerUnit>().ResetPlayer();
        //broadcast to other classes that a new day is starting
        Dispatcher.GameEvents.OnDayStart.Invoke();
        GameController.Instance.townMusic.Play();
        GameController.Instance.townMusic.volume = 0.4f;
        GameController.Instance.forestMusic.DOFade(0, 1);

        SoundsEffectsManager.Instance.Play(morningBirds,0.4f);

        

    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener("ResetDay", ResetDay);
    }


    // Update is called once per frame
    void Update () {

        if (GameController.Instance.currentState != GameState.DURING)
        {
            if( GameController.Instance.currentState == GameState.END )
            {
                hours = 1;
                flashlight.SetActive(false);
                nightDarken.Brightness = 1 - (0.5f * (hours / 12));
                nightDarken.Contrast = 1 + (0.3f * (hours / 12));
            }
            return;
        }

        //update the time on the clock
        hours += Time.deltaTime * gameTimeScale / 120f;
        //set the angle of the clock hands
        likeClockwork.transform.eulerAngles = new Vector3(0, 0, (hours / 12f) * -360);
        //turn on flashlight when it is past 7pm
        if( hours > 7 && !flashlight.activeSelf)
        {
            flashlight.SetActive(true);
            SoundsEffectsManager.Instance.Play(flashlightSound,0.4f);
        }

        //colorGradingSettings.basic.postExposure = -2.5f * (hours / 9f);
        //profile.colorGrading.settings = colorGradingSettings;

        //changes the darkness as the day goes on
            nightDarken.Brightness = 1 - (0.5f * (hours / 12));
            nightDarken.Contrast = 1 +( 0.3f * (hours / 12));

        //allows you to carry on 3 hours into the night, then displays one off message to reset the day
        if (hours > NIGHT_TIME + 3 && !nightTimeShown && playerUnit.IsAlive)
        {
            nightTimeShown = true;
            //GameController.Instance.dialogSystem.show("YOU ARE SO TIRED. YOU CLOSE YOUR EYES HOPING THE PARK RANGERS WILL TAKE YOU HOME IN THE MORNING",3);
            if( GameController.Instance.currentScene == PortalSceneType.FOREST )
                GameController.Instance.dialogSystem.show("THIS IS WOLF COUNTRY!! IT'S TOO DANGEROUS TO STAY ANY LATER, TIME TO HEAD BACK.", 4);
            else
                GameController.Instance.dialogSystem.show("IT'S TOO DARK AND YOU CAN HEAR THE WEREWOLVES HOWLING, YOU DECIDE TO GO TO BED.", 4);
            StartCoroutine(resetDayIn(4, true));
            //why do we kill the player here? DPJ
            
            

        } else
        //one off notification that the WOLVES ARE A COMING
        if ( hours > NIGHT_TIME)
        {
            if (!howlTimeShown)
            {
                howlTimeShown = true;
                GameController.Instance.dialogSystem.show("YOU HEAR SOME DISTURBING HOWLS AS IT GETS DARKER AND DARKER", 3);
            }

        } 
	}

    //resets the day after a set delay, can specify if we increase the days (when we rewind we will override this default)
    public IEnumerator resetDayIn(float _seconds,bool _increaseDay = false)
    {
        GameController.Instance.setState(GameState.TRANSITION);
        if (_seconds <= 1.2f)
            _seconds = 1.3f;
        

        //wait the passed in amount of time, saving 1.2 for the amount of time the transition takes
        yield return new WaitForSeconds(_seconds-1.2f);
        GameController.Instance.setState(GameState.TRANSITION);
        CameraTransition.Instance.Transition(.75f, 1f);
        yield return new WaitForSeconds(1.2f);

        MessageDispatcher.SendMessage("ResetDay");
        
        CameraTransition.Instance.Transition(.75f, 0f);
        //set the game back in a playable state
        GameController.Instance.setState(GameState.DURING);

        //increase the day unless it was a rewind
        if (_increaseDay)

        {
            currentDay++;
            daysLeftLabel.text = (8 - currentDay).ToString();
            //check if we are out of days. If so, load the out of days ending.
            if (currentDay >= 8)
            {

                GameController.Instance.dialogSystem.show("YOU WAKE UP REALIZING, IT'S THAT DAY. SHE IS LEAVING. MAYBE SHE ALREADY DID...", 3);
                StartCoroutine(resetGameIn(4, "OUT_OF_DAYS"));
            }
            else
            {
                if( !GameController.Instance.otlUnit.IsPoisoned )
                    GameController.Instance.dialogSystem.show(morningGreetings[UnityEngine.Random.Range(0, morningGreetings.Length - 1)].ToUpper(),3);
                else
                {
                    GameController.Instance.dialogSystem.show("CRAP! YOU WOKE UP AND REMEMBERED, YOU POISONED NATALIE BUT NEVER HEALED HER!", 3);
                    StartCoroutine(resetGameIn(4, "HEAL_POISON"));
                }

                //GameController.Instance.dialogSystem.show("IT'S A NEW DAY, IT'S A NEW DAWN, IT'S A NEW...\n WELL, THE SAME LOVE AGAIN", 3);
            }
        }  

    }

    //set the game to reset once the ending has displayed. load specific endings based on the _type parameter
    public IEnumerator resetGameIn(float _seconds, string _type)
    {
        if (_seconds <= 1.2f)
            _seconds = 1.3f;
        //start the bad ending music
        if (_type != PotionType.DMG_LOVE.ToString())
        {
            GameController.Instance.endingMusic.Play();
            GameController.Instance.endingMusic.time = _seconds + 0.3f;
            GameController.Instance.endingMusic.DOFade(1, 2);
            GameController.Instance.townMusic.DOFade(0, 4);
            GameController.Instance.forestMusic.DOFade(0, 4);
        }
        else
        //start the good ending music
        {

            GameController.Instance.endingMusic.clip = GameController.Instance.loveEnding;
            GameController.Instance.endingMusic.Play();
            GameController.Instance.endingMusic.DOFade(1, 2);
            GameController.Instance.townMusic.DOFade(0, 4);
            GameController.Instance.forestMusic.DOFade(0, 4);
            GameController.Instance.player.GetComponent<PlayerUnit>().inLove = true;
        }
        //sort the transition to the ending, and set game state to end to stop everything else.
        yield return new WaitForSeconds(_seconds - 1.2f);
        CameraTransition.Instance.Transition(.75f, 1f);
        yield return new WaitForSeconds(1.2f);
        CameraTransition.Instance.Transition(.75f, 0f);
        Debug.Log(_type.ToString());
        GameController.Instance.endingManager.ShowEnding(_type);
        GameController.Instance.UnlockAchievement(_type);
        GameController.Instance.hudCanvas.SetActive(false);

        GameController.Instance.setState(GameState.END);
    }

    //the exact same as above, but can be called with potion type rather than a string
   
    public IEnumerator resetGameIn(float _seconds,PotionType _type)
    {
        if (_seconds <= 1.2f)
            _seconds = 1.3f;

        if( _type != PotionType.DMG_LOVE )
        { 
            GameController.Instance.endingMusic.Play();
            GameController.Instance.endingMusic.time = _seconds+0.3f;
            GameController.Instance.endingMusic.DOFade(1, 2);
            GameController.Instance.townMusic.DOFade(0, 4);
            GameController.Instance.forestMusic.DOFade(0, 4);
        } else
        {

            GameController.Instance.endingMusic.clip = GameController.Instance.loveEnding;
            GameController.Instance.endingMusic.Play();
            GameController.Instance.endingMusic.DOFade(1, 2);
            GameController.Instance.townMusic.DOFade(0, 4);
            GameController.Instance.forestMusic.DOFade(0, 4);
            GameController.Instance.player.GetComponent<PlayerUnit>().inLove = true;
        }

        yield return new WaitForSeconds(_seconds - 1.2f);
        CameraTransition.Instance.Transition(.75f, 1f);
        yield return new WaitForSeconds(1.2f);
        CameraTransition.Instance.Transition(.75f, 0f);
        Debug.Log(_type.ToString());
        GameController.Instance.endingManager.ShowEnding(_type.ToString() );
        GameController.Instance.UnlockAchievement(_type.ToString());
        GameController.Instance.hudCanvas.SetActive(false);

        GameController.Instance.setState(GameState.END);
            

    }


    //function that allows us to reset day and update the day number to refelect the rewind
    public IEnumerator rewindDayIn(float _seconds)
    {
        if (_seconds <= 1.2f)
            _seconds = 1.3f;

        yield return new WaitForSeconds(_seconds - 1.2f);
        GameController.Instance.setState(GameState.TRANSITION);
        CameraTransition.Instance.Transition(.75f, 1f);
        yield return new WaitForSeconds(1.2f);

        currentDay--;
        daysLeftLabel.text = (8 - currentDay).ToString();
        MessageDispatcher.SendMessage("ResetDay");
        CameraTransition.Instance.Transition(.75f, 0f);
        GameController.Instance.setState(GameState.DURING);

    }
    


}
