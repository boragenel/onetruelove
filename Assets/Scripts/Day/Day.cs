﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//structure based class that ensures each day has a list of ingridents for each potion index? and each potion? DPJ
public class Day  {

    public Dictionary<string, int> ingredients = new Dictionary<string, int>();
    public Dictionary<PotionType, int> potions = new Dictionary<PotionType, int>();


}
