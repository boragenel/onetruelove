﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour {

    //this will have the player dragged into it via the inspector
    public Transform followObject;
    Vector3 pos;
    public Vector3 shake = Vector3.zero;
	
	// Update is called once per frame
	void LateUpdate () {
        pos = followObject.position;

        //move camera back 10 as player is on 0 on the z plane DPJ
        pos.z = -10;

        //if shake is required, add it to the current camera position this frame DPJ
        pos += shake;

        //not sure why this is needed, maybe incase we have decimal places for pixels? DPJ
        Vector3 roundPos = new Vector3(RoundToNearestPixel(pos.x, GetComponent<Camera>()), RoundToNearestPixel(pos.y, GetComponent<Camera>()), pos.z);
        transform.position = roundPos;
    }



    public static float RoundToNearestPixel(float unityUnits, Camera viewingCamera)
    {
        //unity units is where in unity world space it is. We do height by width to get the 1080 /  1920 = 05625... * the units we have (say pos 30 in the world). 16.875  DPJ
        float valueInPixels = (Screen.height / (viewingCamera.orthographicSize * 2)) * unityUnits;
        //round that value up so we get an exact pixel position? Rounded would be 16.875 > 17 DPJ  
        valueInPixels = Mathf.Round(valueInPixels);
        //now you do it backwards? 17 / 0.5625 = 30.22222222222222222222222222.  DPJ
        float adjustedUnityUnits = valueInPixels / (Screen.height / (viewingCamera.orthographicSize * 2));
        //In summary, I don't really get why we do all of this :) DPJ
        return adjustedUnityUnits;
    }

}
