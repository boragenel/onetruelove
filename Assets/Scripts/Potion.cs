﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public enum PotionType
{
    NONE,
    DMG_FIREBALL,
    DMG_CHILL,
    DMG_EARTH,
    DMG_LOVE,
    TRANS_FROG,
    TRANS_WEREWOLF,
    TRANS_YOU,
    TRANS_FLOWER,
    HEAL_INC_MAX_HP,
    HEAL_TO_MAX_HP,
    HEAL_INSTA_KILL,
    HEAL_POISON,
    TIME_INC_MOVESPEED,
    TIME_TELEPORT,
    TIME_PAUSE,
    TIME_REWIND,
    LIAM_LOVE     
}

public class Potion {
    //default type to be NONE, as if NONE when we click no effect will be done. Same as not equipping anything.
    public PotionType type = PotionType.NONE;
    //image to use on the potion, loaded from resources
    public Sprite potionSprite;
    //the index of the potion during this game - will be a random number between 0 and 15
    public string potionIndex;

    public Potion()
    {   
        //load the image based on on the index, so each potion has a different image
        potionSprite = GameController.Instance.potionVariations[Convert.ToInt16( potionIndex )];
        if (potionSprite == null)
        {
            Debug.Log("Sprite is null");
        }
        //could name images the same as the spell, and just do LOAD RESOURCE with the enum type as the file name
        potionIndex = "";
    }
}
