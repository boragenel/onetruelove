﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaterialPickup : MonoBehaviour {

    public AudioClip pickupItemSFX;

    //needs to be Shrooms, Lavender, Oranges or Cinnamon
    public string whichMaterial;
    //can overide this in inspector/prefab if you wanted to drop a "cluster" of something. 
    public int amountToGive = 1;
    //reference to material manager as we will be adding to the quantities as we collect a materila 
    public MaterialManager mManager;
    //player is needed so we can make the pickup glow if the player is near
    public GameObject player;
    //the 4 different sprites for the four different materials
    public Sprite[] materialSprites;
    //a way to delay the start of the glowing effect
    public float hintStartDelay = 0;
    
    
	void Start () {
        //get our references
        mManager = FindObjectOfType<MaterialManager>();
        player = GameController.Instance.player;
        //choose a random delay - not sure why random DPJ
        hintStartDelay = Random.Range(0.4f, 0.8f);
	}
	
	void Update () {
        //so if plaer is within 80 units of the pickup AND of if the time at start of time frame, plus our delay, when devided by 2 has a 0 result. EVERY 2 SECONDS?
        if (Vector3.Distance(player.transform.position, transform.position) < 80 && Mathf.RoundToInt(hintStartDelay + Time.time) % 2 == 0)
        {
            //apply the hint particles in ranndom position relative to the material (+/- 2 either way)
            var emitParams = new ParticleSystem.EmitParams();
            emitParams.position = transform.position + (Vector3.right * Random.Range(-2,2)) + (Vector3.up * Random.Range(-2, 2));
            GameController.Instance.PickupHint.Emit(emitParams, 2);
        }
	}

    //method to call in order to set the material to be of a specific type (Shrooms in this case)
    public void SetAsShrooms( )
    {
        whichMaterial = "Shrooms";
        GetComponent<SpriteRenderer>().sprite = materialSprites[1];
    }

    public void SetAsLavender()
    {
        whichMaterial = "Lavender";
        GetComponent<SpriteRenderer>().sprite = materialSprites[2];
    }

    public void SetAsOranges()
    {
        whichMaterial = "Oranges";
        GetComponent<SpriteRenderer>().sprite = materialSprites[3];
    }

    public void SetAsCinnamon()
    {
        whichMaterial = "Cinnamon";
        GetComponent<SpriteRenderer>().sprite = materialSprites[0];
    }

    //when the played colliders with the trigger, add the amount to give to the current material total
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {

            SoundsEffectsManager.Instance.Play(pickupItemSFX,0.28f);

            if (whichMaterial == "Shrooms")
            {
                mManager.UpdateShrooms(amountToGive);
            }
            else if(whichMaterial == "Lavender")
            {
                mManager.UpdateLavender(amountToGive);
            } else if (whichMaterial == "Oranges")
            {
                mManager.UpdateOranges(amountToGive);
            }
            else if (whichMaterial == "Cinnamon")
            {
                mManager.UpdateCinnamon(amountToGive);
            }
            else
            {
                Debug.Log("Invalid Collectable Type");
            }
            gameObject.SetActive(false);
        }
        
    }
}
