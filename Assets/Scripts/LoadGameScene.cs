﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Used on the OnClick event of a button to load the game scene with a nice transition, whilst disabling the panel the button is attached to.
/// </summary>
public class LoadGameScene : MonoBehaviour
{

    public void LoadTheGame()
    {
        CameraTransition.Instance.Completion = 0f;
        CameraTransition.Instance.Transition(2.5f, 1f);
        Invoke("DoStartGameTransition", 2.2f);
        transform.parent.gameObject.SetActive(false);
    }

    void DoStartGameTransition()
    {
        SceneManager.LoadSceneAsync("GameScene");
    }
}