﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this places random rock and grass sprites over the terrain
public class TerrainDecorationGenerator: MonoBehaviour
{
    //list of decation items to be places
    public List<Sprite> Prefabs;
    public float MinStep = 0.1f;
    public float MaxStep = 1f;

    //this will used on to the large chunks of ground for the forest/town
    private SpriteRenderer sr;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        if (Prefabs.Count < 1)
            return;

        //loop from one side of the map to the other, and go down to sweep the area.
        float width = sr.bounds.size.x;
        float height = sr.bounds.size.y;
        //starts top left, incrementing by a random (bound) amount 
        for (float x = 0f; x < width; x += Random.Range(MinStep, MaxStep))
        {
            //does vertical lines down, incrementing by a random (bound) amount 
            for (float y = 0f; y < height; y += Random.Range(MinStep, MaxStep))
            {
                //gets a decoration prefab, sets in positions and handles anchoring at their center.
                var go = InstantiateRandom();
                go.transform.position = new Vector3(-(width * 0.5f) + x, -(height * 0.5f) + y) + transform.position;
            }
        }
    }

    //chooses one of the decoration prefabs at random and sets ti to be part of the background layer and child of the forest/village.
    private GameObject InstantiateRandom()
    {
        var go = new GameObject();
        var gosr = go.AddComponent<SpriteRenderer>();
        gosr.sprite = Prefabs[Random.Range(0, Prefabs.Count)];
        gosr.sortingLayerName = "Background";
        go.transform.parent = transform;
        return go;
    }
}
