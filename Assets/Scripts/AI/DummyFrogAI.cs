﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DummyFrogAI : MonoBehaviour {

    public AudioClip frogSFX;

    Transform player;
    bool playerIsInSight = false;

    TargetableEnemy enemy;
    public bool lookAtPlayer=false;

    private Tweener tweener;

    float sightRadius = 45;

    public float conciousTimer = -1;
    PlayerUnit targetablePlayer;

    Animator animator;
    bool isWalking = false;
    bool isAttacking = false;

    GameController gController;

    void Start ()
    {
        //setup references
        player = GameController.Instance.player.transform;
        targetablePlayer = player.GetComponent<PlayerUnit>();
        enemy = GetComponent<TargetableEnemy>();
        animator = GetComponentInChildren<Animator>();
        gController = GameController.Instance;
    }
	

	void Update () {
        conciousTimer -= Time.deltaTime;
        //only act if timer is below 0
        if (conciousTimer > 0)
            return;
        //check if in range, then attack (after short delay if player is still within attack range)
        checkIfPlayerIsInSight();
        handleMovement();
	}

    public void checkIfPlayerIsInSight()
    {
        //only bother doing this when player is alive or game is in a playing state
        if (!targetablePlayer.IsAlive || gController.currentState != GameState.DURING)
        {
            playerIsInSight = false;
            return;
        }

        //if you are within 10 units, then we can attempt to attack
        if (Vector3.Distance(transform.position, player.position) < 10)
            StartCoroutine(attack());
        else
        {
            //if we can't attack, and we are in range, then we want to turn to face the enemy
            if (Vector3.Distance(transform.position, player.position) < sightRadius)
            {
                playerIsInSight = true;
                if (lookAtPlayer)
                {
                    float _angle = Mathf.Atan2(transform.position.y - player.position.y, transform.position.x - player.position.x) * Mathf.Rad2Deg;
                    transform.localEulerAngles = new Vector3(0, 0, _angle - 90);
                }
            }
            else playerIsInSight = false;
           
        }
    }

    //moves towards player if within aggro range, setting aniation accordingly
    public void handleMovement()
    {
        if( playerIsInSight )
        {
            if (!isWalking )
            {
                if(animator != null)
                    animator.SetBool("isWalking", true);
                isWalking = true;
                isAttacking = false;
            }

            //handles the hopping animation of the frog - so instead of simply moving, it tweens between zoomed and not, whilst making progress towards the player
            if( tweener == null || (tweener != null && !tweener.IsPlaying() ) )
            { 
                //on frog land will just set a 1 second delay on the frog before he attempts to hop / attack again.
                tweener = transform.DOMove(transform.position + (18 *  (player.position - transform.position).normalized), 0.5f).SetEase(Ease.OutCirc).OnComplete(onFrogLand);
                SoundsEffectsManager.Instance.Play(frogSFX,0.1f,Random.Range(1f,1.1f));
                //frog fall down tweens between scales to simulate the jumping animation
                transform.GetChild(0).DOScale(17, 0.25f).OnComplete(frogFallDown);
            }

        } else
        {
            //stop if player is no longer in range
            isWalking = false;
            if( animator != null )
            animator.SetBool("isWalking", false);
        }
    }
    //delay between jumps
    private void onFrogLand()
    {
        conciousTimer = 1f;
    }
    //to change scale as frog falls (midway through move
    private void frogFallDown()
    {
        transform.GetChild(0).DOScale(10, 0.25f);
    }

    //works same as EnemyAI
    private IEnumerator attack()
    {
        isAttacking = false;
        // TODO: play attack anim
        if (GetComponent<EnemyUnit>().isWerewolf)
        {
            conciousTimer = 0.6f;
            yield return new WaitForSeconds(0.05f);
            
        }
        else
        {
            conciousTimer = 0.6f;
            yield return new WaitForSeconds(0.15f);
            
        }
        if (Vector3.Distance(transform.position, player.position) < 10)
        {
            player.GetComponent<GenericUnit>().TakeDamage(GetComponent<GenericUnit>().Damage);
            player.GetComponent<PlayerUnit>().StartCoroutine(player.GetComponent<PlayerUnit>().poisonTick());
            if ( !isAttacking )
            {
                if (animator != null)
                    animator.SetTrigger("Attack");
                isWalking = false;
                isAttacking = true;
            }

        }


    }
    


}
