﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyEnemyAI : MonoBehaviour {

    Transform player;
    bool playerIsInSight = false;

    EnemyUnit enemy;
    public bool lookAtPlayer=false;
    
    //distance from player before "aggrod"
    float sightRadius = 45;

    //concious timer - if above 0 it meansa they don't follow their AI
    public float conciousTimer = -1;
    PlayerUnit targetablePlayer;

    Animator animator;
    bool isWalking = false;
    bool isAttacking = false;
    
    CharacterController2D charControlller;

    GameController gController;

    void Start ()
    {
        //get references
        player = GameController.Instance.player.transform;
        targetablePlayer = player.GetComponent<PlayerUnit>();
        enemy = GetComponent<EnemyUnit>();
        animator = GetComponentInChildren<Animator>();
        gController = GameController.Instance;
        charControlller = GetComponent<CharacterController2D>();
    }
	

	void Update () {
        
        conciousTimer -= Time.deltaTime;
        //reduce concious Timer, if below 0 then don't do anything
        if (conciousTimer > 0)
            return;
        //see if player is in aggro range
        checkIfPlayerIsInSight();
        //set moving animation and move in the direction of the player
        handleMovement();
	}


    public void checkIfPlayerIsInSight()
    {
        //don't bother checking if players dead or the game is in a end game / transition / pause state
        if (!targetablePlayer.IsAlive || gController.currentState != GameState.DURING)
        {
            playerIsInSight = false;
            return;
        }

        //if you're within attacking range, attack 
        if (Vector3.Distance(transform.position, player.position) < 10)
            //this will pause the enemy for 0.6f seconds then attempt to do damage if still in range
            StartCoroutine(attack());
        else
        {
            //if the player is within sight
            if (Vector3.Distance(transform.position, player.position) < sightRadius)
            {
                //rotate so that we are facing the player
                playerIsInSight = true;
                if (lookAtPlayer)
                {
                    float _angle = Mathf.Atan2(transform.position.y - player.position.y, transform.position.x - player.position.x) * Mathf.Rad2Deg;
                    transform.localEulerAngles = new Vector3(0, 0, _angle - 90);
                }
            }
            else playerIsInSight = false;
        }
    }

    //handling movement of enemy
    public void handleMovement()
    {

        if( playerIsInSight )
        {
            //set animation for walking, switch off 
            if (!isWalking )
            {
                //checks if its first time we are walking, if so, set the animation and stop coming in here again until we've stopped walking ( attacked or something )
                if(animator != null)
                    animator.SetBool("isWalking", true);
                isWalking = true;
                isAttacking = false;
            }
            //move towards the player, though normalise it so no matter what angle we move, the same distance will be covered
            enemy.Move((player.position - transform.position).normalized);
        }
        //stop walking if player is no longer in the aggro range
        else
        {
            isWalking = false;
            if( animator != null )
            animator.SetBool("isWalking", false);
        }
    }
    
    //attacking AI
    private IEnumerator attack()
    {
        isAttacking = false;
        // TODO: play attack anim
        //pause for some time when we get in range the player, like a "wind up" timer
        if (GetComponent<EnemyUnit>().isWerewolf)
        {
            conciousTimer = 0.6f;
            yield return new WaitForSeconds(0.05f);
            
        }
        else
        {
            conciousTimer = 0.6f;
            yield return new WaitForSeconds(0.15f);
            
        }
        //if they are STILL in range after the delay, then we set our attacking animation and we do damage to the player
        if (Vector3.Distance(transform.position, player.position) < 10)
        {
            player.gameObject.GetComponent<GenericUnit>().TakeDamage(GetComponent<GenericUnit>().Damage);
            if( !isAttacking )
            {
                if (animator != null)
                    animator.SetTrigger("Attack");
                isWalking = false;
                isAttacking = true;
            }

        }

    }
    

}
