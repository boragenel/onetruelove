﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines an ending panel, so they can be found with ease and allows us to state which PotionType this panel is linked to upon game ending.
/// </summary>
public class EndingPanel : MonoBehaviour {
    public string endingCause;
}
