﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Achievement : MonoBehaviour {

    public PotionType endingType;
    public string achievementName;
    public string achievementDescription;

    public GameObject lockedPanel;
    public bool unlocked;
    public bool isMouseOver;

    public static Action<string, bool> activateTooltip;

    public static Action deactivateTooltip;

    

    void Start () {

    }
	
    public bool IsLocked()
    {
        return !unlocked;
    }


    public void WhenMouseEnter()
    {
        if (!isMouseOver)
        {
            //Debug.Log("Broadacasting ShowTooltip");
            //activate tool tip
            if (activateTooltip != null)
                activateTooltip.Invoke(achievementDescription, unlocked);
                isMouseOver = true;
        }
    }

    public void WhenMouseExit()
    {
        if (isMouseOver)
        {
            //Debug.Log("Broadacasting HideToolTip");
            if (deactivateTooltip != null)
            {
                deactivateTooltip.Invoke();
                isMouseOver = false;
            }
        }
    }

    //returns true if it was unlocked
    public void RefreshAchievement()
    {
        if (PlayerPrefs.HasKey(achievementName))
        {
            //the player prefs int will be used in boolean form, 0 = false, 1 = true;
            if (PlayerPrefs.GetInt(achievementName) == 1)
            {
                Debug.Log("Status: " + achievementName + " is unlocked");
                
                //disable the red locked overlay
                lockedPanel.SetActive(false);
                //set boolean that allows us to check quickly if a panel is locked or not
                unlocked = true;               
            }
            else
            {
                Debug.Log("Status: " + achievementName + " is locked");
            }
        }
        else
        {
            Debug.Log("Status: " + achievementName + " was not set");
            PlayerPrefs.SetInt(achievementName, 0);

        }
    }

}
