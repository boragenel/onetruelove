﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthFaces : MonoBehaviour {

    private PlayerUnit liam;
    //list of the faces to represent damage
    public Sprite[] faces;
    public Image faceDisplay;

	// Use this for initialization
	void Start () {
        liam = GameController.Instance.player.GetComponent<PlayerUnit>();
        faceDisplay.GetComponent<Image>().sprite = faces[0];
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(liam.Health.Current);
        if (liam.inLove)
        {
            faceDisplay.GetComponent<Image>().sprite = faces[4];
        }
        else if(liam.Health.Current <= 0)
        {
            faceDisplay.GetComponent<Image>().sprite = faces[3];
        }else if (liam.Health.Current <= 33)
        {
            faceDisplay.GetComponent<Image>().sprite = faces[2];
        }
        else if (liam.Health.Current <= 66)
        {
            faceDisplay.GetComponent<Image>().sprite = faces[1];
        }
        else
        {
            faceDisplay.GetComponent<Image>().sprite = faces[0];
        }

    }
}
