﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MainMenuHelper: MonoBehaviour
{
    public AudioMixer MasterAudio;

    public Slider MasterAudioSlider;
    public Slider EffectsAudioSlider;
    public Slider MusicAudioSlider;

    private bool SaveChanges = true;

    //panel that shows the condensed story to give some context before starting the game
    public GameObject storyPanel;
    //panel that shows the achievements currently earned.
    public GameObject achievmentPanel;

    public AchievementsLoader achievementsLoader;

    public Toggle hardModeToggle;
    public Text hardModeLabel;

    public bool initedToggle = false;

    private bool isHardModeUnlocked = false;
    public bool isHardMode = false;

    private void Start()
    {
        LoadAudioSettings();
        if( achievementsLoader != null)
        { 
            achievementsLoader.loadAchievements();

            if( !PlayerPrefs.HasKey("HardModeUnlocked") )
            {
                PlayerPrefs.SetInt("HardModeUnlocked", 0);
            }

            int hardModeUnlockedInt = PlayerPrefs.GetInt("HardModeUnlocked");

            //hardModeUnlockedInt = 1; forced unlock hard mode
        

            if (hardModeUnlockedInt == 1)
            {
                isHardModeUnlocked = true;

                hardModeToggle.interactable = true;
                hardModeLabel.color = new Color(43f / 255f, 23f / 255f, 2f / 255f, 1f);

                if (!PlayerPrefs.HasKey("HardMode"))
                {
                    PlayerPrefs.SetInt("HardMode", 0);
                }

                int hardMode = PlayerPrefs.GetInt("HardMode");

            

                if( hardMode == 1)
                {

                    isHardMode = true;
                

                } else
                {
                    isHardMode = false;
                
                }

                
                hardModeToggle.isOn = isHardMode;
                initedToggle = true;
            }

        }



    }

    public void toggleHardMode()
    {
        if (!initedToggle)
            return;

        Debug.Log("it was " + isHardMode);
        isHardMode = !isHardMode;

        if (isHardMode)
            PlayerPrefs.SetInt("HardMode", 1);
        else
            PlayerPrefs.SetInt("HardMode", 0);

        Debug.Log("PlayerPrefs.HardMode : "+PlayerPrefs.GetInt("HardMode"));

    }

    public void LoadAudioSettings()
    {
        //default volumes to be set if first play of game.
        if (!PlayerPrefs.HasKey("MasterVolume"))
        {         
            PlayerPrefs.SetFloat("MasterVolume", 0.75f);
            PlayerPrefs.SetFloat("EffectsVolume", 0.75f);
            PlayerPrefs.SetFloat("MusicVolume", 0.75f);
            PlayerPrefs.Save();
        }

        //set variables to be used as current volume levels
        var master = PlayerPrefs.GetFloat("MasterVolume");
        var effects = PlayerPrefs.GetFloat("EffectsVolume");
        var music = PlayerPrefs.GetFloat("MusicVolume");

        if (MasterAudioSlider != null && EffectsAudioSlider != null && MusicAudioSlider != null)
        {
            MasterAudioSlider.value = master;
            EffectsAudioSlider.value = effects;
            MusicAudioSlider.value = music;
        }

        SaveChanges = false;
        SetMasterVolume(master);
        SetEffectsVolume(effects);
        SetMusicVolume(music);
        SaveChanges = true;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    //linked to start button on menu, to show the intro story panel
    public void ShowIntroStory()
    {
        storyPanel.SetActive(true);
    }

    public void StartGame()
    {
        CameraTransition.Instance.Completion = 0f;
        CameraTransition.Instance.Transition(1f, 2.5f);
        Invoke("DoStartGameTransition", 2.2f);
        transform.parent.gameObject.SetActive(false);
    }

    void DoStartGameTransition()
    {
        SceneManager.LoadSceneAsync("GameScene");
    }

    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void LoadLevelAdditive(string levelName)
    {
        SceneManager.LoadScene(levelName, LoadSceneMode.Additive);
    }

    public void UnloadLevel(string levelName)
    {
        SceneManager.UnloadSceneAsync(levelName);
    }

    //converts the value from the slider into a decibel value to be used by volume setting functions
    float LinearToDB(float value)
    {
        if (value < 0.002f)
            value = -80f;
        else
            value = Mathf.Log10(value) * 40f;
        return value;
    }

    //method is called when the slider is changed within the options screen.
    public void SetMasterVolume(float value)
    {
        //only update player prefs if saveChanges is set to true (in this case, it always will be. could use this if we want "accept button" to save the changes and any temp changes not saved)
        if (SaveChanges)
        {
            PlayerPrefs.SetFloat("MasterVolume", value);
            PlayerPrefs.Save();
        }
        MasterAudio.SetFloat("MasterVolume", LinearToDB(value));
    }

    public void SetMusicVolume(float value)
    {
        if (SaveChanges)
        {
            PlayerPrefs.SetFloat("MusicVolume", value);
            PlayerPrefs.Save();
        }
        MasterAudio.SetFloat("MusicVolume", LinearToDB(value));
    }

    public void SetEffectsVolume(float value)
    {
        if (SaveChanges)
        {
            PlayerPrefs.SetFloat("EffectsVolume", value);
            PlayerPrefs.Save();
        }
        MasterAudio.SetFloat("EffectsVolume", LinearToDB(value));
    }

    //maximises the screen from options menu
    public void SetFullscreen(bool value)
    {
        Screen.fullScreen = value;
    }

    //function to link to buttons where you want it to toggle the enabled state of a panel
    public void TogglePanel (GameObject panel)
    {
        panel.SetActive(!panel.activeSelf);
    }
}
