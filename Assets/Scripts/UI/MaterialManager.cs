﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaterialManager : MonoBehaviour {
    public Text txtShrooms, txtLavender, txtOranges, txtCinnamon;
    public int qShrooms, qLavender, qOranges, qCinnamon;

    private void Start()
    {
        UpdateMaterialDisplays();
    }

    //method to add one to the shrooms total. This will be called by a Material Pickup based on it's chosen type.
    public int UpdateShrooms(int amount)
    {
        //add 1, but cap at 1000. 
        qShrooms = Mathf.Clamp(qShrooms + amount, 0, 1000);
        //update the text values that are displayed on the material panel
        UpdateMaterialDisplays();
        //return the quanity incase we need it (atm it is unused)
        return qShrooms;
    }

    public int UpdateLavender(int amount)
    {
        qLavender = Mathf.Clamp(qLavender + amount, 0, 1000);
        UpdateMaterialDisplays();
        return qLavender;
    }

    public int UpdateOranges(int amount)
    {
        qOranges = Mathf.Clamp(qOranges + amount, 0, 1000);
        UpdateMaterialDisplays();
        return qOranges;
    }

    public int UpdateCinnamon(int amount)
    {
        qCinnamon = Mathf.Clamp(qCinnamon + amount, 0, 1000);
        UpdateMaterialDisplays();
        return qCinnamon;
    }

    //update the text fields to be current value of the materials. Called after any changes are made to the material quantities
    public void UpdateMaterialDisplays()
    {
        txtShrooms.text = qShrooms.ToString();
        txtLavender.text = qLavender.ToString();
        txtOranges.text = qOranges.ToString();
        txtCinnamon.text = qCinnamon.ToString();
    }

    //When a recipie is brewed, work out which ingredients were used and subtract those from the relevent quantities
    public void PostBrewUpdate(string recipeCode)
    {
        UpdateShrooms(recipeCode[0].Equals('1') ? -1 : 0);
        UpdateLavender(recipeCode[1].Equals('1') ? -1 : 0);
        UpdateOranges(recipeCode[2].Equals('1') ? -1 : 0);
        UpdateCinnamon(recipeCode[3].Equals('1') ? -1 : 0);
    }
}
