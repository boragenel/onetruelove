﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTipDisplay : MonoBehaviour {
    private Text tooltipText;
    private Image tooltipBG;
    private bool isTooltipOn;
    public Vector3 offset;

    private void Awake()
    {
        //sub to "showTooltip" events
        Achievement.activateTooltip += ShowTooltip;
        //sub to "hideTooltip" events
        Achievement.deactivateTooltip += HideToolTip;
    }

    private void OnDestroy()
    {
        //sub to "showTooltip" events
        Achievement.activateTooltip -= ShowTooltip;
        //sub to "hideTooltip" events
        Achievement.deactivateTooltip -= HideToolTip;
    }

    // Use this for initialization
    void Start () {
        tooltipText = transform.GetChild(0).GetComponent<Text>();
        tooltipBG = GetComponent<Image>();
        gameObject.SetActive(false);
        //may need to change this later
        //offset = Vector3.zero;
    }
	
	// Update is called once per frame
	void Update () {
        //if toolTipON
        if (isTooltipOn)
        {
            //follow position
            Vector3 _pos = Input.mousePosition;
            if ( Input.mousePosition.x / (float)Screen.width < 0.3f )
            {
                _pos.x = (float)Screen.width * 0.27f;
                transform.position = _pos + offset;
            } else if (Input.mousePosition.x / (float)Screen.width > 0.7f)
            {
                _pos.x = (float)Screen.width * 0.6f;
                transform.position = _pos + offset;
            } else
            {
                transform.position = Input.mousePosition + offset;
            }
        }

    }

    public void ShowTooltip(string message, bool unlocked)
    {
        //Debug.Log("EVENT ShowTooltip");
        //turn boolean
        isTooltipOn = true;
        //set text
        tooltipText.text = message;
        //turn text box
        gameObject.SetActive(true);
        //set colour of text box
        if(unlocked)
            gameObject.GetComponent<Image>().color = Color.green;
        else
            gameObject.GetComponent<Image>().color = Color.red;
    }

    public void ShowTooltipGeneric(string message)
    {
        //Debug.Log("EVENT ShowTooltip");
        //turn boolean
        isTooltipOn = true;
        //set text
        tooltipText.text = message;
        //turn text box
        gameObject.SetActive(true);
        //set colour of text box
      
    }

    public void HideToolTip()
    {
        //Debug.Log("EVENT HideTooltip");

        //turn off bool
        isTooltipOn = false;
        //deactivate text box
        gameObject.SetActive(false);
    }
}
