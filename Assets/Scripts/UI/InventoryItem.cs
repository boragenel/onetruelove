﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour {

    

    //how many of the specific potion you have
    public int quantity;
    //the potion object that sits in an inventory slot - we only need the potionType in reality
    public Potion potion;
    //whether or not new potions can assume this slot to live
    public bool slotInUse = false;

    //displays how many we have on the UI
    public Text quantityText;
    //dont use anymore as we show different color potions instead of the "index"
    public Text recipeIndex;
    //image to display on the UI hotbar
    public Image potionImageComponent;
    //the intentory items index within the inventory bar itself (1-9)
    public int inventoryIndex;


    void Start () {
        //get reference to the components 
        potionImageComponent = transform.GetChild(0).GetComponent<Image>();
        recipeIndex = transform.GetChild(1).GetComponent<Text>();
        quantityText = transform.GetChild(2).GetComponent<Text>();

        quantityText.text = "";
	}

    //Set an inventory slot to hold a particular potion
    public void SetInventorySlot(Potion potionToSet)
    {
        //set the image on 
        potionImageComponent.gameObject.SetActive(true);
        //sets the potion, though only the potion type is used in reality
        potion = potionToSet;
        //we set to one, because if it already existed, we wouldn't have called this function, we would have just added to the quantity.
        quantity = 1;
        //update image and text displays (potionvariations is a list of all 16 different potion sprites)
        potion.potionSprite = GameController.Instance.potionVariations[Convert.ToInt16(potion.potionIndex)];
        potionImageComponent.sprite = potion.potionSprite;
        //potionImageComponent.color = Color.red;
        quantityText.text = quantity.ToString();
        //recipeIndex.text = potion.potionIndex;
        //means when creating a new potion, this slot won't be considered for placing the potion on our hotbar
        slotInUse = true;
    }

    //quick function to check if a slow is free
    public bool IsFree()
    {
        return !slotInUse;
    }

    /// <summary>
    /// Checks that the ability is able to be used, updates the quantity
    /// </summary>
    /// <returns> Returns true if it's reduced the quantity successfully.</returns>
    public bool UsePotion(string potionType)
    {
        if(slotInUse && quantity > 0)
        {
            //if you try using a potion, when you are "blacked out"
            if (!GameController.Instance.player.GetComponent<PlayerUnit>().IsAlive)
            {
                GameController.Instance.dialogSystem.show("NOT HAPPENING,\n ENJOY YOUR DREAMS FIRST.", 2);
                return false;
            }

            Debug.Log("Potion Type is :" + potionType);
            Debug.Log("Current Enum is " + GameController.Instance.curDraggedPotion.type);
            //set the potion type in game controller to be the one that you've clicked on
            GameController.Instance.curDraggedPotion.type = (PotionType)Enum.Parse(typeof(PotionType), potionType);
            //set the cursor to have the same image as the inventory slow and follow the mouse
            GameController.Instance.cursorPotion.SetActive(true);
            GameController.Instance.cursorPotion.GetComponentInChildren<Image>().sprite = potionImageComponent.sprite;
            GameController.Instance.curDraggedPotion.potionSprite = potionImageComponent.sprite;
            GameController.Instance.curDraggedPotion.potionIndex = potion.potionIndex;
            //reduce the quantity, and update display. Update display will remove it from inventory if quantity is 0
            quantity--;
            UpdateDisplay();
            
            Debug.Log("USED POTION " + potionType);
            return true;
        }
        Debug.Log("No potion to use, SILLY!");
        
        return false;
    }

    //updates the display of potions on the hotbar
    public void UpdateDisplay()
    {
        //if you have none left, free up the slow and remove all the graphical display information
        if(quantity == 0)
        {
            //set to be free to be used again
            slotInUse = false;
            //take away image
            potionImageComponent.sprite = null;
            recipeIndex.text = "";
            quantityText.text = "";
            potionImageComponent.gameObject.SetActive(false);
        }
        else
        {
            
            quantityText.text = quantity.ToString();
        }
        
        
    }

    //this is called if the potion type is already on the hotbar
    public void UpdateQuantity(int amount)
    {
        quantity += amount;
        UpdateDisplay();
    }

}
