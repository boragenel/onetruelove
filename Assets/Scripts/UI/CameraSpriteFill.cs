﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraSpriteFill : MonoBehaviour {
    public SpriteRenderer Target;

    private Camera _cam;

    //DPJ I hurt after trying to get this and couldn't find it in game scene
	void Start () {
        _cam = GetComponent<Camera>();
        var camVector = _cam.ViewportToWorldPoint(new Vector2(1,1));
        var mod = (camVector.x * 2f) / Target.bounds.size.x;
        Target.transform.localScale = new Vector3(Target.transform.localScale.x * mod, Target.transform.localScale.x * mod, 1f);
    }
}
