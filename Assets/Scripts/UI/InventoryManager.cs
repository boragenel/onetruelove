﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

    public AudioClip bottleSelectSFX;

    //stores the 9 inventory slots
    public List<InventoryItem> inventoryItems;

	// Use this for initialization
	void Start () {
		//TESTING CODE
        //Potion testPotion = 
        for(int i=0; i < inventoryItems.Count; i++)
        {
            //gives each slot a unique index from 0 to 8
            inventoryItems[i].inventoryIndex = i;
        }
	}

    //when we brew a potion, we will add a potion to the hot bar. Returns false if no potion was able to be added
    public bool AddPotion(Potion newPotion)
    {
        //this will get set only if a free slot is checked;
        int freeSlotIndex = 99;
        //check if any items are free
        InventoryItem[] items = GetComponentsInChildren<InventoryItem>();

        //check if we already have an item of that type, because we should stack potions in that scenario
        for(int i = 0; i < items.Length; i++)
        {
            if (items[i].IsFree())
            {
                //if it hasn't yet been set
                if(freeSlotIndex > 10)
                {
                    freeSlotIndex = i;
                }   
            }
            //check if new potion is already an item
            else if(items[i].potion.type == newPotion.type)
            {
                //we need to add to quantity
                items[i].UpdateQuantity(1);
                return true;
            }
  
        }
        //if there was no free slot
        if(freeSlotIndex > 10)
        {
            //Debug.Log("No free slot for potion");
            return false;
        }
        //assign the potion to the first free slot found
        else
        {
            items[freeSlotIndex].SetInventorySlot(newPotion);
            //Debug.Log("Adding new potion to slot "+freeSlotIndex.ToString());
            return true;
        }
    }

    //function to simply check if there is a free slot on the hot bar
    public bool HasFreeSlot()
    {
        InventoryItem[] items = GetComponentsInChildren<InventoryItem>();

        //check if we already have an item of that type, because we should stack potions in that scenario
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i].IsFree())
            {
                return true;
            }
        }
        return false;
    }


    //ON CLICK EVENTS
    public void ClickUISlot(InventoryItem item)
    {
        //check if the UI slot is set to be used
        if (!item.IsFree() && GameController.Instance.curDraggedPotion.type == PotionType.NONE)
        {
            item.UsePotion(item.potion.type.ToString());
            //use potion stuff. Add it to player controller as "active potion"?
        //If we have a potion already equipped and what we've clicked on is an empty slot, let us reposition it
        } else if( GameController.Instance.curDraggedPotion.type != PotionType.NONE && item.IsFree())
        {
            Potion nPotion = new Potion();
            nPotion.type = GameController.Instance.curDraggedPotion.type;
            nPotion.potionIndex = GameController.Instance.curDraggedPotion.potionIndex;
            nPotion.potionSprite = GameController.Instance.curDraggedPotion.potionSprite;
            
            inventoryItems[item.inventoryIndex].SetInventorySlot(nPotion);
            GameController.Instance.curDraggedPotion.type = PotionType.NONE;
            GameController.Instance.cursorPotion.SetActive(false);
        //if we have one already, and only 1 DJ i dont get this fully, Bora.
        } else if(inventoryItems[item.inventoryIndex].quantity == 1)
        {
           
            Potion nPotion = new Potion();
            nPotion.type = GameController.Instance.curDraggedPotion.type;
            nPotion.potionIndex = GameController.Instance.curDraggedPotion.potionIndex;
            nPotion.potionSprite = GameController.Instance.curDraggedPotion.potionSprite;

            
            GameController.Instance.cursorPotion.SetActive(true);
            GameController.Instance.cursorPotion.GetComponentInChildren<Image>().sprite = inventoryItems[item.inventoryIndex].potionImageComponent.sprite;
            GameController.Instance.curDraggedPotion.potionSprite = inventoryItems[item.inventoryIndex].potionImageComponent.sprite;
            GameController.Instance.curDraggedPotion.potionIndex = inventoryItems[item.inventoryIndex].potion.potionIndex;
            GameController.Instance.curDraggedPotion.type = inventoryItems[item.inventoryIndex].potion.type;

            inventoryItems[item.inventoryIndex].SetInventorySlot(nPotion);

            

        }
        SoundsEffectsManager.Instance.Play(bottleSelectSFX);
    }

    //this will handle when you call with an index (1-9)
    public void ClickUISlot(int _index)
    {
        
        //subtract 1 as we actually use 0 - 8
        InventoryItem item = inventoryItems[_index-1];
        //then call the method
        ClickUISlot(item);
    }
}
