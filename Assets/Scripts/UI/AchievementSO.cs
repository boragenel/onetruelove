﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable] public struct AchievementStruct
{ 
    public string achievementName;
    public string description;
    public Sprite thumbnail;
}

[CreateAssetMenu(fileName = "Achievements", menuName = "MenuItems/AchievementList", order = 1)]
public class AchievementSO : ScriptableObject {
    public List<AchievementStruct> achievements;
}
