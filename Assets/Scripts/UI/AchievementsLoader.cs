﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsLoader : MonoBehaviour {

    public AchievementSO achievementsSO;
    public GameObject achievementPrefab;
    public Text title;
    /// <summary>
    /// to track how many achievements have been unlocked
    /// </summary>
    public int achievementsComplete = 0;

    void Start()
    {

        
    }

    public void loadAchievements()
    {

        foreach (AchievementStruct achieve in achievementsSO.achievements)
        {
            //create a new prefab of an Achievement 
            GameObject achievement = Instantiate(achievementPrefab, transform) as GameObject;
            //set the acheive name of the made prefab so we can check player prefs on start to unlock or not
            achievement.GetComponent<Achievement>().achievementName = achieve.achievementName;
            //set the description to be used with tool tip (maybe)
            achievement.GetComponent<Achievement>().achievementDescription = achieve.description;
            //set the image of the button
            achievement.transform.GetChild(0).GetComponent<Image>().sprite = achieve.thumbnail;
            //remove locked panel if achievement earned
            achievement.GetComponent<Achievement>().RefreshAchievement();
            //total the unlocked achievements

            //achievement.GetComponent<Achievement>().unlocked = true; // TESTING

            if (achievement.GetComponent<Achievement>().unlocked)
                achievementsComplete++;
        }

        
        if (achievementsComplete >= 15)
        {
            PlayerPrefs.SetInt("HardModeUnlocked", 1);
        }

        title.text = "ACHIEVEMENTS (" + achievementsComplete + "/" + achievementsSO.achievements.Count + ")";
        Debug.Log("ACHIEVEMETNS LOADED:" + achievementsComplete);

    }

}
