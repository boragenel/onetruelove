﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class will be used to modify the tick boxes and result section of the recipe book.
/// Will access a row at a time, then use getChild calls to access the tick boxes and the result text.
/// </summary>
public class RecipeBookManager : MonoBehaviour {
    //tracks whick entry in the recipe book is next to be filled upon potion discovery
    private int currentLineInBook = 0; 
    private RecipeInit _recipeInitialiser;
    public GameObject rowContainer;
    //list of all possible rows we comlpete (a row has 4 checkboxes and a text field for the result)
    public List<SpellBookRow> listOfRows;

    // Use this for initialization
    void Start () {
        _recipeInitialiser = FindObjectOfType<RecipeInit>();
        listOfRows = rowContainer.GetComponentsInChildren<SpellBookRow>().ToList();
        //foreach (SpellBookRow row in listOfRows)
        //{
        //    //get hold of the input field, and activate it so that it can be used before the game object is opened in game
        //    row.transform.GetChild(4).GetComponent<InputField>().ActivateInputField();
        //}
        transform.GetChild(0).gameObject.SetActive(false);
	}
	

    public void AddRecipeToBook(PotionType _ptype)
    {
        Debug.Log("Adding " + GameController.Instance.potionDescriptions[_ptype] + " to recipe book...");      
        //to get the number, we look up a potion by its potiontype (Converted to a string), and return the key.
        int recipeIndex = _recipeInitialiser.recipeBook.FirstOrDefault(x => x.Value == _ptype.ToString()).Key;
        Debug.Log("Key of new pot is " + recipeIndex);
        //get the binary representation of the potion index (this game, as it changes every game)
        string binaryCode = ToBin(recipeIndex, 4);
        Debug.Log("Binary code is " + binaryCode);
        //tick the boxes based on the binary representaiton var //0100
        for (int i = 0; i < 4; i++)
        {
            //Debug.Log(binaryCode[i] + "Is it equalt to 1? " + (binaryCode[i].Equals('1') ? true : false));
            listOfRows[currentLineInBook].transform.GetChild(i).GetComponent<Toggle>().isOn = binaryCode[i].Equals('1') ? true : false;
        }
        //add the result from the potion Description                                                GameController.Instance.potionDescriptions[_ptype]
        //if(listOfRows[currentLineInBook].transform.GetChild(4).GetChild(listOfRows[currentLineInBook].transform.GetChild(4).childCount - 1).GetComponent<Text>() == null) { Debug.Log("No text element on this child"); }
        listOfRows[currentLineInBook].transform.GetChild(4).GetComponent<Text>().text = GameController.Instance.potionDescriptions[_ptype];
        //move on a line so the hext potion we discover doesn't override this same line
        currentLineInBook++;
    }

    //converts an integer into a binary string of a specific length
    public static string ToBin(int value, int len)
    {
        return (len > 1 ? ToBin(value >> 1, len - 1) : null) + "01"[value & 1];
    }


}
