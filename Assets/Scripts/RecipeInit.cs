﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeInit : MonoBehaviour {
    public const int maxRecipes = 15;
    public List<int> potionIndexes = new List<int>();
    public List<string> potionTypes = new List<string>();
    //create dictionary which will hold this games recipies
    public Dictionary<int, string> recipeBook = new Dictionary<int, string>();


    void Start () {
        //create a list of all potion indexes
        for (int i = 0; i <= maxRecipes; i++)
        {
            potionIndexes.Add(i);
        }

        //create list of all recipe types
        potionTypes.Add("TIME_PAUSE");
        potionTypes.Add("DMG_FIREBALL");
        potionTypes.Add("DMG_CHILL");
        potionTypes.Add("DMG_EARTH");
        potionTypes.Add("DMG_LOVE");
        potionTypes.Add("TRANS_FROG");
        potionTypes.Add("TRANS_WEREWOLF");
        potionTypes.Add("TRANS_YOU");
        potionTypes.Add("TRANS_FLOWER");
        potionTypes.Add("HEAL_INC_MAX_HP");
        potionTypes.Add("HEAL_TO_MAX_HP");
        potionTypes.Add("HEAL_INSTA_KILL");
        potionTypes.Add("HEAL_POISON");
        potionTypes.Add("TIME_INC_MOVESPEED");
        potionTypes.Add("TIME_TELEPORT");
        potionTypes.Add("TIME_REWIND");
       
        //for length of potion index
        int randIndex = 0;
        int randType = 0;
        recipeBook.Add(potionIndexes[0], "TIME_PAUSE");
        potionIndexes.RemoveAt(0);
        potionTypes.RemoveAt(0);
        for (int i = 1; i <= maxRecipes; i++)
        {
            //choose random one = index
            randIndex = UnityEngine.Random.Range(0, potionIndexes.Count);
            //choose random potion = pot
            randType = UnityEngine.Random.Range(0, potionIndexes.Count);
            //add to dictionary <index> <pot>
            recipeBook.Add(potionIndexes[randIndex], potionTypes[randType]);
            //remove index frpm indexes
            potionIndexes.RemoveAt(randIndex);
            //remove pot from all potions
            potionTypes.RemoveAt(randType);
        }
    }

    public void DisplayRecipies()
    {
        Debug.Log("---GRANDMA'S ALCHEMY HANDBOOK---");
        for (int i = 0; i <= maxRecipes; i++)
        {
            //Debug.Log("in the loop, "+i);
            Debug.Log("Recipe #" + i + ": " + recipeBook[i] + ". Required Materials: " + DisplayIngredients(i));
        }
    }

    public string DisplayIngredients(int recipeCode)
    {
        //convert the integer into a binary representation
        var recipeBinary = ToBin(recipeCode, 4);
        //work out the binary value for the recipe code. Store in an array of 4? 0, 1, 2, 3 indexes
        return "Material A: x" + recipeBinary[0] + ". "+
            "Material B: x" + recipeBinary[1] + ". " +
            "Material C: x" + recipeBinary[2] + ". " +
            "Material D: x" + recipeBinary[3] + ".";
    }

    //convert the integer into a binary representation (as a string). So 3 turns to "0011".
    public static string ToBin(int value, int len)
    {
        return (len > 1 ? ToBin(value >> 1, len - 1) : null) + "01"[value & 1];
    }

    //taking in the binary string representation, this will look up in the recipeBook and return a string that matches the potionType of that recipe (example "DMG_FIRE")
    public string RecipeToPotion(string binaryString)
    {
        int index = Convert.ToInt32(binaryString, 2);
        return recipeBook[index];
    }

    //taking in the integer spell index, this will look up in the recipeBook and return a string that matches the potionType of that recipe (example "DMG_FIRE")
    public string RecipeToPotion(int spellIndex)
    {
        return recipeBook[spellIndex];
    }
}
