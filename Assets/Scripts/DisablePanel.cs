﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple class for buttons to be linked to in order to deactivate an active panel. Panel set in the inspector.
/// </summary>
public class DisablePanel : MonoBehaviour {

    public void turnOffPanel(GameObject panel)
    {
        panel.gameObject.SetActive(false);
    }
}
