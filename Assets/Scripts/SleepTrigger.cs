﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepTrigger : MonoBehaviour {
    public string whosBed = "Liam";
    private bool dialogOpen = false;

    private void Awake()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player" && !dialogOpen) {
            GameController.Instance.dialogSystem.yesAction = YesToBed;
            GameController.Instance.dialogSystem.show("DO YOU WANT TO HEAD TO BED AND CALL IT A DAY?", 2, true);
                 
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (GameController.Instance.dialogSystem.noAction != null)
        {
            GameController.Instance.dialogSystem.noAction.Invoke();
        }
        //dialogOpen = false;
    }

    public void YesToBed()
    {
        if(whosBed.Contains("Liam"))
        {
            Debug.Log("Liam sleep");
            GameController.Instance.dialogSystem.show("TIME TO REST. TOMORROW WILL BE A BETTER DAY", 2);
            StartCoroutine(GameController.Instance.dayController.resetDayIn(2, true));
        }
        else
        {
            Debug.Log("Non liam sleep");
            GameController.Instance.dialogSystem.show("YOU TAKE YOUR CHANCES. PERHAPS SHE'LL JOIN YOU LATER... TIME TO SLEEP", 2);
            StartCoroutine(GameController.Instance.dayController.resetGameIn(4, "NAUGHTY_LIAM"));
        }

        //GameController.Instance.dialogSystem.yesAction -= YesToBed;
    }
}
