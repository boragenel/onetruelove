﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using com.ootii.Messages;

public class TargetablePlayer: TargetableController
{
    public AudioClip VialHitSFX;
    public bool isDeadKnown = false;
    //reference to the different sprites to show for different states
    public GameObject frogSelf;
    public GameObject werewolfSelf;
    public GameObject rosesSelf;

    public bool isPoisoned = false;

    Animator animator;
    //reference to recipeBook so that we can update it if any potions used on player is the first time they've been used
    public RecipeBookManager _recipeBookManager;


    void Start()
    {
        //initialise fields / references
        animator = GetComponentInChildren<Animator>();
        ds = GameController.Instance.dialogSystem;
        MAX_HP = 100;
        hp = MAX_HP;
        _recipeBookManager = FindObjectOfType<RecipeBookManager>();
    }

    void Update()
    {
        //checks if player is dead, if so - say blacked out, freeze movement, and reset day in X seconds
        handleDeath();
    }


    public void ResetPlayer()
    {
        //tuurn off any states from last day
        rosesSelf.SetActive(false);
        werewolfSelf.SetActive(false);
        frogSelf.SetActive(false);

        //go back to the house
        transform.GetChild(0).localScale = new Vector3(20, 20, 20);
        //turn on the "liam" sprite, restore to full HP
        transform.GetChild(0).gameObject.SetActive(true);
        isDead = false;
        hp = MAX_HP;
        //perform the code that would be done if you were to walk into liams house (music etc)
        GameController.Instance.changeScene(PortalSceneType.LIAM_HOUSE);
        isDeadKnown = false;
        animator.Play("Idle");
    }

    //handling the different click actions on the player
    public override void handleClick(PotionType _ptype)
    {
        //DPj_ should only happen if curDraggedPotion is != null?
        playCastAnim();
        SoundsEffectsManager.Instance.Play(VialHitSFX);
        GameController.Instance.cursorPotion.SetActive(false);

        switch (_ptype)
        {
            case PotionType.DMG_FIREBALL:
                //get hold of the fireball particles
                GameObject fball = GameController.Instance.Fireball;
                fball.SetActive(true);
                fball.transform.position = transform.position;
                //hard code duration as distance tends to be roughly consistent
                float fireDuration = 0.3f;
                //tween the fireball to your current position
                fball.transform.DOMove(transform.position + (Vector3.up * 2), fireDuration).SetEase(Ease.Linear).OnComplete(() => fireballComplete(fball));
                ds.show("GREAT BALLS OF FIRE, YOU'VE SCORCHED YOURSELF!", 2);
                break;
            case PotionType.DMG_CHILL:
                //gets particle effect
                GameObject chill = GameController.Instance.Chill;
                chill.SetActive(true);
                chill.transform.position = transform.position;
                //slows speed for some time and does some damage
                GameController.Instance.StartCoroutine(ChillEffect(chill));
                ds.show("SO... COLD... BRRRRRR. SO HARD TO MOVE...", 2);
                break;
            case PotionType.DMG_EARTH:
                //shakes camera and does delayed damage;
                CameraShake.Shake(7, 2f);
                GameController.Instance.StartCoroutine(DelayedDamage(10, 1f));
                // TODO: damage all the enemies in a radius
                ds.show("THE EARTH BENEATH YOU TREMORS FEROCIOUSLY...", 2);
                break;
            case PotionType.DMG_LOVE:
                //start the ending "liam love" after 6 seconds.
                ds.show("SUDDENLY, YOU FEEL AN OVERWHELMING SENSE OF SELF-LOVE. YOU'RE A STRONG INDEPENDANT MAN, WHO DON'T NEED NO WOMAN TO MAKE YOU HAPPY.", 4);
                GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetGameIn(6, PotionType.LIAM_LOVE)
            );
                break;
            case PotionType.TRANS_FROG:
                //switch sprites
                frogSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
                GameController.Instance.StartCoroutine(FrogEffect());
                ds.show("BUD..... WISE.....", 1);
                break;
            case PotionType.TRANS_WEREWOLF:
                GetComponent<GenericUnit>().SetSpeed(45);
                werewolfSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
                ds.show("*HOOOOOOWL* \n WEREWOLVES CARE NOT FOR ALCHEMY, JUST EATING BLOBS... A WASTED DAY. ", 3);
                //this may need to change - its frog effect COPYPASTA DPJ
                GameController.Instance.StartCoroutine(FrogEffect());
                break;
            case PotionType.TRANS_YOU:
                //set confused variable, which in the controller script inverts the controls
                GetComponent<GenericUnit>().IsConfused = true;
                ds.show("YOU'RE MIND BECOMES CLOUDED... AS YOU PONDER HOW TO TRANSFORM YOURSELF INTO WORTH HUSBAND MATERIAL", 3);
                break;
            case PotionType.TRANS_FLOWER:
                //waits, displays message spending day as flower and restarts
                GameController.Instance.StartCoroutine(FlowerEffect());
                //switch sprites
                rosesSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
                ds.show("FINALLY YOU FEEL ROOTED IN LIFE... BUT SUDDENLY YOU REALISE WHY. YOU'VE BEEN TRANSFORMED INTO BEAUTIFUL ROSES", 3);
                break;
            case PotionType.HEAL_INC_MAX_HP:
                //inc max hp and heal that amount
                MAX_HP += 20;
                addDamage(-20);
                ds.show("YOU FEEL MORE RESILIENT. BRING THE PAIN!", 2);
                // TODO: heal anim
                break;
            case PotionType.HEAL_TO_MAX_HP:
                //heal the difference between max hp and current hp
                ds.show("YOU FEEL REVITALISED. FIGHTING FIT AND READY TO RUMBLE.", 2);
                addDamage(-(MAX_HP - hp));
                break;
            case PotionType.HEAL_INSTA_KILL:
                //insta kill - well blackout
                hp = 0;
                isDeadKnown = false;
                isDead = true;
                break;
            case PotionType.HEAL_POISON:
                //damages 10 damage over 2.5 sec
                StartCoroutine(poisonTick());
                // TODO: add poison effect
                ds.show("YOU FEEL VERY UNWELL... AS THOUGH YOUR LIFE IS BEING SLOWLY DRAINED AWAY BY THE MINUTE...", 3);
                break;
            case PotionType.TIME_INC_MOVESPEED:
                //set the speed of the component that handles the movement
                GetComponent<GenericUnit>().SetSpeed(GetComponent<GenericUnit>().Speed * 1.5f);
                ds.show("YOUR LEGS FEEL CHARGED WITH TREMENDOUS ENERGY... ", 2);
                break;
            case PotionType.TIME_PAUSE:
                GameController.Instance.setState(GameState.PAUSED);
                //ds.show("SUDDENLY, TIME STOOD STILL... SECRET PAUSE MENU Y'ALL", 2);
                break;
            case PotionType.TIME_TELEPORT:
                //move 10 units in the direction you are facing!
                transform.position += -transform.GetChild(0).up * 10;
                ds.show("WOW, TELEPORTATION. LIKE IN THE MOVIES", 1);
                break;
            case PotionType.TIME_REWIND:
                // TODO: Rewind effect on clock
                ds.show("THE HANDS ON THE CLOCK START TURNING BACKWARDS... WHAT THE...", 2);
                //adds one to days and restarts day
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.rewindDayIn(6)
           );
                break;
        }

        //update array of discovered potions
        if (_ptype != PotionType.NONE && _ptype != PotionType.TIME_PAUSE && GameController.Instance.discoveredPotions[_ptype] == false)
        {
            Debug.Log("First time discovering potion, adding to recipe book!");
            GameController.Instance.discoveredPotions[_ptype] = true;
            //call the recipe manager to handle new update
            _recipeBookManager.AddRecipeToBook(_ptype);
        }
        //unequips any potion
        GameController.Instance.curDraggedPotion.type = PotionType.NONE;
    }

    //do five ticks of 2 damage over 2.5 seconds
    public IEnumerator poisonTick()
    {
        if (isPoisoned)
            yield break;
        isPoisoned = true;

        yield return new WaitForSeconds(2f);
        addDamage(2);
        yield return new WaitForSeconds(2f);
        addDamage(2);
        yield return new WaitForSeconds(2f);
        addDamage(2);
        yield return new WaitForSeconds(2f);
        addDamage(2);
        yield return new WaitForSeconds(2f);
        addDamage(2);

        isPoisoned = false;
    }

    public void playCastAnim()
    {
        animator.SetTrigger("Cast");
    }

    //apply damage after a defined delay
    public IEnumerator DelayedDamage(float _amount, float _time)
    { 
        yield return new WaitForSeconds(_time);
        addDamage(_amount);
    }

    //simply waits for some time then resets day, doesn't handle changing into wolf
    public IEnumerator WerewolfEffect()
    {

        yield return new WaitForSeconds(5f);

        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3, true)
            );

    }

    //simply waits for some time then resets day, displays message, doesn't handle changing into frog
    public IEnumerator FrogEffect()
    {
        //reduce movement speed
        GetComponent<CharacterController2D>().Speed *= 0.1f;
        yield return new WaitForSeconds(5f);

        ds.show("YOU SPENT THE DAY AS A FROG. SHAME :(", 2);

        //Debug.Log("========= You spent the day as a frog. Shame :( ===========");
        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3, true)
            );
    }

    //simply waits for some time then resets day, displays message, doesn't handle changing into wolf
    public IEnumerator FlowerEffect()
    {
        GetComponent<CharacterController2D>().Speed = 0;
        //ds.show("YOU ARE A FLOWER NOW :/", 1);
        // TODO: TURN YOURSELF INTO A FROG
        yield return new WaitForSeconds(5f);
        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3, true)
            );

    }

    //slow down, apply damage, wait, resume speed, turn off ice particle effect
    public IEnumerator ChillEffect(GameObject _chill)
    {
        GetComponent<CharacterController2D>().Speed *= 0.7f;
        addDamage(15);
        // TODO: add permanent chill effect on player
        yield return new WaitForSeconds(3f);

        _chill.SetActive(false);

    }

    //called when fireball hits target. Adds damage and deactivates the fireball effect.
    public void fireballComplete(GameObject fball)
    {
        addDamage(30);
        fball.SetActive(false);

    }

    //checked every update frame, shows message and then handles the animations / speed changes and calls coroutine to reset day
    public override void handleDeath()
    {


        if (isDead)
        {
            //gameObject.SetActive(false);
            if (!isDeadKnown)
            {
                GameController.Instance.dialogSystem.show("YOU PASSED YOUR PAIN TRESHOLD. \n ENJOY THE BLACKOUT.", 2);
                GameController.Instance.StartCoroutine(GameController.Instance.dayController.resetDayIn(3.5f, true));
                isWalking = false;
                animator.SetTrigger("Faint");
                isDeadKnown = true;

            }
            GetComponent<CharacterController2D>().Speed = 0;

        }


    }



}
