﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//a base class for all things that can be targeted by potions - tend to be "creatures / units".
public abstract class TargetableController : MonoBehaviour {

    protected DialogSystem ds;
    public int MAX_HP = 100;
    public float hp = 100;
    public float speed = 10;
    public float damage = 5;
    public float INIT_DAMAGE = 5;
    public float INIT_SPEED = 10;
    public bool isDead = false;
    public bool isWerewolf = false;
    public bool isFrog = false;
    public bool isWalking = false;

    public AudioClip DamageSFX;

    //all subclasses need to define what to do when clicked.
    public abstract void handleClick(PotionType _ptype);

    //
    public void addDamage(float _amount)
    {
        if (DamageSFX != null)
            SoundsEffectsManager.Instance.Play(DamageSFX);

        bool _dmg = _amount > 0;

        //set scrolling text for the damage applies

        //reduce the units health, and set to be dead if relevent 
        hp -= _amount;
        if (hp <= 0)
            isDead = true;
    }

    //all subclasses should define what to do when dead, which will be called when "isDead" I assume DPJ
    public abstract void handleDeath();

}
