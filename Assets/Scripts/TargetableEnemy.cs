﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using com.ootii.Messages;

//definition of targetable class to be associated with hostile creatures found in the woods
public class TargetableEnemy : TargetableController {

    public AudioClip VialHitSFX;
    //the position of the player
    Transform player;
    Animator animator;
    //flag to allow some creatures to remain during daily level generation
    public bool dontResetAtTheEndOfTheDay = false;
    /// <summary>
    /// reference to reciep book manager needed, so an update can be called upon it if a potion is used on ths enemy for the first time
    /// </summary>
    public RecipeBookManager _recipeBookManager;

    public float regentDropChance = 1f; // between 0 and 1 (design choice - set to 100% for now)
    

	void Start () {
        player = GameController.Instance.player.transform;
        //reference to the dialog system as we need display messages when different potions are applies to the different enemies
        ds = GameController.Instance.dialogSystem;
        //set variables based on the base class
        damage = INIT_DAMAGE;
        MAX_HP = 30;
        hp = MAX_HP;
        speed = INIT_SPEED;
        //the units need to know when the day is finished, so they can reset (disabled)
        MessageDispatcher.AddListener("ResetDay", ResetEnemy);
        animator = GetComponentInChildren<Animator>();
        _recipeBookManager = FindObjectOfType<RecipeBookManager>();
    }

    //function to call upon end of day on each enemy of the type
    public void ResetEnemy(IMessage msg)
    {
        //reinitialise settigns based on base class
        damage = INIT_DAMAGE;
        MAX_HP = 30;
        hp = MAX_HP;
        speed = INIT_SPEED;
        isDead = false;

        //if you're a werewolf who resets, or a frog, then you need to be deactivated in the object pool. 
        if ((isWerewolf && !dontResetAtTheEndOfTheDay) || isFrog )
            ObjectPool.Instance.killObject(gameObject);
        //otherwise, we just set everything else to be a blob. 
        else
            GetComponentInChildren<SpriteRenderer>().sprite = GameController.Instance.blob;
        //enable the collider of the enemies, as this may have been switched off (when turned into flowers etc)
        GetComponentInChildren<Collider2D>().enabled = true;
    }

    void Update () {
        //check if the enemy is dead, if it is - then we deactivate it and drop materials base on the drop chance
        handleDeath();
    }

    //what to do when clicked on
    public override void handleClick(PotionType _ptype)
    {

        player.GetComponent<TargetablePlayer>().playCastAnim();
        GameController.Instance.cursorPotion.SetActive(false);

        //work out difference in distance between player and this enemy, turn it degrees and minus 90 off of it for some reason. Get the angle from player to enemy essesntially.
        float angleBetween = (Mathf.Atan2(player.position.y - transform.position.y, player.position.x - transform.position.x) * Mathf.Rad2Deg) - 90;

        //dont play a sound if there is no potion current equipped
        if (_ptype != PotionType.NONE) { 
            SoundsEffectsManager.Instance.Play(VialHitSFX);
        }
        //the things to do based on the different potions effect
        switch (_ptype)
        {

            case PotionType.DMG_FIREBALL:
                //get hold of the fireball particles
                GameObject fball = GameController.Instance.Fireball;
                fball.SetActive(true);
                //originate the fireball from the player
                fball.transform.position = player.position;
                //set the angle of fireball based on angle between player and enemy
                fball.transform.eulerAngles = new Vector3(0, 0, angleBetween);
                //work out how long it'll take to get to the enemy in seconds based on the distance away
                float fireDuration = Mathf.Abs((player.position - transform.position).magnitude / 60f);
                //tween (animate) the fireball between the two positions, and when finished run the fireballComplete function.
                fball.transform.DOMove(transform.position, fireDuration).SetEase(Ease.Linear).OnComplete( ()=>fireballComplete(fball)) ;
                //is this to stop the enemy moving while fire ball approaches? DPJ
                GetComponent<DummyEnemyAI>().conciousTimer = fireDuration;
                break;
            case PotionType.DMG_CHILL:
                //gets particle effect
                GameObject chill = GameController.Instance.Chill;
                chill.SetActive(true);
                chill.transform.position = transform.position;
                //applies damage and slow down immediately. After 3 seconds, resumes to normal speed.
                GameController.Instance.StartCoroutine(ChillEffect(chill));
                break;
            case PotionType.DMG_EARTH:
                CameraShake.Shake(7, 2f);
                //stops movement for 2 seconds on the enemy
                GetComponent<DummyEnemyAI>().conciousTimer = 2;
                //apply damage to the enemy after 1 seconds
                GameController.Instance.StartCoroutine(DelayedDamage(15,1f));
                break;
            case PotionType.DMG_LOVE:
                //deactivates the enemy, cannot return to consciousness
                GetComponent<DummyEnemyAI>().conciousTimer = int.MaxValue;
                //set the love particles to start and move them to the position of the enemy
                GameController.Instance.Love.SetActive(true);
                GameController.Instance.Love.transform.position = transform.position;
                GetComponentInChildren<Collider2D>().enabled = false;
                break;
            case PotionType.TRANS_FROG:
                ds.show("LOOKS LIKE YOU TURNED IT INTO A FROG... ARE YOU GONNA KISS IT?!", 2);
                //deactivate the old enemy, and instantiate a new object at the position. 
                ObjectPool.Instance.killObject(gameObject);
                GameObject frogGo = ObjectPool.Instance.useObject(ObjectPool.Instance.frogPrefab);
                frogGo.transform.position = transform.position;
                break;
            case PotionType.TRANS_WEREWOLF:
                //deactate the object, then create a new werewolf. Not sure why we are adjusting speed and damage if the werewolf prefab is instantiated. 
                speed = 40;
                ObjectPool.Instance.killObject(gameObject);
                damage = 40;
                GameObject go = ObjectPool.Instance.useObject(ObjectPool.Instance.werewolfPrefab);
                go.transform.position = transform.position;
                break;
            case PotionType.TRANS_YOU:
                speed = 0;
                //change the image to pink liam
                GetComponentInChildren<SpriteRenderer>().sprite = GameController.Instance.liam;
                //drops an item
                checkDropItem();
                //make the enemy not responde for a very long time
                GetComponent<DummyEnemyAI>().conciousTimer = int.MaxValue;
                break;
            case PotionType.TRANS_FLOWER:
                ds.show("WOW. WHAT A WONDERFUL FLOWER! WOULD LOOK LOVELY IN A VASE AT HOME...", 2);
                checkDropItem();
                GetComponentInChildren<SpriteRenderer>().sprite = GameController.Instance.rose;
                GetComponent<DummyEnemyAI>().conciousTimer = int.MaxValue;
                break;
            case PotionType.HEAL_INC_MAX_HP:
                ds.show("WHAT A WISE MOVE... IT SEEMS YOU'VE MADE BLOBBA-FET MORE RESISTANT", 2);
                //increase max HP of this enemy, and heal itself so that it had those extra hitpoints, not just an increase max hitpoints.
                MAX_HP += 20;
                addDamage(-20);
                break;
            case PotionType.HEAL_TO_MAX_HP:
                ds.show("WHAT COMPASSION YOU'VE SHOWN! YOU'VE RESTORED IT'S HEALTH!", 2);
                //heal however many hp are missing from the enemies health pool
                addDamage(-(MAX_HP - hp));
                break;
            case PotionType.HEAL_INSTA_KILL:
                ds.show("THAT MADE QUICK WORK OF THAT BLOB BETTER NOT SPILL THIS POTION...", 2);
                hp = 0;
                isDead = true;
                break;
            case PotionType.HEAL_POISON:
                //poison tick just applies 2 damage every X amount of time
                StartCoroutine(poisonTick());
                ds.show("YOU'VE POISONED THE BLIGHTED BLOB", 1);
                // TODO: add poison effect
                break;
            case PotionType.TIME_INC_MOVESPEED:
                speed *= 1.5f;
                ds.show("OH NO, IT'S MOVING FASTER NOW!", 2);
                break;
            case PotionType.TIME_PAUSE:
                GameController.Instance.setState(GameState.PAUSED);
                //ds.show("SUDDENLY, TIME STOOD STILL... SECRET PAUSE MENU Y'ALL", 2);
                break;
            case PotionType.TIME_TELEPORT:
                //switch position of player and enemy - so does getting player transform at start mean we can ammend it and it changes the players transform too? Not just a copy? DPJ
                Vector3 _pos = transform.position;
                transform.position = player.transform.position;
                player.transform.position = _pos;
                break;
            case PotionType.TIME_REWIND:
                // TODO: Edit graphics of clock to go backwards! maybe just need to change time value
                ds.show("THE HANDS ON THE CLOCK START TURNING BACKWARDS... WHAT THE...",2);
                //set the game to reset in 3 seconds, whilst adding 1 extra to the day once the reinitialiation has done.
                GameController.Instance.StartCoroutine(
          GameController.Instance.dayController.rewindDayIn(3)
          );
                break;
        }

        //update array of discovered potions
        if (_ptype != PotionType.NONE && _ptype != PotionType.TIME_PAUSE && GameController.Instance.discoveredPotions[_ptype] == false)
        {
            //Debug.Log("First time discovering potion, adding to recipe book!");
            GameController.Instance.discoveredPotions[_ptype] = true;
            //call the recipe manager to handle new update
            _recipeBookManager.AddRecipeToBook(_ptype);
        }
        //face the player towards the place that was clicked.
        player.transform.eulerAngles = new Vector3(0, 0, angleBetween);
        //reset the potion
        GameController.Instance.curDraggedPotion.type = PotionType.NONE;

    }

    //do five ticks of 2 damage over 2.5 seconds
    public IEnumerator poisonTick()
    {
        yield return new WaitForSeconds(0.5f);
        addDamage(2);
        yield return new WaitForSeconds(0.5f);
        addDamage(2);
        yield return new WaitForSeconds(0.5f);
        addDamage(2);
        yield return new WaitForSeconds(0.5f);
        addDamage(2);
        yield return new WaitForSeconds(0.5f);
        addDamage(2);
    }

    //apply damage after a defined delay
    public IEnumerator DelayedDamage(float _amount,float _time)
    { 
        yield return new WaitForSeconds(_time);
        addDamage(_amount);
    }

    //slow down, apply damage, wait, resume speed, turn off ice particle effect
    public IEnumerator ChillEffect(GameObject _chill)
    {
        speed *= 0.5f;
        addDamage(15);
        yield return new WaitForSeconds(3f);
        speed *= 2;
        _chill.SetActive(false);
    }

    //called when fireball hits target. Adds damage and deactivates the fireball effect.
    public void fireballComplete(GameObject fball)
    {
        addDamage(30);
        fball.SetActive(false);      
    }
    
    //spawns a random material based on the regant drop chance
    public void checkDropItem()
    {
        if( Random.Range(0f,1f) < regentDropChance)
        {
            GameObject go = ObjectPool.Instance.useObject(ObjectPool.Instance.pickupPrefab);
            go.transform.position = transform.position;

        }
    }

    //if yo dead, deactivate and drop yo loots
    public override void handleDeath()
    {
        if( isDead )
        {
            gameObject.SetActive(false);
            checkDropItem();
        }
    }


}
