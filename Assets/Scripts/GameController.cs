﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using DG.Tweening;
using UnityEngine.SceneManagement;

/// <summary>
/// Enum to state the current state of the game
/// </summary>
public enum GameState
{
    PAUSED,
    DURING,
    TRANSITION,
    END
}

public class GameController: MonoBehaviour
{

    public GameState prevState;
    public GameState currentState = GameState.DURING;
    public GameObject hudCanvas;
    public Sprite[] potionVariations;

    public GameObject cursorPotion;

    public bool isHardMode = false;

    public GameObject player;
    public OTLUnit otlUnit;

    public Camera mainCam;

    public Dictionary<string, int> potionUsages = new Dictionary<string, int>();

    public bool isOptionsVisible = false;

    public PortalSceneType currentScene;

    [Header("Scenes")]
    public Transform town;
    public Transform forest1;

    [Header("Portals")]
    public Transform forestPortal1;
    public Transform townLiamPortal;
    public Transform townForestPortal;
    public Transform townOtlPortal;
    public Transform liamHousePortal;
    public Transform otlHousePortal;

    [Header("Particles")]
    
    public GameObject Fireball;
    public GameObject Chill;
    public GameObject Love;
    public ParticleSystem deathParticle;
    public ParticleSystem transformParticle;
    public ParticleSystem PickupHint;
    
    public GameObject wind;

    [Header("UI")]
    public DialogSystem dialogSystem;

    [Header("Music")]
    public AudioSource townMusic;
    public AudioSource forestMusic;
    public AudioSource endingMusic;
    public AudioClip loveEnding;

    [Header("SFX")]
    public AudioClip doorOpenCloseSFX;
    public AudioSource windAudio;

    [Header("Controllers")]
    public DayController dayController;
    public EndingManager endingManager;
    public InventoryManager inventoryManager;
    public MaterialManager materialManger;
    public DamageInfoManager damageInfoManager;
    public GameObject craftingTable;
    



    [Header("Sprites")]
    public Sprite frog;
    public Sprite werewolf;
    public Sprite rose;
    public Sprite liam;
    public Sprite blob;

    //the potion that is currently equipped by the player (after clicking on it from your hotbar)
    public Potion curDraggedPotion = null;

    /// <summary>
    /// Stores all potions and a meaningful description that can be displayed within the recipe book
    /// </summary>
    public Dictionary<PotionType, string> potionDescriptions = new Dictionary<PotionType, string>();
    /// <summary>
    /// Keeps track of all potions and whether or not they have been discovered or not yet
    /// </summary>
    public Dictionary<PotionType, bool> discoveredPotions = new Dictionary<PotionType, bool>();


    // Singleton instance
    static GameController _instance;
 
    void Start()
    {
        Application.targetFrameRate = 60;

        GameObject musicManager = transform.Find("MusicManager").gameObject;
        townMusic = musicManager.GetComponents<AudioSource>()[0];
        forestMusic = musicManager.GetComponents<AudioSource>()[1];
        endingMusic = musicManager.GetComponents<AudioSource>()[2];
        //this will set up the two dictionaries storing the descriptions and whether or not they've been discovered yet
        InitPotionDictionaries();
        //create an instance of potion, that will be updates as different potions are equipped / used
        curDraggedPotion = new Potion();

        mainCam = Camera.main;

        if( !PlayerPrefs.HasKey("HardMode") )
        {
            PlayerPrefs.SetInt("HardMode",0);
        }

        int hardMode = PlayerPrefs.GetInt("HardMode");

        if (hardMode == 1)
            isHardMode = true;

        

        if ( isHardMode )
        {
            otlUnit._recipeBookManager.gameObject.SetActive(false);
        }

        setState(GameState.DURING);
    }

    // Update is called once per frame
    void Update()
    {
        handleInput();
    }

    static public GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = UnityEngine.Object.FindObjectOfType(typeof(GameController)) as GameController;

                if (_instance == null)
                {
                    GameObject go = new GameObject("GameController");
                    DontDestroyOnLoad(go);
                    _instance = go.AddComponent<GameController>();
                }
            }
            return _instance;
        }
    }


    public void setState(GameState _state)
    {


        prevState = currentState;
        currentState = _state;

        handleNewState();



    }

    public void handleInput()
    {
        //handles escape to pause / show menu
        if (Input.GetKeyDown(KeyCode.Escape) )
        {
            if(craftingTable.activeSelf)
            {
                craftingTable.SetActive(false);
                return;
            } else
            {

                //endingManager.pause();
                //loads the scene that includs the options window. UPDATE: should make one with an Exit button.
                isOptionsVisible = !isOptionsVisible;
                if( isOptionsVisible )
                    SceneManager.LoadScene("Options", LoadSceneMode.Additive);
                else
                {
                    SceneManager.UnloadScene("Options");
                }

                return;
            }
            
        }
        //relasing mouse button
        if (Input.GetMouseButtonUp(0))
        {
            //get the x/y co-ordinate in game of where the mouse has clicked
            Vector2 worldTouch = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //create a line from 10 units back (in line with camera) in straight direction, in order to see if we intersect with any game objects
            RaycastHit2D hit;
            hit = Physics2D.Raycast(new Vector3(worldTouch.x, worldTouch.y, -10), Vector2.zero, Mathf.Infinity);
            //if we clicked on something that we've flagged as ray castable
            if (hit)
            {
                //get hold of what type of potion was in hand at the time of clicking.
                PotionType pt = PotionType.NONE;
                if (curDraggedPotion != null)
                {
                    pt = curDraggedPotion.type;
                }
                //targetable controlle being base class for OTL, player and enemy targetable items
                var controller = hit.collider.GetComponentInParent<GenericUnit>();
                //if we clicked on one of our valid potion-intractible controllers, call the abstract class Handle Click
                if (controller != null)
                    controller.ApplyPotion(pt);
            }
        }
        //To handle accesing the UI slots with keybinds.
        if( Input.GetKeyUp(KeyCode.Alpha1))
        {
            inventoryManager.ClickUISlot(1);
        } else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            inventoryManager.ClickUISlot(2);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            inventoryManager.ClickUISlot(3);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha4))
        {
            inventoryManager.ClickUISlot(4);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha5))
        {
            inventoryManager.ClickUISlot(5);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha6))
        {
            inventoryManager.ClickUISlot(6);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha7))
        {
            inventoryManager.ClickUISlot(7);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha8))
        {
            inventoryManager.ClickUISlot(8);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha9))
        {
            inventoryManager.ClickUISlot(9);
        }
    }

    //things to do each time the state is changed
    public void handleNewState()
    {
       
        switch (currentState)
        {
            case GameState.PAUSED:
                endingManager.pause();
                break;
        }

    }

    /// <summary>
    /// Handles the settings (environment/sound/positioning of player) of different portals and the areas they teleport you to.
    /// </summary>
    /// <param name="_type"></param>
    public void changeScene(PortalSceneType _type)
    {
        Transform curPortal = null;
        
        switch (_type)
        {

            case PortalSceneType.FOREST:
                windAudio.Play();
                curPortal = forestPortal1;
                forestMusic.Play();
                forestMusic.DOFade(0.4f, 1);
                townMusic.DOFade(0, 1);
                wind.SetActive(true);
                break;

            case PortalSceneType.TOWN_FOREST_ENTRANCE:
                windAudio.Play();
                curPortal = townForestPortal;
                townMusic.Play();
                forestMusic.DOFade(0, 1);
                townMusic.DOFade(0.4f, 1);
                wind.SetActive(true);
                break;

            case PortalSceneType.LIAM_HOUSE:
                windAudio.Stop();
                wind.SetActive(false);
                curPortal = liamHousePortal;
                
                break;

            case PortalSceneType.OTL_HOUSE:
                windAudio.Stop();
                wind.SetActive(false);
                curPortal = otlHousePortal;
                
                break;

            case PortalSceneType.TOWN_LIAM:
                windAudio.Play();
                wind.SetActive(true);
                curPortal = townLiamPortal;
                
                break;

            case PortalSceneType.TOWN_OTL:
                windAudio.Play();
                wind.SetActive(true);
                curPortal = townOtlPortal;
                
                break;
        }

        player.transform.position = curPortal.GetChild(0).position;

    }

    public void InitPotionDictionaries() { 
        //array of potion types and semantical meaning. 
        potionDescriptions.Add(PotionType.TIME_PAUSE, "Pause Time");
        potionDescriptions.Add(PotionType.DMG_FIREBALL, "Fire");
        potionDescriptions.Add(PotionType.DMG_CHILL, "Ice");
        potionDescriptions.Add(PotionType.DMG_EARTH, "Earthquake");
        potionDescriptions.Add(PotionType.DMG_LOVE, "Potion of Love!");
        potionDescriptions.Add(PotionType.TRANS_FROG, "Frog!");
        potionDescriptions.Add(PotionType.TRANS_WEREWOLF, "Werewolf? #teamjacob");
        potionDescriptions.Add(PotionType.TRANS_YOU, "Self clone");
        potionDescriptions.Add(PotionType.TRANS_FLOWER, "Flowers!?");
        potionDescriptions.Add(PotionType.HEAL_INC_MAX_HP, "Increase fortitude");
        potionDescriptions.Add(PotionType.HEAL_TO_MAX_HP, "Heal");
        potionDescriptions.Add(PotionType.HEAL_INSTA_KILL, "Potion of Death");
        potionDescriptions.Add(PotionType.HEAL_POISON, "Poison");
        potionDescriptions.Add(PotionType.TIME_INC_MOVESPEED, "Haste");
        potionDescriptions.Add(PotionType.TIME_TELEPORT, "Teleportation");
        potionDescriptions.Add(PotionType.TIME_REWIND, "Rewind");

        discoveredPotions.Add(PotionType.TIME_PAUSE, true);     //always discovered because we don't want to add to recipe book
        discoveredPotions.Add(PotionType.DMG_FIREBALL, false);
        discoveredPotions.Add(PotionType.DMG_CHILL, false);
        discoveredPotions.Add(PotionType.DMG_EARTH, false);
        discoveredPotions.Add(PotionType.DMG_LOVE, false);
        discoveredPotions.Add(PotionType.TRANS_FROG, false);
        discoveredPotions.Add(PotionType.TRANS_WEREWOLF, false);
        discoveredPotions.Add(PotionType.TRANS_YOU, false);
        discoveredPotions.Add(PotionType.TRANS_FLOWER, false);
        discoveredPotions.Add(PotionType.HEAL_INC_MAX_HP, false);
        discoveredPotions.Add(PotionType.HEAL_TO_MAX_HP, false);
        discoveredPotions.Add(PotionType.HEAL_INSTA_KILL, false);
        discoveredPotions.Add(PotionType.HEAL_POISON, false);
        discoveredPotions.Add(PotionType.TIME_INC_MOVESPEED, false);
        discoveredPotions.Add(PotionType.TIME_TELEPORT, false);
        discoveredPotions.Add(PotionType.TIME_REWIND, false);


    }

    //returns true if it was the first time the achievement was unlocked
    public bool UnlockAchievement(string playerPrefName)
    {
        Debug.Log("Attempting to unlock achievement");
        if (PlayerPrefs.HasKey(playerPrefName))
        {
            //already unlocked, no need for notification
            if (PlayerPrefs.GetInt(playerPrefName) == 1)
            {
                Debug.Log("already earned achievement "+playerPrefName);
                return false;
            }
            //first time unlocking, set the player pref
            else
            {
                PlayerPrefs.SetInt(playerPrefName, 1);
                Debug.Log("player pref existed, first time earning achievement " + playerPrefName);
                return true;
            }
        }
        //first time unlokcing, but player pref didn't exist already
        else
        {
            PlayerPrefs.SetInt(playerPrefName, 1);
            Debug.Log("no pref existed, first time earning achievement " + playerPrefName);
            return true;
        }        
    }

}
