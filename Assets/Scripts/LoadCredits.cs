﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using com.ootii.Messages;

/// <summary>
/// Changes the scene, but also ensures all existing listeners from the game scene are unsubscribed before the scene is changed.
/// </summary>
public class LoadCredits : MonoBehaviour {

    public void LoadTheCredits()
    {
        //ooti is a tidy event broadcast/listening utility
        //Debug.Log("loading credits");
        MessageDispatcher.ClearListeners();
        SceneManager.LoadScene("Credits");
    }
}
