﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransition : MonoBehaviour {
    public Material TransitionMaterial;
    [Range(0f,1f)]
    public float Completion = 0f;

    private static CameraTransition _instance;
    //soft singleton declaration
    public static CameraTransition Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (CameraTransition)FindObjectOfType(typeof(CameraTransition));
            }
            return _instance;
        }
    }

    public Action OnTransitionFinished;
    public Action OnTransitionStarted;

    //when we start, we go from black to transparent (1f = all black, 0f heart finished and everything in sight) DPJ?
    private void Start()
    {
        Completion = 1f;
        //first argument is time, second is target 
        Transition(1f, 0f);
    }

    public void Transition(float duration, float targetValue)
    {
        //delta is how far from completion are we currently - not in time but in terms of 0-1 transition progress
        float delta = targetValue - Completion;
        //does this scale delta based on the duration? so we transition smoothly across the entire duration DPJ
        delta /= duration;
        StartCoroutine(_doTransition(delta, duration));
        if (OnTransitionStarted != null)
        {
            OnTransitionStarted.Invoke();
        }
    }

    private IEnumerator _doTransition(float deltaValue, float duration)
    {
        float time = 0;
        //start a counter
        while(time < duration)
        {
            time += Time.unscaledDeltaTime;
            //add to our completion rate based on the deltavalue (a unit we need to increase by to finish transition within the duration)
            Completion += Time.unscaledDeltaTime * deltaValue;
            Completion = Mathf.Clamp01(Completion);
            //only do this once per frame
            yield return new WaitForEndOfFrame();
        }
        //broadcast that the transition has finished
        if(OnTransitionFinished != null)
        {
            OnTransitionFinished.Invoke();
        }
        yield return null;
    }

    private void Update()
    {
        //update the material that covers the screen based on current compeltion rate. Should only really change things during transition.
        TransitionMaterial.SetFloat("_Cutoff", Completion);
    }

    //Yeah, don't know about this one DPJ
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, TransitionMaterial);
    }
}
