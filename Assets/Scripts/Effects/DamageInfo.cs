﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class DamageInfo : MonoBehaviour {

    public int amount;
    public TextMeshProUGUI text;
    private Tweener tweener1;
    private Tweener tweener2;
    public bool isPlaying = false;


    public void Show(Vector3 pos, float amount)
    {
        //if there is already a text scolling, get rid of them to start scrolling this text?
        tweener1.Kill();
        tweener2.Kill();

        isPlaying = true;
        gameObject.SetActive(true);

        //is this so it's on top of everything and we always see it?
        pos.z = -6f;
        //set the text to be on the position passed in, normally the player / enemy position
        transform.position = pos;
        text.transform.localPosition = Vector3.zero;
        //if damage, make it red. Otherwise, make it green. 
        text.color = amount > 0 ? new Color(0.8f, 0.2f, 0.2f, 1) : new Color(0.2f, 0.8f, 0.2f, 1);
        text.text = amount > 0 ? "-" : "+";
        //get rid of minus symbol so we don't have -- or whatever
        text.text += Mathf.Abs(amount).ToString();
        //scroll up 5 units on the Y axis over 1.6 seconds
        tweener1 = text.transform.DOLocalMoveY(-5, 1.6f).OnComplete(stopShowing);
        //make it invisible after 1.6s? does it fade?
        tweener2 =text.DOColor(new Color(0, 0, 0, 0), 1.6f);
    }


    public void stopShowing()
    {
        gameObject.SetActive(false);
        isPlaying = false;

    }

}
