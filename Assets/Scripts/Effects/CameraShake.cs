﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class CameraShake : MonoBehaviour
{

    public static CameraShake instance;

    private Vector3 _originalPos;
    private float _timeAtCurrentFrame;
    private float _timeAtLastFrame;
    private float _fakeDelta;

    CameraFollowPlayer cam;

    void Awake()
    {
        instance = this;
        cam = Camera.main.GetComponent<CameraFollowPlayer>();
    }
    private void Start()
    {


    }




    void Update()
    {
        // Calculate a fake delta time, so we can Shake while game is paused.
        _timeAtCurrentFrame = Time.realtimeSinceStartup;
        _fakeDelta = _timeAtCurrentFrame - _timeAtLastFrame;
        _timeAtLastFrame = _timeAtCurrentFrame;
    }

    public static void Shake(float duration, float amount)
    {
        instance._originalPos = instance.gameObject.transform.localPosition;
        instance.StopAllCoroutines();
        instance.StartCoroutine(instance.cShake(duration, amount));
    }

    public IEnumerator cShake(float duration, float amount)
    {

            float endTime = Time.time + duration;

            while (duration > 0)
            {
                    //shake a large amount (within a sphere of current position). 
                    //as duration goes on, the sphere in which the shake placement is chose is reduced (less shake as it goes on)
                    cam.shake = Random.insideUnitSphere * amount;
                    //camFollow.shakeRot = Random.insideUnitSphere * amount * 60;
                    duration -= _fakeDelta;
                    if (endTime > Time.time + (duration * 0.5f))
                        amount *= 0.9f;
                //DPJ - does it go back to normal positon due to the follow player script on camera?
                yield return null;
            }

     
    }
}