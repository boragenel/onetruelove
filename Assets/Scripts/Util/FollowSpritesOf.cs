﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowSpritesOf : MonoBehaviour {
    //unsure what or where this is used DPJ
    public SpriteRenderer followTarget;
    public float yOffset = 4;
    public bool useScaleAsZ = false;
    public Vector3 baseScale;
    public Vector3 baseTargetScale;
    SpriteRenderer myRenderer;
    // Use this for initialization
    void Start () {
        myRenderer = GetComponent<SpriteRenderer>();
        baseScale = transform.localScale;
        baseTargetScale = followTarget.transform.localScale;

    }
	
	// Update is called once per frame
	void LateUpdate () {
        myRenderer.sprite = followTarget.sprite;

        //myRenderer.transform.localScale = new Vector3(20 + ((180f - Mathf.Abs(angle)*20 )/ 180f), 33, 1);

        transform.position = followTarget.transform.position + (Vector3.up * yOffset);

        if (useScaleAsZ)
        {
            transform.localScale = (baseTargetScale.x / followTarget.transform.localScale.x) * baseScale;
            transform.position += (followTarget.transform.localScale.x / baseTargetScale.x) * (-Vector3.up * yOffset);
        }
      

    }
}
