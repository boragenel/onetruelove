﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController: MonoBehaviour
{
    public GameObject charObject;

    bool movingUp = false;
    bool movingLeft = false;
    bool movingRight = false;
    bool movingDown = false;


    public float moveSpeed = 30;
    [HideInInspector]
    public static float INIT_MOVE_SPEED = 30;

    public bool confusion = false;

    public bool collidingWithSomething = false;


    void Update()
    {
        handleInput();
        handleMovement();
        handleRotation();
    }

    
    public void changeSpeedPermanently(float _newSpeed)
    {
        moveSpeed = _newSpeed;
        INIT_MOVE_SPEED = _newSpeed;
    }

    //set the movemenet booleans based on key up and down of WASD
    private void handleInput()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            movingUp = true;
        }
        else
       if (Input.GetKeyUp(KeyCode.W))
        {
            movingUp = false;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            movingDown = true;
        }
        else
        if (Input.GetKeyUp(KeyCode.S))
        {
            movingDown = false;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            movingLeft = true;
        }
        else
        if (Input.GetKeyUp(KeyCode.A))
        {
            movingLeft = false;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            movingRight = true;
        }
        else
        if (Input.GetKeyUp(KeyCode.D))
        {
            movingRight = false;
        }
    }

    private void handleMovement()
    {
        //if moving diagonally, reduce the move speed in order to normalise the distance moved per unit of time
        float moveSpeedNew = moveSpeed;
        if ((movingUp || movingDown) && (movingLeft || movingRight))
            moveSpeedNew *= 0.8f;

        //slow down movement when bumping into things
        if (collidingWithSomething)
            moveSpeed = 20;

        //controls for when confused (Inverted)
        if ((movingUp && !confusion) || (movingDown && confusion))
        {
            transform.position += Vector3.up * moveSpeedNew * Time.deltaTime;
        }
        else
        if ((movingUp && confusion) || (movingDown && !confusion))
        {
            transform.position += -Vector3.up * moveSpeedNew * Time.deltaTime;
        }
        if ((movingRight && confusion) || (movingLeft && !confusion))
        {
            transform.position += -Vector3.right * moveSpeedNew * Time.deltaTime;
        }
        else
        if ((movingRight && !confusion) || (movingLeft && confusion))
        {
            transform.position += Vector3.right * moveSpeedNew * Time.deltaTime;
        }

    }


    private void handleRotation()
    {
        //set rotation based on the moving direction. Instantly changes to that angle
        if (movingUp && movingRight)
        {
            charObject.transform.localEulerAngles = new Vector3(0, 0, 135);
        }
        else
        if (movingUp && movingLeft)
        {
            charObject.transform.localEulerAngles = new Vector3(0, 0, -135);
        }
        else
        if (movingDown && movingLeft)
        {
            charObject.transform.localEulerAngles = new Vector3(0, 0, -45);
        }
        else
        if (movingDown && movingRight)
        {
            charObject.transform.localEulerAngles = new Vector3(0, 0, 45);
        }
        else
        if (movingDown)
        {
            charObject.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else
        if (movingUp)
        {
            charObject.transform.eulerAngles = new Vector3(0, 0, 180);
        }
        else
        if (movingLeft)
        {
            charObject.transform.eulerAngles = new Vector3(0, 0, -90);
        }
        else
        if (movingRight)
        {
            charObject.transform.eulerAngles = new Vector3(0, 0, 90);
        }

        if (confusion && (movingDown || movingUp || movingLeft || movingRight))
            charObject.transform.eulerAngles += new Vector3(0, 0, 180);

    }

    //keeping track of when we are "rubbing up against something"
    private void OnCollisionEnter2D(Collision2D collision)
    {
        collidingWithSomething = true;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collidingWithSomething = false;
    }


}
