﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;

public class RandomizableCircleArea: MonoBehaviour
{
    [Header("Generation Parameters")]
    public float Radius = 50f;
    public float ItemRadius = 2f;
    public int MaxTries = 20;

    //which items to generate randomly, and how many we should generate (min/max)
    public List<GameObject> listOfPrefabs;
    public List<int> minAmountOfInstancesForEachPrefab;
    public List<int> maxAmountOfInstancesForEachPrefab;
    public List<float> amountOfIncreaseEveryDay;


    public List<GameObject> generatedObjects = new List<GameObject>();

    ObjectPool pool;

    void Start()
    {
        generate();
        MessageDispatcher.AddListener("ResetDay", ResetGeneration);
    }

    //this called at the start of each new day, I assume DPJ
    public void ResetGeneration(IMessage msg)
    {
        killAll();
        generate();
    }

    //way to add visuals onto the scene view to give visual representation of the areas items can spawn
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, Radius);
    }

    public void generate()
    {
        pool = ObjectPool.Instance;
        GameObject tempObject;
        //for each prefab that we want to randomly generate
        for (int i = 0; i < listOfPrefabs.Count; i++)
        {
            //choose an amount of that prefab that you want to generate - chooses random number between min amount and a scaled max amount amountIncreaseEveryDay and also day count
            int randomCreateAmount = Random.Range(minAmountOfInstancesForEachPrefab[i],Mathf.RoundToInt( maxAmountOfInstancesForEachPrefab[i] * amountOfIncreaseEveryDay[i] * (Mathf.Clamp(GameController.Instance.dayController.currentDay*0.5f, 1f, 7f))));

            //create randomCreateAmount of them, requesting the object we want from the an pool
            for (int j = 0; j < randomCreateAmount; j++)
            {
                tempObject = pool.useObject(listOfPrefabs[i]);
                //move the newly generated object to the current position of our randomizable circle area. 
                tempObject.transform.parent = transform;

                //get a position within the bounds of a circle (DPJ - not sure we need this since it#s first line of the DO loop)
                Vector3 randomPos = Random.insideUnitCircle * Radius;

                //keep track how many attemps we are have used whilst trying to find a suitable place for object to be spawn
                int tries = 0;

                //Ignore raycasts to this while it's checking
                tempObject.SetActive(false);
                //leep trying whilst you still have tries left (so game doesn't get stuck / lag whilst looking for a valid spot for too long)
                do
                {
                    //choose new random position inside the circle
                    randomPos = Random.insideUnitCircle * Radius;
                    tempObject.transform.localPosition = randomPos;
                    tries++;               
                } while (PositionOverlaps(tempObject) && tries < MaxTries);
                if (tries >= MaxTries)
                {
                    //if we haven't found a valid non-intersecting point, then delete the object and lets move on
                    ObjectPool.Instance.killObject(tempObject);
                    continue;
                }
                tempObject.SetActive(true);
                //sorting how to display the spawned game objects, based on a component which allows you to update sorting order
                ZSortedObject _zSortedObject = tempObject.GetComponentInChildren<ZSortedObject>();
                if( _zSortedObject != null)
                    _zSortedObject.updateZSortingAccordingtoYPos(null);

                //if temp object is a Material Pickup
                if (tempObject.GetComponent<MaterialPickup>() != null)
                {
                    float randNum = Random.Range(0f, 1f);
                    if (randNum <= .25f)
                    {
                        tempObject.GetComponent<MaterialPickup>().SetAsShrooms();
                    }
                    else if (randNum <= .5f)
                    {
                        tempObject.GetComponent<MaterialPickup>().SetAsLavender();
                    }
                    else if (randNum <= .75f)
                    {
                        tempObject.GetComponent<MaterialPickup>().SetAsOranges();
                    }
                    else
                    {
                        tempObject.GetComponent<MaterialPickup>().SetAsCinnamon();
                    }
                }
                //call setshroom/lav/ora/cin randomly to initialise the type and image for it
                var sr = tempObject.GetComponent<SpriteRenderer>();
                if (sr == null)
                {
                    sr = tempObject.GetComponentInChildren<SpriteRenderer>();
                }
               
                generatedObjects.Add(tempObject);
            }
        }
        // Updates all ZSortOders
        //MessageDispatcher.SendMessage("UpdateZSorting");
    }

    //Checks that the 2f units around the position of the object doesn't interect with any other objects (I believe that's what Overlap does). 
    public bool PositionOverlaps(GameObject t)
    {
        var hit = Physics2D.OverlapCircle(t.transform.position, ItemRadius);
        if (hit != null)
        {
            return true;
        }
        return false;
    }

    //A way to deactivate all objects within the generated object pool
    public void killAll()
    {
        for (int i = 0; i < generatedObjects.Count; i++)
        {
            ObjectPool.Instance.killObject(generatedObjects[i]);
        }
    }
}
