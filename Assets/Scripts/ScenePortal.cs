﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PortalSceneType
    {
    TOWN_LIAM,
    TOWN_FOREST_ENTRANCE,
    TOWN_OTL,
    LIAM_HOUSE,
    OTL_HOUSE,
    FOREST
}

public class ScenePortal : MonoBehaviour {
    public PortalSceneType toScene = PortalSceneType.TOWN_LIAM;

    //when we move from pseudo scene to sceene
    void TransitionScene()
    {
        //moves player to right position, and sets pseudo scene parameters such as music etc.
        GameController.Instance.changeScene(toScene);
        //start the heart transition. 
        CameraTransition.Instance.Transition(.75f, 0f);
        //wait .75, then set the game back in a "playble state".
        GameController.Instance.StartCoroutine(endTransition());
        GameController.Instance.player.GetComponentInChildren<Animator>().SetBool("IsWalking", false);
        //unsub. It is subbed when the played colliders with the portal
        CameraTransition.Instance.OnTransitionFinished -= TransitionScene;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            
            if( toScene != PortalSceneType.FOREST &&  toScene != PortalSceneType.TOWN_FOREST_ENTRANCE)
                SoundsEffectsManager.Instance.Play(GameController.Instance.doorOpenCloseSFX,0.25f);



            //close hear tot black
            CameraTransition.Instance.Transition(.75f, 1f);
            //subscribe TransitioniScene to listen when the transition is finished, will be broadcast from the transition (I assume) DPJ
            CameraTransition.Instance.OnTransitionFinished += TransitionScene;
            //set game state to reflect it is transitioning. This is useful to stop enemies attacking during a transition.
            GameController.Instance.setState(GameState.TRANSITION);

            GameController.Instance.currentScene = toScene;
        }
    }

    //couroutine to set the game state after the transition time (.75) has been set.
    IEnumerator endTransition()
    {
        yield return new WaitForSeconds(0.75f);
        GameController.Instance.setState(GameState.DURING);
    }

}
