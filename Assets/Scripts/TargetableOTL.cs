﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using com.ootii.Messages;

public class TargetableOTL : TargetableController {

    public bool isDeadKnown = false;
    public GameObject frogSelf;
    public GameObject werewolfSelf;
    public GameObject rosesSelf;
    public GameObject liamSelf;

    //reference to the player
    public TargetablePlayer player;

    //reference to reciepe book incase first use of spell is on OTL (so we add to recipe book)
    public RecipeBookManager _recipeBookManager;

    void Start () {
        //init parameters for OTL
        player = GameController.Instance.player.GetComponent<TargetablePlayer>();
        ds = GameController.Instance.dialogSystem;
        MAX_HP = 100;
        hp = MAX_HP;
        _recipeBookManager = FindObjectOfType<RecipeBookManager>();
    }
	

    public void ResetPlayer()
    {
        //reset parameters for OTL - used when start a new game
        transform.GetChild(0).gameObject.SetActive(true);
        isDead = false;
        hp = MAX_HP;
        isDeadKnown = false;
    }

    //the different actions to perform when clicked upon
    public override void handleClick(PotionType _ptype)
    {
        //switch off the potion curser and play casting animation
        GameController.Instance.cursorPotion.SetActive(false);
        player.GetComponent<TargetablePlayer>().playCastAnim();

        switch (_ptype)
        {           
            case PotionType.DMG_FIREBALL:
                //get hold of the fireball particles
                GameObject fball = GameController.Instance.Fireball;
                fball.SetActive(true);
                //originate the fireball from the player
                fball.transform.position = transform.position;
                //hard code duration as distance tends to be roughly consistent
                float fireDuration = 0.3f;
                fball.transform.DOMove(transform.position+(Vector3.up*2), fireDuration).SetEase(Ease.Linear).OnComplete(() => fireballComplete(fball));
                ds.show("THE POTION BURSTS INTO FLAMES AT THE FEET OF YOUR ONE TRUE LOVE!", 2);
                //this will sort out changing music based on ending, then loading the ending canvas after a set duration
                GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetGameIn(4,_ptype)
            );
                break;
            case PotionType.DMG_CHILL:
                //gets particle effect
                GameObject chill = GameController.Instance.Chill;
                chill.SetActive(true);
                chill.transform.position = transform.position;
                GameController.Instance.StartCoroutine(ChillEffect(chill));
                ds.show("THE TEMPERATURE OF THE ROOM PLUMITS, AS YOU START TO QUESTION WHAT YOU'VE DONE...", 2);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(4, _ptype)
           );
                break;
            case PotionType.DMG_EARTH:
                //shake camera and do delayed damage
                CameraShake.Shake(7, 2f);
                GameController.Instance.StartCoroutine(DelayedDamage(10, 1f));
                // TODO: damage all the enemies in a radius
                ds.show("NATALIE STARTS TO SHAKE... HER BODY IN A FIT... SOMETHINGS REALLY WRONG HERE...", 2);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(4, _ptype)
           );
                break;
            case PotionType.DMG_LOVE:                
                ds.show("NATALIE LOOKS AT YOU, WITH A LOOK IN HER EYE YOU'VE NEVER SEEN BEFORE", 2);
                // TODO: WIN!
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetGameIn(6, _ptype)
            );
                break;
            case PotionType.TRANS_FROG:
                //turns on frog game object sprite within the OTL GO, and turn off the sprite
                frogSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);             
                ds.show("SHE INSTANTLY TRANSFORMS INTO A FROG... MAYBE YOU COULD GIVE HER A KISS?", 2);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(6, _ptype)
           );
                break;
            case PotionType.TRANS_WEREWOLF:   
                //enable the werewolf child, disable the OTL sprite
                werewolfSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
                ds.show("YOU GOT EATEN BY YOUR ONE TRUE LOVE \n EPIC...\n JUST EPIC...", 3);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(5, _ptype)
           );
                break;
            case PotionType.TRANS_YOU:
                //enable liam sprite, disable OTL sprite
                liamSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
                ds.show("HER BEAUTY MELTS AWAY, AS SHE IS TRANSFORMED INTO... YOU? YET YOU STILL FEEL DEEPLY IN LOVE WITH... YOURSELF.", 3);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.resetGameIn(6, _ptype)
           );
                break;
            case PotionType.TRANS_FLOWER:
                //DPJ following isnt needed - it shows message and resets the level!
                GameController.Instance.StartCoroutine(FlowerEffect());
                //switch sprites
                rosesSelf.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);              
                ds.show("HER ONCE LONG LEGS TRANSFORM INTO A LONG LUSCIOUS STEM... PLANTING HERSELF INTO THE GROUND...", 3);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
          GameController.Instance.dayController.resetGameIn(6, _ptype)
          );
                break;
            case PotionType.HEAL_INC_MAX_HP:
                //add to total health and then heal that same amount with inverted damage function
                MAX_HP += 20;
                addDamage(-20);
                ds.show("SHE LOOKS BEMUSED, THOUGH YOU SENSE SHE IS MORE RESILIENT THAN EVER BEFORE", 3);
                // TODO: heal anim
                break;
            case PotionType.HEAL_TO_MAX_HP:
                ds.show("ANY SIGNS OF ILL HEALTH IN NATALIE HAVE NOW DISAPPEARED", 2);
                //restore to full hp, DPJ unsure if we need the Hp = maxhp line, as we will always heal 0 if so.
                hp = MAX_HP;
                addDamage(-(MAX_HP-hp));
                break;
            case PotionType.HEAL_INSTA_KILL:
                hp = 0;
                ds.show("HER BODY FALLS INSTANTLY TO THE GROUND. SHE'S STOPPED BREATHING. DAMN.", 2);
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
         GameController.Instance.dayController.resetGameIn(6, _ptype)
         );
                break;
            case PotionType.HEAL_POISON:
                //does 10 damager over 2.5 secodns 
                StartCoroutine(poisonTick());
                // TODO: add poison effect
                ds.show("SUDDENLY NATALIE DOESN'T LOOK SO WELL... ", 1);
                break;
            case PotionType.TIME_INC_MOVESPEED:
                //potion of haste, reduce days remaining
                GameController.Instance.dayController.currentDay++;
                //if the days are now 0 or less, load the ending :(
                if (GameController.Instance.dayController.currentDay >= 8)
                {
                    GameController.Instance.StartCoroutine(LeavingTooSoon());
                    
                } else
                {
                    ds.show("WITH HER NEWLY AQUIRED ENERGY, SHE CONSIDERS LEAVING TOWN A DAY EARLIER THAN SCHEDULED", 2);
                }
                //update the heart UI sprite
                GameController.Instance.dayController.daysLeftLabel.text = (8 - GameController.Instance.dayController.currentDay).ToString();
                break;
            case PotionType.TIME_PAUSE:
                //simply pauses game, nothing more.
                GameController.Instance.setState(GameState.PAUSED);
                break;
            case PotionType.TIME_TELEPORT:
                //teleports 10 units forward - DPJ could be improved by calculating distance between you and OTL
                transform.position += -transform.GetChild(0).up*10;
                ds.show("YOU TELEPORT FORWARD, BUT FIND YOURSELF SUDDENLY INSIDE HER...", 2 );
                //sets music and sets relevent ending in x amount of time
                GameController.Instance.StartCoroutine(
                GameController.Instance.dayController.resetGameIn(6, _ptype)
                );
                break;
            case PotionType.TIME_REWIND:
                ds.show("THE HANDS ON THE CLOCK START TURNING BACKWARDS... WHAT THE...", 2);
                //takes us back 1 day (day count) and restarts the day
                GameController.Instance.StartCoroutine(
           GameController.Instance.dayController.rewindDayIn(3)
           );
                break;
        }

        //update array of discovered potions
        if (_ptype != PotionType.NONE && _ptype != PotionType.TIME_PAUSE && GameController.Instance.discoveredPotions[_ptype] == false)
        {
            GameController.Instance.discoveredPotions[_ptype] = true;
            //call the recipe manager to handle new update
            _recipeBookManager.AddRecipeToBook(_ptype);
        }
        //reset potion type to avoid re-use
        GameController.Instance.curDraggedPotion.type = PotionType.NONE;
    }

    //do five ticks of 2 damage over 2.5 seconds
    public IEnumerator poisonTick()
    {
        yield return new WaitForSeconds(2f);
        addDamage(2);
        yield return new WaitForSeconds(2f);
        addDamage(2);
        yield return new WaitForSeconds(2f);
        addDamage(2);
        yield return new WaitForSeconds(2f);
        addDamage(2);
        yield return new WaitForSeconds(0.5f);
        addDamage(2);
    }

    //handling the special case that we fast forward a day when already on 1 day or less
    public IEnumerator LeavingTooSoon()
    {
        ds.show("SHE MOVES SO FAST THAT SHE LEAVES THE TOWN IMMEDIATELY.", 2);
        yield return new WaitForSeconds(2f);
        GameController.Instance.endingManager.ShowEnding("OUT_OF_DAYS");
    }

    //apply damage after a defined delay
    public IEnumerator DelayedDamage(float _amount,float _time)
    {      
        yield return new WaitForSeconds(_time);
        addDamage(_amount);
    }

    //simply waits for some time then resets day - (shouldn't. DPJ), doesn't handle changing into wolf
    public IEnumerator WerewolfEffect()
    {     
        yield return new WaitForSeconds(5f);
        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3)
            );
    }

    //simply waits for some time then resets day, displays message, doesn't handle changing into frog
    public IEnumerator FrogEffect()
    {
        yield return new WaitForSeconds(5f);
        //best line ever
        ds.show("YOU SPENT THE DAY AS A FROG. SHAME :(", 2);
        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3)
            );
    }

    //simply waits for some time, displays message, doesn't handle changing into wolf
    public IEnumerator FlowerEffect()
    {       
        yield return new WaitForSeconds(5f);
        ds.show("YOU SPENT THE DAY AS A FLOWER. \n YOU ENJOYED THE POLLEN THOUGH... :(", 2);
        GameController.Instance.StartCoroutine(
            GameController.Instance.dayController.resetDayIn(3)
            );
    }

    //slow down, apply damage, wait, resume speed, turn off ice particle effect
    public IEnumerator ChillEffect(GameObject _chill)
    {       
        addDamage(15);
        yield return new WaitForSeconds(3f);       
        _chill.SetActive(false);
    }

    //called when fireball hits target. Adds damage and deactivates the fireball effect.
    public void fireballComplete(GameObject fball)
    {
        addDamage(30);
        fball.SetActive(false);      
    }


    public override void handleDeath()
    {
        
     
        

    }

}
